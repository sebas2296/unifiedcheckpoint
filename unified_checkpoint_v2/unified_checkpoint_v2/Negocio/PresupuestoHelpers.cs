﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using unified_checkpoint_v2.Models;

namespace unified_checkpoint_v2.Negocio
{
    public class PresupuestoHelpers
    {
        public void sp_InsertarPresupuesto(Presupuesto presupuestos)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter Propiedad = new SqlParameter("@Propiedad", presupuestos.Propiedad);
            SqlParameter Ancho = new SqlParameter("@Ancho", presupuestos.Ancho);
            SqlParameter Largo = new SqlParameter("@Largo", presupuestos.Largo);
            SqlParameter Material = new SqlParameter("@Material", presupuestos.Material);
            SqlParameter Costo = new SqlParameter("@Costo", presupuestos.Costo);
            SqlParameter PresupuestoPrezi = new SqlParameter("@PresupuestoPrezi", presupuestos.PresupuestoPrezi);
            SqlParameter PresupuestoWazi = new SqlParameter("@PresupuestoWazi", presupuestos.PresupuestoWazi);
            SqlParameter PresupuestoWebBienesRaices = new SqlParameter("@PresupuestoWebBienesRaices", presupuestos.PresupuestoWebBienesRaices);
            SqlParameter PresupuestoFacebook = new SqlParameter("@PresupuestoFacebook", presupuestos.PresupuestoFacebook);
            SqlParameter PresupuestoInstagram = new SqlParameter("@PresupuestoInstagram", presupuestos.PresupuestoInstagram);
            SqlParameter PresupuestoEncuentra24 = new SqlParameter("@PresupuestoEncuentra24", presupuestos.PresupuestoEncuentra24);
            SqlParameter PresupuestoVideoPromocional = new SqlParameter("@PresupuestoVideoPromocional", presupuestos.PresupuestoVideoPromocional);
            SqlParameter PrecioFinalPresupuesto = new SqlParameter("@PrecioFinalPresupuesto ", presupuestos.PrecioFinalPresupuesto);

            model.Database.ExecuteSqlCommand("sp_InsertarPresupuesto @Propiedad, @Ancho, @Largo, @Material, @Costo,@PresupuestoPrezi,@PresupuestoWazi,@PresupuestoWebBienesRaices,@PresupuestoFacebook,@PresupuestoInstagram,@PresupuestoEncuentra24,@PresupuestoVideoPromocional,@PrecioFinalPresupuesto",
                 Propiedad, Ancho, Largo, Material, Costo, PresupuestoPrezi, PresupuestoWazi, PresupuestoWebBienesRaices, PresupuestoFacebook, PresupuestoInstagram, PresupuestoEncuentra24, PresupuestoVideoPromocional, PrecioFinalPresupuesto);
        }

        public void sp_ActualizarPresupuesto(Presupuesto presupuestos)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            try
            {
                SqlParameter IdPresupuesto = new SqlParameter("@IdPresupuesto", presupuestos.IdPresupuesto);
                SqlParameter Propiedad = new SqlParameter("@Propiedad", presupuestos.Propiedad);
                SqlParameter Ancho = new SqlParameter("@Ancho", presupuestos.Ancho);
                SqlParameter Largo = new SqlParameter("@Largo", presupuestos.Largo);
                SqlParameter Material = new SqlParameter("@Material", presupuestos.Material);
                SqlParameter Costo = new SqlParameter("@Costo", presupuestos.Costo);
                SqlParameter PresupuestoPrezi = new SqlParameter("@PresupuestoPrezi", presupuestos.PresupuestoPrezi);
                SqlParameter PresupuestoWazi = new SqlParameter("@PresupuestoWazi", presupuestos.PresupuestoWazi);
                SqlParameter PresupuestoWebBienesRaices = new SqlParameter("@PresupuestoWebBienesRaices", presupuestos.PresupuestoWebBienesRaices);
                SqlParameter PresupuestoFacebook = new SqlParameter("@PresupuestoFacebook", presupuestos.PresupuestoFacebook);
                SqlParameter PresupuestoInstagram = new SqlParameter("@PresupuestoInstagram", presupuestos.PresupuestoInstagram);
                SqlParameter PresupuestoEncuentra24 = new SqlParameter("@PresupuestoEncuentra24", presupuestos.PresupuestoEncuentra24);
                SqlParameter PresupuestoVideoPromocional = new SqlParameter("@PresupuestoVideoPromocional", presupuestos.PresupuestoVideoPromocional);
                SqlParameter PrecioFinalPresupuesto = new SqlParameter("@PrecioFinalPresupuesto ", presupuestos.PrecioFinalPresupuesto);

                model.Database.ExecuteSqlCommand("sp_ActualizarPresupuesto @IdPresupuesto,@Propiedad, @Ancho, @Largo, @Material, @Costo,@PresupuestoPrezi,@PresupuestoWazi,@PresupuestoWebBienesRaices,@PresupuestoFacebook,@PresupuestoInstagram,@PresupuestoEncuentra24,@PresupuestoVideoPromocional,@PrecioFinalPresupuesto",
                       IdPresupuesto, Propiedad, Ancho, Largo, Material, Costo, PresupuestoPrezi, PresupuestoWazi, PresupuestoWebBienesRaices, PresupuestoFacebook, PresupuestoInstagram, PresupuestoEncuentra24, PresupuestoVideoPromocional, PrecioFinalPresupuesto);
            }
            catch (Exception e)
            {
                e.ToString();
            }

        }


        public void sp_EliminarPresupuesto(Presupuesto presupuestos)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter IdPresupuesto = new SqlParameter("@IdPresupuesto", presupuestos.IdPresupuesto);

            model.Database.ExecuteSqlCommand("sp_EliminarPresupuesto @IdPresupuesto", IdPresupuesto);
        }
    }
}