﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using unified_checkpoint_v2.Models;

namespace unified_checkpoint_v2.Negocio
{
    public class AgenteHelpers
    {
        public void sp_InsertarAgente(Agente agente)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter tipoCedula = new SqlParameter("@tipoCedula", agente.TipoCedula);
            SqlParameter cedula = new SqlParameter("@cedula", agente.Cedula);
            SqlParameter Nombre = new SqlParameter("@Nombre", agente.Nombre);
            SqlParameter Apellido = new SqlParameter("@Apellido", agente.Apellido);
            SqlParameter Telefono = new SqlParameter("@Telefono", agente.Telefono);
            SqlParameter Correo = new SqlParameter("@Correo", agente.Correo);
            SqlParameter Agencia = new SqlParameter("@Agencia", agente.Agencia);
            SqlParameter Informacion = new SqlParameter("@Informacion", agente.InformacionAdicional);

            model.Database.ExecuteSqlCommand("sp_InsertarAgente @tipoCedula, @cedula, @Nombre, @Apellido,@Telefono,@Correo,@Agencia,@Informacion",
                tipoCedula, cedula, Nombre, Apellido, Telefono, Correo, Agencia, Informacion);
        }

        

        public void sp_ActualizarAgente(Agente agente)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter tipoCedula = new SqlParameter("@tipoCedula", agente.TipoCedula);
            SqlParameter cedula = new SqlParameter("@cedula", agente.Cedula);
            SqlParameter Nombre = new SqlParameter("@Nombre", agente.Nombre);
            SqlParameter Apellido = new SqlParameter("@Apellido", agente.Apellido);
            SqlParameter Telefono = new SqlParameter("@Telefono", agente.Telefono);
            SqlParameter Correo = new SqlParameter("@Correo", agente.Correo);
            SqlParameter Agencia = new SqlParameter("@Agencia", agente.Agencia);
            SqlParameter Informacion = new SqlParameter("@Informacion", agente.InformacionAdicional);

            model.Database.ExecuteSqlCommand("sp_ActualizarAgente @tipoCedula, @cedula, @Nombre, @Apellido,@Telefono,@Correo,@Agencia,@Informacion",
                tipoCedula, cedula, Nombre, Apellido, Telefono, Correo, Agencia, Informacion);
        }

        public void sp_EliminarAgente(Agente agente)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter cedula = new SqlParameter("@cedula", agente.Cedula);

            model.Database.ExecuteSqlCommand("sp_EliminarAgente @cedula", cedula);
        }
    }
}