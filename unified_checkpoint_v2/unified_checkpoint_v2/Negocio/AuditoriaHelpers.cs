﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using unified_checkpoint_v2.Models;

namespace unified_checkpoint_v2.Negocio
{
    public class AuditoriaHelpers
    {
        public void spAuditoria(Auditoria auditoria)
        {
            try
            {
                DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

                SqlParameter usuario = new SqlParameter("@usuario", auditoria.Usuario);
                SqlParameter modulo = new SqlParameter("@modulo", auditoria.Modulo);
                SqlParameter accion = new SqlParameter("@Accion", auditoria.Accion);

                model.Database.ExecuteSqlCommand("exec spAuditoria @usuario,@modulo,@Accion", usuario, modulo, accion);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }
    }
}