﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using unified_checkpoint_v2.Models;

namespace unified_checkpoint_v2.Negocio
{
    public class ClientesHelpers
    {
        public void sp_InsertarClientes(Cliente Clientes)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter cedula = new SqlParameter("@Cedula", Clientes.Cedula);
            SqlParameter Nombre = new SqlParameter("@Nombre", Clientes.Nombre);
            SqlParameter Apellido = new SqlParameter("@Apellido", Clientes.Apellido);
            SqlParameter Telefono = new SqlParameter("@Telefono", Clientes.Telefono);
            SqlParameter Correo = new SqlParameter("@Correo", Clientes.Correo);
            SqlParameter PropiedadGrs = new SqlParameter("@PropiedadGrs", Clientes.PropiedadGRS);
            SqlParameter Propiedad = new SqlParameter("@Propiedad", Clientes.Propiedad);
            SqlParameter Estado = new SqlParameter("@Estado", Clientes.Estado);

            model.Database.ExecuteSqlCommand("sp_InsertarCliente @Cedula, @Nombre, @Apellido,@Telefono,@Correo,@PropiedadGrs,@Propiedad,@Estado",
                cedula, Nombre, Apellido, Telefono, Correo,PropiedadGrs,Propiedad,Estado);
        }

        public void sp_ActualizarClientes(Cliente Clientes)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter cedula = new SqlParameter("@Cedula", Clientes.Cedula);
            SqlParameter Nombre = new SqlParameter("@Nombre", Clientes.Nombre);
            SqlParameter Apellido = new SqlParameter("@Apellido", Clientes.Apellido);
            SqlParameter Telefono = new SqlParameter("@Telefono", Clientes.Telefono);
            SqlParameter Correo = new SqlParameter("@Correo", Clientes.Correo);
            SqlParameter PropiedadGrs = new SqlParameter("@PropiedadGrs", Clientes.PropiedadGRS);
            SqlParameter Propiedad = new SqlParameter("@Propiedad", Clientes.Propiedad);
            SqlParameter Estado = new SqlParameter("@Estado", Clientes.Estado);

            model.Database.ExecuteSqlCommand("sp_ActualizarCliente @Cedula, @Nombre, @Apellido,@Telefono,@Correo,@PropiedadGrs,@Propiedad,@Estado",
                cedula, Nombre, Apellido, Telefono, Correo, PropiedadGrs, Propiedad, Estado);
        }

        public void sp_EliminarClientes(Cliente Clientes)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter cedula = new SqlParameter("@Cedula", Clientes.Cedula);
            SqlParameter propiedad = new SqlParameter("@Propiedad", Clientes.Propiedad);

            model.Database.ExecuteSqlCommand("sp_EliminarCliente @Cedula,@Propiedad", cedula,propiedad);
        }
    }
}