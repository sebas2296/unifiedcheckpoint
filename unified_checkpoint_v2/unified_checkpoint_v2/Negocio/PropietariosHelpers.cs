﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using unified_checkpoint_v2.Models;
using System.Data.SqlClient;

namespace unified_checkpoint_v2.Negocio
{
    public class PropietariosHelpers
    {
        public void sp_InsertarPropietarios(Propietario Propietarios)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter tipoCedula = new SqlParameter("@TipoCedula", Propietarios.TipoCedula);
            SqlParameter cedula = new SqlParameter("@Cedula", Propietarios.Cedula);
            SqlParameter Nombre = new SqlParameter("@Nombre", Propietarios.Nombre);
            SqlParameter Apellido = new SqlParameter("@Apellido", Propietarios.Apellido);
            SqlParameter Telefono = new SqlParameter("@Telefono", Propietarios.Telefono);
            SqlParameter Correo = new SqlParameter("@Correo", Propietarios.Correo);
            SqlParameter Informacion = new SqlParameter("@Informacion", Propietarios.InformacionAdicional);
            SqlParameter NombreSec = new SqlParameter("@NombreSec", Propietarios.NombreCompleto_Sec);
            SqlParameter CorreoSec = new SqlParameter("@CorreoSec", Propietarios.Email_Sec);
            SqlParameter TelefonoSec = new SqlParameter("@TelefonoSec", Propietarios.Telefono_Sec);
            SqlParameter ParentezcoSec = new SqlParameter("@ParentezcoSec", Propietarios.Parentezco_Sec);

            model.Database.ExecuteSqlCommand("sp_InsertarPropietario @TipoCedula, @Cedula, @Nombre, @Apellido,@Telefono,@Correo,@Informacion,@NombreSec,@CorreoSec,@TelefonoSec,@ParentezcoSec",
                tipoCedula, cedula, Nombre, Apellido, Telefono, Correo, Informacion, NombreSec, CorreoSec, TelefonoSec, ParentezcoSec);
        }

        public void sp_ActualizarPropietarios(Propietario Propietarios)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter tipoCedula = new SqlParameter("@TipoCedula", Propietarios.TipoCedula);
            SqlParameter cedula = new SqlParameter("@Cedula", Propietarios.Cedula);
            SqlParameter Nombre = new SqlParameter("@Nombre", Propietarios.Nombre);
            SqlParameter Apellido = new SqlParameter("@Apellido", Propietarios.Apellido);
            SqlParameter Telefono = new SqlParameter("@Telefono", Propietarios.Telefono);
            SqlParameter Correo = new SqlParameter("@Correo", Propietarios.Correo);
            SqlParameter Informacion = new SqlParameter("@Informacion", Propietarios.InformacionAdicional);
            SqlParameter NombreSec = new SqlParameter("@NombreSec", Propietarios.NombreCompleto_Sec);
            SqlParameter CorreoSec = new SqlParameter("@CorreoSec", Propietarios.Email_Sec);
            SqlParameter TelefonoSec = new SqlParameter("@TelefonoSec", Propietarios.Telefono_Sec);
            SqlParameter ParentezcoSec = new SqlParameter("@ParentezcoSec", Propietarios.Parentezco_Sec);

            model.Database.ExecuteSqlCommand("sp_ActualizarPropietario @TipoCedula, @Cedula, @Nombre, @Apellido,@Telefono,@Correo,@Informacion,@NombreSec,@CorreoSec,@TelefonoSec,@ParentezcoSec",
                tipoCedula, cedula, Nombre, Apellido, Telefono, Correo, Informacion, NombreSec, CorreoSec, TelefonoSec, ParentezcoSec);
        }

        public void sp_EliminarPropietarios(Propietario Propietarios)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter cedula = new SqlParameter("@cedula", Propietarios.Cedula);

            model.Database.ExecuteSqlCommand("sp_EliminarPropietario @Cedula", cedula);
        }
    }
}