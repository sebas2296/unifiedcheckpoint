﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using unified_checkpoint_v2.Models;

namespace unified_checkpoint_v2.Negocio
{
    public class PropiedadAgenteHelpers
    {
        public void sp_InsertarPropiedad(Propiedade propiedad)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter codigo = new SqlParameter("@Codigo", propiedad.Codigo);
            SqlParameter nombre = new SqlParameter("@Nombre", propiedad.Nombre);
            SqlParameter agente = new SqlParameter("@Agente", propiedad.Agente);
            SqlParameter linkSitioWeb = new SqlParameter("@LinkSitioWeb", propiedad.LinkSitioWeb);
            SqlParameter linkGoogleMaps = new SqlParameter("@LinkGoogleMaps", propiedad.LinkGoogleMaps);
            SqlParameter precioVenta = new SqlParameter("@PrecioVenta", propiedad.PrecioVenta);
            SqlParameter precioAlquiler = new SqlParameter("@PrecioAlquiler", propiedad.PrecioAlquiler);
            SqlParameter noPlanoCatastro = new SqlParameter("@NoPlanoCatastro", propiedad.NoPlanoCatastro);
            SqlParameter NoFolioReal = new SqlParameter("@NoFolioReal", propiedad.NoFolioReal);
            SqlParameter AreaTerrenoM2 = new SqlParameter("@AreaTerrenoM2", propiedad.AreaTerrenoM2);
            SqlParameter AreaConstruccionM2 = new SqlParameter("@AreaConstruccionM2", propiedad.AreaConstruccionM2);
            SqlParameter Provincia = new SqlParameter("@Provincia", propiedad.Provincia);
            SqlParameter Canton = new SqlParameter("@Canton", propiedad.Canton);
            SqlParameter Distrito = new SqlParameter("@Distrito", propiedad.Distrito);
            SqlParameter Direccion = new SqlParameter("@Direccion", propiedad.Direccion);
            SqlParameter CuotaMantenimiento = new SqlParameter("@CuotaMantenimiento", propiedad.CuotaMantenimiento);
            SqlParameter CuotaSeguridad = new SqlParameter("@CuotaSeguridad", propiedad.CuotaSeguridad);
            SqlParameter ImpuestoSolidario = new SqlParameter("@ImpuestoSolidario", propiedad.ImpuestoSolidario);
            SqlParameter Impuestos = new SqlParameter("@Impuestos", propiedad.Impuestos);
            SqlParameter IVA = new SqlParameter("@IVA", propiedad.IVA);
            SqlParameter CodigoPostal = new SqlParameter("@CodigoPostal", propiedad.CodigoPostal);
            SqlParameter FrenteM2 = new SqlParameter("@FrenteM2 ", propiedad.FrenteM2);
            SqlParameter FondoM2 = new SqlParameter("@FondoM2", propiedad.FondoM2);
            SqlParameter AlturaM2 = new SqlParameter("@AlturaM2", propiedad.AlturaM2);
            SqlParameter Niveles = new SqlParameter("@Niveles", propiedad.Niveles);
            SqlParameter Dormitorios = new SqlParameter("@Dormitorios", propiedad.Dormitorios);
            SqlParameter Baños = new SqlParameter("@Baños", propiedad.Baños);
            SqlParameter Pisos = new SqlParameter("@Pisos", propiedad.Pisos);
            SqlParameter Cielos = new SqlParameter("@Cielos", propiedad.Cielos);
            SqlParameter MedidorAgua = new SqlParameter("@MedidorAgua", propiedad.MedidorAgua);
            SqlParameter Sala = new SqlParameter("@Sala", propiedad.Sala);
            SqlParameter Comedor = new SqlParameter("@Comedor", propiedad.Comedor);
            SqlParameter Cocina = new SqlParameter("@Cocina", propiedad.Cocina);
            SqlParameter Desayunador = new SqlParameter("@Desayunador", propiedad.Desayunador);
            SqlParameter Antecom = new SqlParameter("@Antecom", propiedad.Antecom);
            SqlParameter SalaTV = new SqlParameter("@SalaTV", propiedad.SalaTV);
            SqlParameter Bar = new SqlParameter("@Bar", propiedad.Bar);
            SqlParameter Casino = new SqlParameter("@Casino", propiedad.Casino);
            SqlParameter Terraza = new SqlParameter("@Terraza", propiedad.Terraza);
            SqlParameter Balcon = new SqlParameter("@Balcon", propiedad.Balcon);
            SqlParameter Lavanderia = new SqlParameter("@Lavanderia", propiedad.Lavanderia);
            SqlParameter Despensa = new SqlParameter("@Despensa", propiedad.Despensa);
            SqlParameter ClosetBlanco = new SqlParameter("@ClosetBlanco", propiedad.ClosetBlanco);
            SqlParameter Walk_in_Closet = new SqlParameter("@Walk_in_Closet", propiedad.Walk_in_Closet);
            SqlParameter Puertas_Baño = new SqlParameter("@Puertas_Baño", propiedad.Puertas_Baño);
            SqlParameter Senderos = new SqlParameter("@Senderos", propiedad.Senderos);
            SqlParameter Mascotas = new SqlParameter("@Mascotas", propiedad.Mascotas);
            SqlParameter AreaMascotaM2 = new SqlParameter("@AreaMascotaM2", propiedad.AreaMascotaM2);
            SqlParameter Perrera = new SqlParameter("@Perrera", propiedad.Perrera);
            SqlParameter Seguridad = new SqlParameter("@Seguridad", propiedad.Seguridad);
            SqlParameter Oficina = new SqlParameter("@Oficina", propiedad.Oficina);
            SqlParameter Bodega = new SqlParameter("@Bodega", propiedad.Bodega);
            SqlParameter Garage = new SqlParameter("@Garage", propiedad.Garage);
            SqlParameter Jardin = new SqlParameter("@Jardin", propiedad.Jardin);
            SqlParameter Patio = new SqlParameter("@Patio", propiedad.Patio);
            SqlParameter Patio_Luz = new SqlParameter("@Patio_Luz", propiedad.Patio_Luz);
            SqlParameter Atico = new SqlParameter("@Atico", propiedad.Atico);
            SqlParameter Piscina = new SqlParameter("@Piscina", propiedad.Piscina);
            SqlParameter Rancho = new SqlParameter("@Rancho", propiedad.Rancho);
            SqlParameter Canchas = new SqlParameter("@Canchas", propiedad.Canchas);
            SqlParameter Gimnacio = new SqlParameter("@Gimnacio", propiedad.Gimnacio);
            SqlParameter Lounge = new SqlParameter("@Lounge ", propiedad.Lounge);
            SqlParameter Jacuzzi = new SqlParameter("@Jacuzzi", propiedad.Jacuzzi);
            SqlParameter Cortinas = new SqlParameter("@Cortinas", propiedad.Cortinas);
            SqlParameter Lamparas = new SqlParameter("@Lamparas", propiedad.Lamparas);
            SqlParameter Chimenea = new SqlParameter("@Chimenea", propiedad.Chimenea);
            SqlParameter Cedazos = new SqlParameter("@Cedazos", propiedad.Cedazos);
            SqlParameter InterCom = new SqlParameter("@InterCom", propiedad.InterCom);
            SqlParameter PlantaElectrica = new SqlParameter("@PlantaElectrica ", propiedad.PlantaElectrica);
            SqlParameter Asensor = new SqlParameter("@Asensor", propiedad.Asensor);
            SqlParameter Parqueo = new SqlParameter("@Parqueo", propiedad.Parqueo);
            SqlParameter HotH20 = new SqlParameter("@HotH20", propiedad.HotH20);
            SqlParameter Cloaca = new SqlParameter("@Cloaca", propiedad.Cloaca);
            SqlParameter Septico = new SqlParameter("@Septico", propiedad.Septico);
            SqlParameter PlantaTratamiento = new SqlParameter("@PlantaTratamiento", propiedad.PlantaTratamiento);
            SqlParameter Capt_Bomba = new SqlParameter("@Capt_Bomba", propiedad.Capt_Bomba);
            SqlParameter Tels = new SqlParameter("@Tels", propiedad.Tels);
            SqlParameter Cable = new SqlParameter("@Cable", propiedad.Cable);
            SqlParameter Internet = new SqlParameter("@Internet", propiedad.Internet);
            SqlParameter Alarma = new SqlParameter("@Alarma", propiedad.Alarma);
            SqlParameter CCTV = new SqlParameter("@CCTV", propiedad.CCTV);
            SqlParameter PortonElectrico = new SqlParameter("@PortonElectrico", propiedad.PortonElectrico);
            SqlParameter Control = new SqlParameter("@Control", propiedad.Control);
            SqlParameter CercaElectrica = new SqlParameter("@CercaElectrica", propiedad.CercaElectrica);
            SqlParameter AlambreNavaja = new SqlParameter("@AlambreNavaja", propiedad.AlambreNavaja);
            SqlParameter A_C = new SqlParameter("@A_C", propiedad.A_C);
            SqlParameter LampEmergencia = new SqlParameter("@LampEmergencia", propiedad.LampEmergencia);
            SqlParameter DetectorHumo = new SqlParameter("@DetectorHumo", propiedad.DetectorHumo);
            SqlParameter SistemaIncendio = new SqlParameter("@SistemaIncendio", propiedad.SistemaIncendio);
            SqlParameter SistemaSonido = new SqlParameter("@SistemaSonido", propiedad.SistemaSonido);
            SqlParameter TableroDigital = new SqlParameter("@TableroDigital", propiedad.TableroDigital);
            SqlParameter TableroElec = new SqlParameter("@TableroElec", propiedad.TableroElec);
            SqlParameter Sobres = new SqlParameter("@Sobres", propiedad.Sobres);
            SqlParameter Amueblado = new SqlParameter("@Amueblado", propiedad.Amueblado);
            SqlParameter MueblesCocina = new SqlParameter("@MueblesCocina", propiedad.MueblesCocina);
            SqlParameter Electrod = new SqlParameter("@Electrod", propiedad.Electrod);
            SqlParameter Cerchas = new SqlParameter("@Cerchas ", propiedad.Cerchas);
            SqlParameter Cornizas = new SqlParameter("@Cornizas", propiedad.Cornizas);
            SqlParameter Puertas = new SqlParameter("@Puertas", propiedad.Puertas);
            SqlParameter ContraMarcos = new SqlParameter("@ContraMarcos", propiedad.ContraMarcos);
            SqlParameter Ventaneria = new SqlParameter("@Ventaneria", propiedad.Ventaneria);
            SqlParameter Cobertura = new SqlParameter("@Cobertura", propiedad.Cobertura);
            SqlParameter Est_Suelo = new SqlParameter("@Est_Suelo", propiedad.Est_Suelo);
            SqlParameter DispositivoH2O = new SqlParameter("@DispositivoH2O", propiedad.DispositivoH2O);
            SqlParameter UsoSuelo = new SqlParameter("@UsoSuelo", propiedad.UsoSuelo);
            SqlParameter Planos_Construccion = new SqlParameter("@Planos_Construccion", propiedad.Planos_Construccion);
            SqlParameter Plano_Catastro = new SqlParameter("@Plano_Catastro", propiedad.Plano_Catastro);
            SqlParameter Curvas_Nivel = new SqlParameter("@Curvas_Nivel", propiedad.Curvas_Nivel);
            SqlParameter Avaluo = new SqlParameter("@Avaluo", propiedad.Avaluo);
            SqlParameter Venta = new SqlParameter("@Venta", propiedad.Venta);
            SqlParameter Alquiler = new SqlParameter("@Alquiler", propiedad.Alquiler);
            SqlParameter T_Casa = new SqlParameter("@T_Casa", propiedad.T_Casa);
            SqlParameter T_Terreno = new SqlParameter("@T_Terreno", propiedad.T_Terreno);
            SqlParameter T_Local = new SqlParameter("@T_Local", propiedad.T_Local);
            SqlParameter T_Oficina = new SqlParameter("@T_Oficina", propiedad.T_Oficina);
            SqlParameter T_Bodega = new SqlParameter("@T_Bodega ", propiedad.T_Bodega);
            SqlParameter T_Edificio = new SqlParameter("@T_Edificio", propiedad.T_Edificio);
            SqlParameter T_Hotel = new SqlParameter("@T_Hotel", propiedad.T_Hotel);
            SqlParameter T_Condominio = new SqlParameter("@T_Condominio", propiedad.T_Condominio);
            SqlParameter T_Apartamento = new SqlParameter("@T_Apartamento ", propiedad.T_Apartamento);
            SqlParameter Ocupado = new SqlParameter("@Ocupado", propiedad.Ocupado);
            SqlParameter Llaves = new SqlParameter("@Llaves", propiedad.Llaves);
            SqlParameter MapsLat = new SqlParameter("@MapsLat ", propiedad.MapsLat);
            SqlParameter MapsLong = new SqlParameter("@MapsLong", propiedad.MapsLong);
            SqlParameter Estado = new SqlParameter("@Estado", propiedad.Estado);


            model.Database.ExecuteSqlCommand("sp_InsertarPropiedadAgente @Codigo,@Nombre,@Agente,@LinkSitioWeb,@LinkGoogleMaps,@PrecioVenta,@PrecioAlquiler,@NoPlanoCatastro,@NoFolioReal,@AreaTerrenoM2,@AreaConstruccionM2,@Provincia,@Canton,@Distrito,@Direccion,@CuotaMantenimiento,@CuotaSeguridad,@ImpuestoSolidario,@Impuestos,@IVA,@CodigoPostal,@FrenteM2,@FondoM2,@AlturaM2,@Niveles,@Dormitorios,@Baños,@Pisos,@Cielos,@MedidorAgua,@Sala,@Comedor,@Cocina,@Desayunador,@Antecom,@SalaTV,@Bar,@Casino,@Terraza,@Balcon,@Lavanderia,@Despensa,@ClosetBlanco,@Walk_in_Closet,@Puertas_Baño,@Senderos,@Mascotas,@AreaMascotaM2,@Perrera,@Seguridad,@Oficina,@Bodega,@Garage,@Jardin,@Patio,@Patio_Luz,@Atico,@Piscina,@Rancho,@Canchas,@Gimnacio,@Lounge,@Jacuzzi,@Cortinas,@Lamparas,@Chimenea,@Cedazos,@InterCom,@PlantaElectrica,@Asensor,@Parqueo,@HotH20,@Cloaca,@Septico,@PlantaTratamiento,@Capt_Bomba,@Tels,@Cable,@Internet,@Alarma,@CCTV,@PortonElectrico,@Control,@CercaElectrica,@AlambreNavaja,@A_C,@LampEmergencia,@DetectorHumo,@SistemaIncendio,@SistemaSonido,@TableroDigital,@TableroElec,@Sobres,@Amueblado,@MueblesCocina,@Electrod,@Cerchas,@Cornizas,@Puertas,@ContraMarcos,@Ventaneria,@Cobertura,@Est_Suelo,@DispositivoH2O,@UsoSuelo,@Planos_Construccion,@Plano_Catastro,@Curvas_Nivel,@Avaluo,@Venta,@Alquiler,@T_Casa,@T_Terreno,@T_Local,@T_Oficina,@T_Bodega,@T_Edificio,@T_Hotel,@T_Condominio,@T_Apartamento,@Ocupado,@Llaves,@MapsLat,@MapsLong,@Estado",
                codigo, nombre, agente, linkSitioWeb, linkGoogleMaps, precioVenta, precioAlquiler, noPlanoCatastro, NoFolioReal, AreaTerrenoM2, AreaConstruccionM2, Provincia, Canton, Distrito, Direccion, CuotaMantenimiento, CuotaSeguridad, ImpuestoSolidario, Impuestos, IVA, CodigoPostal, FrenteM2, FondoM2, AlturaM2, Niveles, Dormitorios, Baños, Pisos, Cielos, MedidorAgua, Sala, Comedor, Cocina, Desayunador, Antecom, SalaTV, Bar, Casino, Terraza, Balcon, Lavanderia, Despensa, ClosetBlanco, Walk_in_Closet, Puertas_Baño, Senderos, Mascotas, AreaMascotaM2, Perrera, Seguridad, Oficina, Bodega, Garage, Jardin, Patio, Patio_Luz, Atico, Piscina, Rancho, Canchas, Gimnacio, Lounge, Jacuzzi, Cortinas, Lamparas, Chimenea, Cedazos, InterCom, PlantaElectrica, Asensor, Parqueo, HotH20, Cloaca, Septico, PlantaTratamiento, Capt_Bomba, Tels, Cable, Internet, Alarma, CCTV, PortonElectrico, Control, CercaElectrica, AlambreNavaja, A_C, LampEmergencia, DetectorHumo, SistemaIncendio, SistemaSonido, TableroDigital, TableroElec, Sobres, Amueblado, MueblesCocina, Electrod, Cerchas, Cornizas, Puertas, ContraMarcos, Ventaneria, Cobertura, Est_Suelo, DispositivoH2O, UsoSuelo, Planos_Construccion, Plano_Catastro, Curvas_Nivel, Avaluo, Venta, Alquiler, T_Casa, T_Terreno, T_Local, T_Oficina, T_Bodega, T_Edificio, T_Hotel, T_Condominio, T_Apartamento, Ocupado, Llaves, MapsLat, MapsLong, Estado);
        }

        public void sp_Actualizarpropiedad(Propiedade propiedad)
        {
            try
            {
                DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

                SqlParameter codigo = new SqlParameter("@Codigo", propiedad.Codigo);
                SqlParameter nombre = new SqlParameter("@Nombre", propiedad.Nombre);
                SqlParameter agente = new SqlParameter("@Agente", propiedad.Agente);
                SqlParameter linkSitioWeb = new SqlParameter("@LinkSitioWeb", propiedad.LinkSitioWeb);
                SqlParameter linkGoogleMaps = new SqlParameter("@LinkGoogleMaps", propiedad.LinkGoogleMaps);
                SqlParameter precioVenta = new SqlParameter("@PrecioVenta", propiedad.PrecioVenta);
                SqlParameter precioAlquiler = new SqlParameter("@PrecioAlquiler", propiedad.PrecioAlquiler);
                SqlParameter noPlanoCatastro = new SqlParameter("@NoPlanoCatastro", propiedad.NoPlanoCatastro);
                SqlParameter NoFolioReal = new SqlParameter("@NoFolioReal", propiedad.NoFolioReal);
                SqlParameter AreaTerrenoM2 = new SqlParameter("@AreaTerrenoM2", propiedad.AreaTerrenoM2);
                SqlParameter AreaConstruccionM2 = new SqlParameter("@AreaConstruccionM2", propiedad.AreaConstruccionM2);
                SqlParameter Provincia = new SqlParameter("@Provincia", propiedad.Provincia);
                SqlParameter Canton = new SqlParameter("@Canton", propiedad.Canton);
                SqlParameter Distrito = new SqlParameter("@Distrito", propiedad.Distrito);
                SqlParameter Direccion = new SqlParameter("@Direccion", propiedad.Direccion);
                SqlParameter CuotaMantenimiento = new SqlParameter("@CuotaMantenimiento", propiedad.CuotaMantenimiento);
                SqlParameter CuotaSeguridad = new SqlParameter("@CuotaSeguridad", propiedad.CuotaSeguridad);
                SqlParameter ImpuestoSolidario = new SqlParameter("@ImpuestoSolidario", propiedad.ImpuestoSolidario);
                SqlParameter Impuestos = new SqlParameter("@Impuestos", propiedad.Impuestos);
                SqlParameter IVA = new SqlParameter("@IVA", propiedad.IVA);
                SqlParameter CodigoPostal = new SqlParameter("@CodigoPostal", propiedad.CodigoPostal);
                SqlParameter FrenteM2 = new SqlParameter("@FrenteM2 ", propiedad.FrenteM2);
                SqlParameter FondoM2 = new SqlParameter("@FondoM2", propiedad.FondoM2);
                SqlParameter AlturaM2 = new SqlParameter("@AlturaM2", propiedad.AlturaM2);
                SqlParameter Niveles = new SqlParameter("@Niveles", propiedad.Niveles);
                SqlParameter Dormitorios = new SqlParameter("@Dormitorios", propiedad.Dormitorios);
                SqlParameter Baños = new SqlParameter("@Baños", propiedad.Baños);
                SqlParameter Pisos = new SqlParameter("@Pisos", propiedad.Pisos);
                SqlParameter Cielos = new SqlParameter("@Cielos", propiedad.Cielos);
                SqlParameter MedidorAgua = new SqlParameter("@MedidorAgua", propiedad.MedidorAgua);
                SqlParameter Sala = new SqlParameter("@Sala", propiedad.Sala);
                SqlParameter Comedor = new SqlParameter("@Comedor", propiedad.Comedor);
                SqlParameter Cocina = new SqlParameter("@Cocina", propiedad.Cocina);
                SqlParameter Desayunador = new SqlParameter("@Desayunador", propiedad.Desayunador);
                SqlParameter Antecom = new SqlParameter("@Antecom", propiedad.Antecom);
                SqlParameter SalaTV = new SqlParameter("@SalaTV", propiedad.SalaTV);
                SqlParameter Bar = new SqlParameter("@Bar", propiedad.Bar);
                SqlParameter Casino = new SqlParameter("@Casino", propiedad.Casino);
                SqlParameter Terraza = new SqlParameter("@Terraza", propiedad.Terraza);
                SqlParameter Balcon = new SqlParameter("@Balcon", propiedad.Balcon);
                SqlParameter Lavanderia = new SqlParameter("@Lavanderia", propiedad.Lavanderia);
                SqlParameter Despensa = new SqlParameter("@Despensa", propiedad.Despensa);
                SqlParameter ClosetBlanco = new SqlParameter("@ClosetBlanco", propiedad.ClosetBlanco);
                SqlParameter Walk_in_Closet = new SqlParameter("@Walk_in_Closet", propiedad.Walk_in_Closet);
                SqlParameter Puertas_Baño = new SqlParameter("@Puertas_Baño", propiedad.Puertas_Baño);
                SqlParameter Senderos = new SqlParameter("@Senderos", propiedad.Senderos);
                SqlParameter Mascotas = new SqlParameter("@Mascotas", propiedad.Mascotas);
                SqlParameter AreaMascotaM2 = new SqlParameter("@AreaMascotaM2", propiedad.AreaMascotaM2);
                SqlParameter Perrera = new SqlParameter("@Perrera", propiedad.Perrera);
                SqlParameter Seguridad = new SqlParameter("@Seguridad", propiedad.Seguridad);
                SqlParameter Oficina = new SqlParameter("@Oficina", propiedad.Oficina);
                SqlParameter Bodega = new SqlParameter("@Bodega", propiedad.Bodega);
                SqlParameter Garage = new SqlParameter("@Garage", propiedad.Garage);
                SqlParameter Jardin = new SqlParameter("@Jardin", propiedad.Jardin);
                SqlParameter Patio = new SqlParameter("@Patio", propiedad.Patio);
                SqlParameter Patio_Luz = new SqlParameter("@Patio_Luz", propiedad.Patio_Luz);
                SqlParameter Atico = new SqlParameter("@Atico", propiedad.Atico);
                SqlParameter Piscina = new SqlParameter("@Piscina", propiedad.Piscina);
                SqlParameter Rancho = new SqlParameter("@Rancho", propiedad.Rancho);
                SqlParameter Canchas = new SqlParameter("@Canchas", propiedad.Canchas);
                SqlParameter Gimnacio = new SqlParameter("@Gimnacio", propiedad.Gimnacio);
                SqlParameter Lounge = new SqlParameter("@Lounge ", propiedad.Lounge);
                SqlParameter Jacuzzi = new SqlParameter("@Jacuzzi", propiedad.Jacuzzi);
                SqlParameter Cortinas = new SqlParameter("@Cortinas", propiedad.Cortinas);
                SqlParameter Lamparas = new SqlParameter("@Lamparas", propiedad.Lamparas);
                SqlParameter Chimenea = new SqlParameter("@Chimenea", propiedad.Chimenea);
                SqlParameter Cedazos = new SqlParameter("@Cedazos", propiedad.Cedazos);
                SqlParameter InterCom = new SqlParameter("@InterCom", propiedad.InterCom);
                SqlParameter PlantaElectrica = new SqlParameter("@PlantaElectrica ", propiedad.PlantaElectrica);
                SqlParameter Asensor = new SqlParameter("@Asensor", propiedad.Asensor);
                SqlParameter Parqueo = new SqlParameter("@Parqueo", propiedad.Parqueo);
                SqlParameter HotH20 = new SqlParameter("@HotH20", propiedad.HotH20);
                SqlParameter Cloaca = new SqlParameter("@Cloaca", propiedad.Cloaca);
                SqlParameter Septico = new SqlParameter("@Septico", propiedad.Septico);
                SqlParameter PlantaTratamiento = new SqlParameter("@PlantaTratamiento", propiedad.PlantaTratamiento);
                SqlParameter Capt_Bomba = new SqlParameter("@Capt_Bomba", propiedad.Capt_Bomba);
                SqlParameter Tels = new SqlParameter("@Tels", propiedad.Tels);
                SqlParameter Cable = new SqlParameter("@Cable", propiedad.Cable);
                SqlParameter Internet = new SqlParameter("@Internet", propiedad.Internet);
                SqlParameter Alarma = new SqlParameter("@Alarma", propiedad.Alarma);
                SqlParameter CCTV = new SqlParameter("@CCTV", propiedad.CCTV);
                SqlParameter PortonElectrico = new SqlParameter("@PortonElectrico", propiedad.PortonElectrico);
                SqlParameter Control = new SqlParameter("@Control", propiedad.Control);
                SqlParameter CercaElectrica = new SqlParameter("@CercaElectrica", propiedad.CercaElectrica);
                SqlParameter AlambreNavaja = new SqlParameter("@AlambreNavaja", propiedad.AlambreNavaja);
                SqlParameter A_C = new SqlParameter("@A_C", propiedad.A_C);
                SqlParameter LampEmergencia = new SqlParameter("@LampEmergencia", propiedad.LampEmergencia);
                SqlParameter DetectorHumo = new SqlParameter("@DetectorHumo", propiedad.DetectorHumo);
                SqlParameter SistemaIncendio = new SqlParameter("@SistemaIncendio", propiedad.SistemaIncendio);
                SqlParameter SistemaSonido = new SqlParameter("@SistemaSonido", propiedad.SistemaSonido);
                SqlParameter TableroDigital = new SqlParameter("@TableroDigital", propiedad.TableroDigital);
                SqlParameter TableroElec = new SqlParameter("@TableroElec", propiedad.TableroElec);
                SqlParameter Sobres = new SqlParameter("@Sobres", propiedad.Sobres);
                SqlParameter Amueblado = new SqlParameter("@Amueblado", propiedad.Amueblado);
                SqlParameter MueblesCocina = new SqlParameter("@MueblesCocina", propiedad.MueblesCocina);
                SqlParameter Electrod = new SqlParameter("@Electrod", propiedad.Electrod);
                SqlParameter Cerchas = new SqlParameter("@Cerchas ", propiedad.Cerchas);
                SqlParameter Cornizas = new SqlParameter("@Cornizas", propiedad.Cornizas);
                SqlParameter Puertas = new SqlParameter("@Puertas", propiedad.Puertas);
                SqlParameter ContraMarcos = new SqlParameter("@ContraMarcos", propiedad.ContraMarcos);
                SqlParameter Ventaneria = new SqlParameter("@Ventaneria", propiedad.Ventaneria);
                SqlParameter Cobertura = new SqlParameter("@Cobertura", propiedad.Cobertura);
                SqlParameter Est_Suelo = new SqlParameter("@Est_Suelo", propiedad.Est_Suelo);
                SqlParameter DispositivoH2O = new SqlParameter("@DispositivoH2O", propiedad.DispositivoH2O);
                SqlParameter UsoSuelo = new SqlParameter("@UsoSuelo", propiedad.UsoSuelo);
                SqlParameter Planos_Construccion = new SqlParameter("@Planos_Construccion", propiedad.Planos_Construccion);
                SqlParameter Plano_Catastro = new SqlParameter("@Plano_Catastro", propiedad.Plano_Catastro);
                SqlParameter Curvas_Nivel = new SqlParameter("@Curvas_Nivel", propiedad.Curvas_Nivel);
                SqlParameter Avaluo = new SqlParameter("@Avaluo", propiedad.Avaluo);
                SqlParameter Venta = new SqlParameter("@Venta", propiedad.Venta);
                SqlParameter Alquiler = new SqlParameter("@Alquiler", propiedad.Alquiler);
                SqlParameter T_Casa = new SqlParameter("@T_Casa", propiedad.T_Casa);
                SqlParameter T_Terreno = new SqlParameter("@T_Terreno", propiedad.T_Terreno);
                SqlParameter T_Local = new SqlParameter("@T_Local", propiedad.T_Local);
                SqlParameter T_Oficina = new SqlParameter("@T_Oficina", propiedad.T_Oficina);
                SqlParameter T_Bodega = new SqlParameter("@T_Bodega ", propiedad.T_Bodega);
                SqlParameter T_Edificio = new SqlParameter("@T_Edificio", propiedad.T_Edificio);
                SqlParameter T_Hotel = new SqlParameter("@T_Hotel", propiedad.T_Hotel);
                SqlParameter T_Condominio = new SqlParameter("@T_Condominio", propiedad.T_Condominio);
                SqlParameter T_Apartamento = new SqlParameter("@T_Apartamento ", propiedad.T_Apartamento);
                SqlParameter Ocupado = new SqlParameter("@Ocupado", propiedad.Ocupado);
                SqlParameter Llaves = new SqlParameter("@Llaves", propiedad.Llaves);
                SqlParameter MapsLat = new SqlParameter("@MapsLat ", propiedad.MapsLat);
                SqlParameter MapsLong = new SqlParameter("@MapsLong", propiedad.MapsLong);
                SqlParameter Estado = new SqlParameter("@Estado", propiedad.Estado);

                model.Database.ExecuteSqlCommand("sp_ActualizarPropiedadAgente @Codigo,@Nombre,@Agente,@LinkSitioWeb,@LinkGoogleMaps,@PrecioVenta,@PrecioAlquiler,@NoPlanoCatastro,@NoFolioReal,@AreaTerrenoM2,@AreaConstruccionM2,@Provincia,@Canton,@Distrito,@Direccion,@CuotaMantenimiento,@CuotaSeguridad,@ImpuestoSolidario,@Impuestos,@IVA,@CodigoPostal,@FrenteM2,@FondoM2,@AlturaM2,@Niveles,@Dormitorios,@Baños,@Pisos,@Cielos,@MedidorAgua,@Sala,@Comedor,@Cocina,@Desayunador,@Antecom,@SalaTV,@Bar,@Casino,@Terraza,@Balcon,@Lavanderia,@Despensa,@ClosetBlanco,@Walk_in_Closet,@Puertas_Baño,@Senderos,@Mascotas,@AreaMascotaM2,@Perrera,@Seguridad,@Oficina,@Bodega,@Garage,@Jardin,@Patio,@Patio_Luz,@Atico,@Piscina,@Rancho,@Canchas,@Gimnacio,@Lounge,@Jacuzzi,@Cortinas,@Lamparas,@Chimenea,@Cedazos,@InterCom,@PlantaElectrica,@Asensor,@Parqueo,@HotH20,@Cloaca,@Septico,@PlantaTratamiento,@Capt_Bomba,@Tels,@Cable,@Internet,@Alarma,@CCTV,@PortonElectrico,@Control,@CercaElectrica,@AlambreNavaja,@A_C,@LampEmergencia,@DetectorHumo,@SistemaIncendio,@SistemaSonido,@TableroDigital,@TableroElec,@Sobres,@Amueblado,@MueblesCocina,@Electrod,@Cerchas,@Cornizas,@Puertas,@ContraMarcos,@Ventaneria,@Cobertura,@Est_Suelo,@DispositivoH2O,@UsoSuelo,@Planos_Construccion,@Plano_Catastro,@Curvas_Nivel,@Avaluo,@Venta,@Alquiler,@T_Casa,@T_Terreno,@T_Local,@T_Oficina,@T_Bodega,@T_Edificio,@T_Hotel,@T_Condominio,@T_Apartamento,@Ocupado,@Llaves,@MapsLat,@MapsLong,@Estado",
                         codigo, nombre, agente, linkSitioWeb, linkGoogleMaps, precioVenta, precioAlquiler, noPlanoCatastro, NoFolioReal, AreaTerrenoM2, AreaConstruccionM2, Provincia, Canton, Distrito, Direccion, CuotaMantenimiento, CuotaSeguridad, ImpuestoSolidario, Impuestos, IVA, CodigoPostal, FrenteM2, FondoM2, AlturaM2, Niveles, Dormitorios, Baños, Pisos, Cielos, MedidorAgua, Sala, Comedor, Cocina, Desayunador, Antecom, SalaTV, Bar, Casino, Terraza, Balcon, Lavanderia, Despensa, ClosetBlanco, Walk_in_Closet, Puertas_Baño, Senderos, Mascotas, AreaMascotaM2, Perrera, Seguridad, Oficina, Bodega, Garage, Jardin, Patio, Patio_Luz, Atico, Piscina, Rancho, Canchas, Gimnacio, Lounge, Jacuzzi, Cortinas, Lamparas, Chimenea, Cedazos, InterCom, PlantaElectrica, Asensor, Parqueo, HotH20, Cloaca, Septico, PlantaTratamiento, Capt_Bomba, Tels, Cable, Internet, Alarma, CCTV, PortonElectrico, Control, CercaElectrica, AlambreNavaja, A_C, LampEmergencia, DetectorHumo, SistemaIncendio, SistemaSonido, TableroDigital, TableroElec, Sobres, Amueblado, MueblesCocina, Electrod, Cerchas, Cornizas, Puertas, ContraMarcos, Ventaneria, Cobertura, Est_Suelo, DispositivoH2O, UsoSuelo, Planos_Construccion, Plano_Catastro, Curvas_Nivel, Avaluo, Venta, Alquiler, T_Casa, T_Terreno, T_Local, T_Oficina, T_Bodega, T_Edificio, T_Hotel, T_Condominio, T_Apartamento, Ocupado, Llaves, MapsLat, MapsLong, Estado);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void sp_Eliminarpropiedad(Propiedade propiedad)
        {
            DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

            SqlParameter codigo = new SqlParameter("@Codigo", propiedad.Codigo);

            model.Database.ExecuteSqlCommand("sp_EliminarPropiedad @Codigo", codigo);
        }
    }
}