﻿function GuardarAgente() {
    $.ajax({
        type: 'POST',
        url: '/Agente/InsertarAgente',
        data:
        {
            tipoCedula: $("#tipoCedula").val(),
            cedula:     $("#cedula").val(),
            nombre:     $("#nombre").val(),
            apellido:   $("#apellido").val(),
            telefono:   $("#telefono").val(),
            correo:     $("#correo").val(),
            agencia:    $("#agencia").val(),
            InformacionAdicional: $("#info").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Agente/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}

var tipoCedula; var Cedula; var Nombre; var Apellido; var Telefono; var Correo; var Agencia; var Info;
function seleccionar(tip,ced,nomb,ape,tel,cor,agen,inf) {
    tipoCedula = tip
    Cedula = ced
    Nombre = nomb
    Apellido = ape
    Telefono = tel
    Correo = cor
    Agencia = agen
    Info = inf
}

function GetActualizarAgente() {
    var url = "/Agente/ActualizarAgente/?Cedula=" + Cedula
    console.log(url)
    window.location.href = url;
}

function GetActualizarAgente2(id) {
    var url = "/Agente/ActualizarAgente/?Cedula=" + id
    console.log(url)
    window.location.href = url;
}

function GetEliminarAgente() {
    var url = "/Agente/EliminarAgente/?Cedula=" + Cedula
    console.log(url)
    window.location.href = url;
}

function GetDetallesAgente() {
    var url = "/Agente/DetallesAgente/?Cedula=" + Cedula
    console.log(url)
    window.location.href = url;
}

function ActualizarAgente() {
    $.ajax({
        type: 'POST',
        url: '/Agente/ActualizarAgente',
        data:
        {
            tipoCedula: $("#tipoCedula").val(),
            cedula:     $("#cedula").val(),
            nombre:     $("#nombre").val(),
            apellido:   $("#apellido").val(),
            telefono:   $("#telefono").val(),
            correo:     $("#correo").val(),
            agencia: $("#agencia").val(),
            InformacionAdicional: $("#info").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Agente/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}

function EliminarAgente() {
    $.ajax({
        type: 'POST',
        url: '/Agente/EliminarAgente',
        data:
        {
            cedula: $("#cedula").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Agente/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}


var tipoCedulaP; var CedulaP; var NombreP; var ApellidoP; var TelefonoP; var CorreoP;
var InfoP; var NombreSecP; var CorreoSecP; var telefonoSecP; var parentezcoSecP;
function seleccionarPropietario(tip, ced, nomb, ape, tel, cor, inf, nombSec, corSec, telSec, parenSec) {
    tipoCedulaP = tip
    CedulaP = ced
    NombreP = nomb
    ApellidoP = ape
    TelefonoP = tel
    CorreoP = cor
    InfoP = inf
    NombreSecP = nombSec
    CorreoSecP = corSec
    telefonoSecP = telSec
    parentezcoSecP = parenSec
}

function GetActualizarPropietario() {
    var url = "/Propietarios/ActualizarPropietario/?Cedula=" + CedulaP
    console.log(url)
    window.location.href = url;
}

function GetActualizarPropietario2(id) {
    var url = "/Propietarios/ActualizarPropietario/?Cedula=" + id
    console.log(url)
    window.location.href = url;
}

function GetEliminarPropietario() {
    var url = "/Propietarios/EliminarPropietario/?Cedula=" + CedulaP
    console.log(url)
    window.location.href = url;
}

function GetDetallesPropietario() {
    var url = "/Propietarios/DetallesPropietario/?Cedula=" + CedulaP
    console.log(url)
    window.location.href = url;
}

function GuardarPropietario() {
    $.ajax({
        type: 'POST',
        url: '/Propietarios/InsertarPropietario',
        data:
        {
            tipoCedula:             $("#tipoCedula").val(),
            cedula:                 $("#cedula").val(),
            nombre:                 $("#nombre").val(),
            apellido:               $("#apellido").val(), 
            telefono:               $("#telefono").val(),
            correo:                 $("#correo").val(),
            InformacionAdicional:   $("#info").val(),
            NombreCompleto_Sec:     $("#nombreSec").val(),
            Email_Sec:              $("#emailSec").val(),
            Telefono_Sec:           $("#telefonoSec").val(),
            Parentezco_Sec:         $("#parentezcoSec").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Propietarios/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}

function ActualizarPropietario() {
    $.ajax({
        type: 'POST',
        url: '/Propietarios/ActualizarPropietario',
        data:
        {
            tipoCedula: $("#tipoCedula").val(),
            cedula:               $("#cedula").val(),
            nombre:               $("#nombre").val(),
            apellido:             $("#apellido").val(), 
            telefono:             $("#telefono").val(),
            correo:               $("#correo").val(),
            InformacionAdicional: $("#info").val(),
            NombreCompleto_Sec:   $("#nombreSec").val(),
            Email_Sec:            $("#emailSec").val(),
            Telefono_Sec:         $("#telefonoSec").val(),
            Parentezco_Sec:       $("#parentezcoSec").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Propietarios/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}

function EliminarPropietario() {
    $.ajax({
        type: 'POST',
        url: '/Propietarios/EliminarPropietario',
        data:
        {
            cedula: $("#cedula").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Propietarios/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}


var CedulaC; var NombreC; var ApellidoC; var TelefonoC; var CorreoC;
var PropiedadGrsC; var PropiedadC; var EstadoC; 
function seleccionarCliente(ced,prop) {
    CedulaC = ced
    PropiedadC=prop
}

function GetActualizarCliente() {
    var url = "/Cliente/ActualizarCliente/?Cedula=" + CedulaC + "&Propiedad=" + PropiedadC
    console.log(url)
    window.location.href = url;
}

function GetActualizarCliente2(id,prop) {
    var url = "/Cliente/ActualizarCliente/?Cedula=" + id + "&Propiedad=" + prop
    console.log(url)
    window.location.href = url;
}

function GetEliminarCliente() {
    var url = "/Cliente/EliminarCliente/?Cedula=" + CedulaC + "&Propiedad=" + PropiedadC
    console.log(url)
    window.location.href = url;
}

function GetDetallesCliente() {
    var url = "/Cliente/DetallesCliente/?Cedula=" + CedulaC + "&Propiedad=" + PropiedadC
    console.log(url)
    window.location.href = url;
}


function GuardarCliente() {
    var EstTemp = $("#estado").val()
    var Estado=false
    if (EstTemp = 1) {
        Estado = true
    }
    else
    {
        if (EstTemp=0) {
            Estado = false
        }
    }
    $.ajax({
        type: 'POST',
        url: '/Cliente/InsertarClientes',
        data:
        {
            cedula: $("#cedula").val(),
            nombre: $("#nombre").val(),
            apellido: $("#apellido").val(),
            telefono: $("#telefono").val(),
            correo: $("#correo").val(),
            Propiedad: $("#propiedad").val(),
            FechaInteres: $("#fecha").val(),
            estado: Estado
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Cliente/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}

function ActualizarCliente() {
    var EstTemp = $("#estado").val()
    var Estado = false
    if (EstTemp = 1) {
        Estado = true
    }
    else {
        if (EstTemp = 0) {
            Estado = false
        }
    }
    $.ajax({
        type: 'POST',
        url: '/Cliente/ActualizarCliente',
        data:
        {
            cedula: $("#cedula").val(),
            nombre: $("#nombre").val(),
            apellido: $("#apellido").val(),
            telefono: $("#telefono").val(),
            correo: $("#correo").val(),
            Propiedad: $("#propiedad").val(),
            FechaInteres: $("#fecha").val(),
            estado: Estado
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Cliente/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}

function EliminarCliente() {
    $.ajax({
        type: 'POST',
        url: '/Cliente/EliminarCliente',
        data:
        {
            cedula: $("#cedula").val(),
            Propiedad: $("#propiedad").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Cliente/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}

var CodigoP; 
function seleccionarPropiedad(codigo) {
    CodigoP = codigo
    console.log(CodigoP)
}

function GetActualizarPropiedad() {
    var url = "/Propiedadess/ActualizarPropiedad/?Codigo=" + CodigoP
    console.log(url)
    window.location.href = url;
}

function GetActualizarPropiedad2(cod) {
    console.log(cod)
    var url = "/Propiedadess/ActualizarPropiedad/?Codigo=" + cod
    console.log(url)
    window.location.href = url;
}

function GetEliminarPropiedad() {
    var url = "/Propiedadess/EliminarPropiedad/?Codigo=" + CodigoP 
    console.log(url)
    window.location.href = url;
}

function GetDetallesPropiedad() {
    var url = "/Propiedadess/DetallesPropiedad/?Codigo=" + CodigoP
    console.log(url)
    window.location.href = url;
}

var CodigoPA;
function seleccionarPropiedadAgente(codigo) {
    CodigoPA = codigo
    console.log(CodigoPA)
}

function GetActualizarPropiedadAgente() {
    var url = "/PropiedadesAgente/ActualizarPropiedad/?Codigo=" + CodigoPA
    console.log(url)
    window.location.href = url;
}

function GetActualizarPropiedadAgente2(cod) {
    var url = "/PropiedadesAgente/ActualizarPropiedad/?Codigo=" + id
    console.log(url)
    window.location.href = url;
}

function GetEliminarPropiedadAgente() {
    var url = "/PropiedadesAgente/EliminarPropiedad/?Codigo=" + CodigoPA
    console.log(url)
    window.location.href = url;
}

function GetDetallesPropiedadAgente() {
    var url = "/PropiedadesAgente/DetallesPropiedad/?Codigo=" + CodigoPA
    console.log(url)
    window.location.href = url;
}

var tipoCasa; var tipoTerreno; var tipoLocal; var tipoOficina; var tipoBodega; var tipoEdificio; var tipoHotel; var tipoCondominio; var tipoApartamento;


$("#tipoLocal").click(function () {
    tipoCasa = false
    tipoTerreno = false
    tipoLocal = true
    tipoOficina = false
    tipoBodega = false
    tipoEdificio = false
    tipoHotel = false
    tipoCondominio = false
    tipoApartamento = false
});

$("#tipoOficina").click(function () {
    tipoCasa = false
    tipoTerreno = false
    tipoLocal = false
    tipoOficina = true
    tipoBodega = false
    tipoEdificio = false
    tipoHotel = false
    tipoCondominio = false
    tipoApartamento = false
});

$("#tipoBodega").click(function () {
    tipoCasa = false
    tipoTerreno = false
    tipoLocal = false
    tipoOficina = false
    tipoBodega = true
    tipoEdificio = false
    tipoHotel = false
    tipoCondominio = false
    tipoApartamento = false
});

$("#tipoEdificio").click(function () {
    tipoCasa = false
    tipoTerreno = false
    tipoLocal = false
    tipoOficina = false
    tipoBodega = false
    tipoEdificio = true
    tipoHotel = false
    tipoCondominio = false
    tipoApartamento = false
});

$("#tipoHotel").click(function () {
    tipoCasa = false
    tipoTerreno = false
    tipoLocal = false
    tipoOficina = false
    tipoBodega = false
    tipoEdificio = false
    tipoHotel = true
    tipoCondominio = false
    tipoApartamento = false
});

$("#tipoCondominio").click(function () {
    tipoCasa = false
    tipoTerreno = false
    tipoLocal = false
    tipoOficina = false
    tipoBodega = false
    tipoEdificio = false
    tipoHotel = false
    tipoCondominio = true
    tipoApartamento = false
});

$("#tipoApartamento").click(function () {
    tipoCasa = false
    tipoTerreno = false
    tipoLocal = false
    tipoOficina = false
    tipoBodega = false
    tipoEdificio = false
    tipoHotel = false
    tipoCondominio = false
    tipoApartamento = true
});

$("#tipoCasa").click(function () {
    tipoCasa = true
    tipoTerreno = false
    tipoLocal = false
    tipoOficina = false
    tipoBodega = false
    tipoEdificio = false
    tipoHotel = false
    tipoCondominio = false
    console.log(tipoCasa)
    console.log(tipoTerreno)
    tipoApartamento = false
});

$("#tipoTerreno").click(function () {
    tipoCasa = false
    tipoTerreno = true
    tipoLocal = false
    tipoOficina = false
    tipoBodega = false
    tipoEdificio = false
    tipoHotel = false
    tipoCondominio = false
    console.log(tipoCasa)
    console.log(tipoTerreno)
    tipoApartamento = false
});


function GuardarPropiedad() {
    var medidorAguaTemp = $("input:checkbox[name=medidorAgua]:checked").val(); var medidorAgua = false
    if (medidorAguaTemp == 0 || medidorAguaTemp == "on") { medidorAgua = true } else { if (medidorAguaTemp == undefined) { medidorAgua = false } }

    var ventaTemp = $("input:checkbox[name=venta]:checked").val(); var venta = false
    if (ventaTemp == 0 || ventaTemp == "on") { venta = true } else { if (ventaTemp == undefined) { venta = false } }

    var alquilerTemp = $("input:checkbox[name=alquiler]:checked").val(); var alquiler = false
    if (alquilerTemp == 0 || alquilerTemp == "on") { alquiler = true } else { if (alquilerTemp == undefined) { alquiler = false } }

    var ocupadoTemp = $("input:checkbox[name=ocupado]:checked").val(); var ocupado = false
    if (ocupadoTemp == 0 || ocupadoTemp == "on") { ocupado = true } else { if (ocupadoTemp == undefined) { ocupado = false } }

    var salaTemp = $("input:checkbox[name=sala]:checked").val(); var sala = false
    if (salaTemp == 0 || salaTemp == "on") { sala = true } else { if (salaTemp == undefined) { sala = false } }

    var comedorTemp = $("input:checkbox[name=comedor]:checked").val(); var comedor = false
    if (comedorTemp == 0 || comedorTemp == "on") { comedor = true } else { if (comedorTemp == undefined) { comedor = false } }

    var cocinaTemp = $("input:checkbox[name=cocina]:checked").val(); var cocina = false
    if (cocinaTemp == 0 || cocinaTemp == "on") { cocina = true } else { if (cocinaTemp == undefined) { cocina = false } }

    var desayunadorTemp = $("input:checkbox[name=desayunador]:checked").val(); var desayunador = false
    if (desayunadorTemp == 0 || desayunadorTemp == "on") { desayunador = true } else { if (desayunadorTemp == undefined) { desayunador = false } }

    var antecomTemp = $("input:checkbox[name=antecom]:checked").val(); var antecom = false
    if (antecomTemp == 0 || antecomTemp == "on") { antecom = true } else { if (antecomTemp == undefined) { antecom = false } }

    var salaTVTemp = $("input:checkbox[name=salaTV]:checked").val(); var salaTV = false
    if (salaTVTemp == 0 || salaTVTemp == "on") { salaTV = true } else { if (salaTVTemp == undefined) { salaTV = false } }

    var barTemp = $("input:checkbox[name=bar]:checked").val(); var bar = false
    if (barTemp == 0 || barTemp == "on") { bar = true } else { if (barTemp == undefined) { bar = false } }

    var cacinoTemp = $("input:checkbox[name=cacino]:checked").val(); var cacino = false
    if (cacinoTemp == 0 || cacinoTemp == "on") { cacino = true } else { if (cacinoTemp == undefined) { cacino = false } }

    var terrazaTemp = $("input:checkbox[name=terraza]:checked").val(); var terraza = false
    if (terrazaTemp == 0 || terrazaTemp == "on") { terraza = true } else { if (terrazaTemp == undefined) { terraza = false } }

    var balconTemp = $("input:checkbox[name=balcon]:checked").val(); var balcon = false
    if (balconTemp == 0 || balconTemp == "on") { balcon = true } else { if (balconTemp == undefined) { balcon = false } }

    var lavanderiaTemp = $("input:checkbox[name=lavanderia]:checked").val(); var lavanderia = false
    if (lavanderiaTemp == 0 || lavanderiaTemp == "on") { lavanderia = true } else { if (lavanderiaTemp == undefined) { lavanderia = false } }

    var despensaTemp = $("input:checkbox[name=despensa]:checked").val(); var despensa = false
    if (despensaTemp == 0 || despensaTemp == "on") { despensa = true } else { if (despensaTemp == undefined) { despensa = false } }

    var closetBlancoTemp = $("input:checkbox[name=closetBlanco]:checked").val(); var closetBlanco = false
    if (closetBlancoTemp == 0 || closetBlancoTemp == "on") { closetBlanco = true } else { if (closetBlancoTemp == undefined) { closetBlanco = false } }

    var walkInClosetTemp = $("input:checkbox[name=walkInCloset]:checked").val(); var walkInCloset = false
    if (walkInClosetTemp == 0 || walkInCloset == "on") { walkInCloset = true } else { if (walkInClosetTemp == undefined) { walkInCloset = false } }

    var puertasBañoTemp = $("input:checkbox[name=puertasBaño]:checked").val(); var puertasBaño = false
    if (puertasBañoTemp == 0 || puertasBañoTemp == "on") { puertasBaño = true } else { if (puertasBañoTemp == undefined) { puertasBaño = false } }

    var senderosTemp = $("input:checkbox[name=senderos]:checked").val(); var senderos = false
    if (senderosTemp == 0 || senderosTemp == "on") { senderos = true } else { if (senderosTemp == undefined) { senderos = false } }

    var mascotasTemp = $("input:checkbox[name=mascotas]:checked").val(); var mascotas = false
    if (mascotasTemp == 0 || mascotasTemp == "on") { mascotas = true } else { if (mascotasTemp == undefined) { mascotas = false } }

    var perreraTemp = $("input:checkbox[name=perrera]:checked").val(); var perrera = false
    if (perreraTemp == 0 || perreraTemp == "on") { perrera = true } else { if (perreraTemp == undefined) { perrera = false } }

    var seguridadTemp = $("input:checkbox[name=seguridad]:checked").val(); var seguridad = false
    if (seguridadTemp == 0 || seguridadTemp == "on") { seguridad = true } else { if (seguridadTemp == undefined) { seguridad = false } }

    var oficinaTemp = $("input:checkbox[name=oficina]:checked").val(); var oficina = false
    if (oficinaTemp == 0 || oficinaTemp == "on") { oficina = true } else { if (oficinaTemp == undefined) { oficina = false } }

    var bodegaTemp = $("input:checkbox[name=bodega]:checked").val(); var bodega = false
    if (bodegaTemp == 0 || bodegaTemp == "on") { bodega = true } else { if (bodegaTemp == undefined) { bodega = false } }

    var garageTemp = $("input:checkbox[name=garage]:checked").val(); var garage = false
    if (garageTemp == 0 || garageTemp == "on") { garage = true } else { if (garageTemp == undefined) { garage = false } }

    var jardinTemp = $("input:checkbox[name=jardin]:checked").val(); var jardin = false
    if (jardinTemp == 0 || jardinTemp == "on") { jardin = true } else { if (jardinTemp == undefined) { jardin = false } }

    var patioTemp = $("input:checkbox[name=patio]:checked").val(); var patio = false
    if (patioTemp == 0 || patioTemp == "on") { patio = true } else { if (patioTemp == undefined) { patio = false } }

    var patioLuzTemp = $("input:checkbox[name=patioLuz]:checked").val(); var patioLuz = false
    if (patioLuzTemp == 0 || patioLuzTemp == "on") { patioLuz = true } else { if (patioLuzTemp == undefined) { patioLuz = false } }

    var aticoTemp = $("input:checkbox[name=atico]:checked").val(); var atico = false
    if (aticoTemp == 0 || aticoTemp == "on") { atico = true } else { if (aticoTemp == undefined) { atico = false } }

    var piscinaTemp = $("input:checkbox[name=piscina]:checked").val(); var piscina = false
    if (piscinaTemp == 0 || piscinaTemp == "on") { piscina = true } else { if (piscinaTemp == undefined) { piscina = false } }

    var ranchoTemp = $("input:checkbox[name=rancho]:checked").val(); var rancho = false
    if (ranchoTemp == 0 || ranchoTemp == "on") { rancho = true } else { if (ranchoTemp == undefined) { rancho = false } }

    var canchasTemp = $("input:checkbox[name=canchas]:checked").val(); var canchas = false
    if (canchasTemp == 0 || canchasTemp == "on") { canchas = true } else { if (canchasTemp == undefined) { canchas = false } }

    var cloacaTemp = $("input:checkbox[name=cloaca]:checked").val(); var cloaca = false
    if (cloacaTemp == 0 || cloacaTemp == "on") { cloaca = true } else { if (cloacaTemp == undefined) { cloaca = false } }

    var septicoTemp = $("input:checkbox[name=septico]:checked").val(); var septico = false
    if (septicoTemp == 0 || septicoTemp == "on") { septico = true } else { if (septicoTemp == undefined) { septico = false } }

    var plantaTratamientoTemp = $("input:checkbox[name=plantaTratamiento]:checked").val(); var plantaTratamiento = false
    if (plantaTratamientoTemp == 0 || planoCatastroTemp == "on") { plantaTratamiento = true } else { if (plantaTratamientoTemp == undefined) { plantaTratamiento = false } }

    var captBombaTemp = $("input:checkbox[name=captBomba]:checked").val(); var captBomba = false
    if (captBombaTemp == 0 || captBombaTemp == "on") { captBomba = true } else { if (captBombaTemp == undefined) { captBomba = false } }

    var telsTemp = $("input:checkbox[name=tels]:checked").val(); var tels = false
    if (telsTemp == 0 || telsTemp == "on") { tels = true } else { if (telsTemp == undefined) { tels = false } }

    var cableTemp = $("input:checkbox[name=cable]:checked").val(); var cable = false
    if (cableTemp == 0 || cableTemp == "on") { cable = true } else { if (cableTemp == undefined) { cable = false } }

    var internetTemp = $("input:checkbox[name=internet]:checked").val(); var internet = false
    if (internetTemp == 0 || internetTemp == "on") { internet = true } else { if (internetTemp == undefined) { internet = false } }

    var alarmaTemp = $("input:checkbox[name=alarma]:checked").val(); var alarma = false
    if (alarmaTemp == 0 || alarmaTemp == "on") { alarma = true } else { if (alarmaTemp == undefined) { alarma = false } }

    var cctvTemp = $("input:checkbox[name=cctv]:checked").val(); var cctv = false
    if (cctvTemp == 0 || cctvTemp == "on") { cctv = true } else { if (cctvTemp == undefined) { cctv = false } }

    var portonElectTemp = $("input:checkbox[name=portonElect]:checked").val(); var portonElect = false
    if (portonElectTemp == 0 || portonElectTemp == "on") { portonElect = true } else { if (portonElectTemp == undefined) { portonElect = false } }

    var controlTemp = $("input:checkbox[name=control]:checked").val(); var control = false
    if (controlTemp == 0 || controlTemp == "on") { control = true } else { if (controlTemp == undefined) { control = false } }

    var cercaElectricaTemp = $("input:checkbox[name=cercaElectrica]:checked").val(); var cercaElectrica = false
    if (cercaElectricaTemp == 0 || cercaElectricaTemp == "on") { cercaElectrica = true } else { if (cercaElectricaTemp == undefined) { cercaElectrica = false } }

    var alamNavajaTemp = $("input:checkbox[name=alamNavaja]:checked").val(); var alamNavaja = false
    if (alamNavajaTemp == 0 || alamNavajaTemp == "on") { alamNavaja = true } else { if (alamNavajaTemp == undefined) { alamNavaja = false } }

    var acTemp = $("input:checkbox[name=ac]:checked").val(); var ac = false
    if (acTemp == 0 || acTemp == "on") { ac = true } else { if (acTemp == undefined) { ac = false } }


    var detectorHumoTemp = $("input:checkbox[name=detectorHumo]:checked").val(); var detectorHumo = false
    if (detectorHumoTemp == 0 || detectorHumoTemp == "on") { detectorHumo = true } else { if (detectorHumoTemp == undefined) { detectorHumo = false } }

    var sistIncendioTemp = $("input:checkbox[name=sistIncendio]:checked").val(); var sistIncendio = false
    if (sistIncendioTemp == 0 || sistIncendioTemp == "on") { sistIncendio = true } else { if (sistIncendioTemp == undefined) { sistIncendio = false } }

    var sistSonidoTemp = $("input:checkbox[name=sistSonido]:checked").val(); var sistSonido = false
    if (sistSonidoTemp == 0 || sistSonidoTemp == "on") { sistSonido = true } else { if (sistSonidoTemp == undefined) { sistSonido = false } }

    var tableroDigitalTemp = $("input:checkbox[name=tableroDigital]:checked").val(); var tableroDigital = false
    if (tableroDigitalTemp == 0 || tableroDigitalTemp == "on") { tableroDigital = true } else { if (tableroDigitalTemp == undefined) { tableroDigital = false } }

    var tableroElecTemp = $("input:checkbox[name=tableroElec]:checked").val(); var tableroElec = false
    if (tableroElecTemp == 0 || tableroElecTemp == "on") { tableroElec = true } else { if (tableroElecTemp == undefined) { tableroElec = false } }

    var dispH20Temp = $("input:checkbox[name=dispH20]:checked").val(); var dispH20 = false
    if (dispH20Temp == 0 || dispH20Temp == "on") { dispH20 = true } else { if (dispH20Temp == undefined) { dispH20 = false } }

    var amuebladoTemp = $("input:checkbox[name=amueblado]:checked").val(); var amueblado = false
    if (amuebladoTemp == 0 || amuebladoTemp == "on") { amueblado = true } else { if (amuebladoTemp == undefined) { amueblado = false } }

    var gimnacioTemp = $("input:checkbox[name=gimnacio]:checked").val(); var gimnacio = false
    if (gimnacioTemp == 0 || gimnacioTemp == "on") { gimnacio = true } else { if (gimnacioTemp == undefined) { gimnacio = false } }

    var loungeTemp = $("input:checkbox[name=lounge]:checked").val(); var lounge = false
    if (loungeTemp == 0 || loungeTemp == "on") { lounge = true } else { if (loungeTemp == undefined) { lounge = false } }

    var jacuzziTemp = $("input:checkbox[name=jacuzzi]:checked").val(); var jacuzzi = false
    if (jacuzziTemp == 0 || jacuzziTemp == "on") { jacuzzi = true } else { if (jacuzziTemp == undefined) { jacuzzi = false } }

    var cortinasTemp = $("input:checkbox[name=cortinas]:checked").val(); var cortinas = false
    if (cortinasTemp == 0 || cortinasTemp == "on") { cortinas = true } else { if (cortinasTemp == undefined) { cortinas = false } }

    var lamparasTemp = $("input:checkbox[name=lamparas]:checked").val(); var lamparas = false
    if (lamparasTemp == 0 || lamparasTemp == "on") { lamparas = true } else { if (lamparasTemp == undefined) { lamparas = false } }

    var chimeneaTemp = $("input:checkbox[name=chimenea]:checked").val(); var chimenea = false
    if (chimeneaTemp == 0 || chimeneaTemp == "on") { chimenea = true } else { if (chimeneaTemp == undefined) { chimenea = false } }

    var cedazosTemp = $("input:checkbox[name=cedazos]:checked").val(); var cedazos = false
    if (cedazosTemp == 0 || cedazosTemp == "on") { cedazos = true } else { if (cedazosTemp == undefined) { cedazos = false } }

    var intercomTemp = $("input:checkbox[name=intercom]:checked").val(); var intercom = false
    if (intercomTemp == 0 || intercomTemp == "on") { intercom = true } else { if (intercomTemp == undefined) { intercom = false } }

    var plantElectricaTemp = $("input:checkbox[name=plantElectrica]:checked").val(); var plantElectrica = false
    if (plantElectricaTemp == 0 || plantaTratamientoTemp == "on") { plantElectrica = true } else { if (plantElectricaTemp == undefined) { plantElectrica = false } }

    var asensorTemp = $("input:checkbox[name=asensor]:checked").val(); var asensor = false
    if (asensorTemp == 0 || asensorTemp == "on") { asensor = true } else { if (asensorTemp == undefined) { asensor = false } }

    var usoSueloTemp = $("input:checkbox[name=usoSuelo]:checked").val(); var usoSuelo = false
    if (usoSueloTemp == 0 || usoSueloTemp == "on") { usoSuelo = true } else { if (usoSueloTemp == undefined) { usoSuelo = false } }

    var planoCatastroTemp = $("input:checkbox[name=planoCatastro]:checked").val(); var planoCatastro = false
    if (planoCatastroTemp == 0 || planoCatastroTemp == "on") { planoCatastro = true } else { if (planoCatastroTemp == undefined) { planoCatastro = false } }

    var planoConstruccionTemp = $("input:checkbox[name=planoConstruccion]:checked").val(); var planoConstruccion = false
    if (planoConstruccionTemp == 0 || planoConstruccionTemp == "on") { planoConstruccion = true } else { if (planoConstruccionTemp == undefined) { planoConstruccion = false } }

    $.ajax({
        type: 'POST',
        url: '/Propiedadess/InsertarPropiedad',
        data:
        {
            Codigo: $("#codigo").val(),
            Nombre: $("#nombre").val(),
            Propietario: $('#propietario').val(),
            LinkSitioWeb: $("#linkSitioWeb").val(),
            LinkGoogleMaps: $("#maps").val(),
            PrecioVenta: $("#precioVenta").val(),
            PrecioAlquiler: $("#precioAlquiler").val(),
            NoPlanoCatastro: $("#noPlanoCatastro").val(),
            NoFolioReal: $("#noFolioReal").val(),
            AreaTerrenoM2: $("#areaTerreno").val(),
            AreaConstruccionM2: $("#areaConstruccion").val(),
            Provincia: $("#provincia").val(),
            Canton: $("#canton").val(),
            Distrito: $("#distrito").val(),
            Direccion: $("#direccion").val(),
            CuotaMantenimiento: $("#cuotaMantenimiento").val(),
            CuotaSeguridad: $("#cuotaSeguridad").val(),
            ImpuestoSolidario: $("#impuestoSolidario").val(),
            Impuestos: $("#impuestos").val(),
            IVA: $("#iva").val(),
            CodigoPostal: $("#postal").val(),
            FrenteM2: $("#frente").val(),
            FondoM2: $("#fondo").val(),
            AlturaM2: $("#altura").val(),
            Niveles: $("#niveles").val(),
            Dormitorios: $("#dormitorios").val(),
            Baños: $("#baños").val(),
            Pisos: $("#pisos").val(),
            Cielos: $("#cielos").val(),
            MedidorAgua: medidorAgua,
            Sala: sala,
            Comedor: comedor,
            Cocina: cocina,
            Desayunador: desayunador,
            Antecom: antecom,
            SalaTV: salaTV,
            Bar: bar,
            Casino: cacino,
            Terraza: terraza,
            Balcon: balcon,
            Lavanderia: lavanderia,
            Despensa: despensa,
            ClosetBlanco: closetBlanco,
            Walk_in_Closet: walkInCloset,
            Puertas_Baño: puertasBaño,
            Senderos: senderos,
            Mascotas: mascotas,
            AreaMascotaM2: $("#areaMascota").val(),
            Perrera: perrera,
            Seguridad: seguridad,
            Oficina: oficina,
            Bodega: bodega,
            Garage: garage,
            Jardin: jardin,
            Patio: patio,
            Patio_Luz: patioLuz,
            Atico: atico,
            Piscina: piscina,
            Rancho: rancho,
            Canchas: canchas,
            Gimnacio: gimnacio,
            Lounge: lounge,
            Jacuzzi: jacuzzi,
            Cortinas: cortinas,
            Lamparas: $("#lamparas").val(),
            Chimenea: chimenea,
            Cedazos: cedazos,
            InterCom: intercom,
            PlantaElectrica: plantElectrica,
            Asensor: asensor,
            Parqueo: $("#parqueo").val(),
            HotH20: $("#hotH2O").val(),
            Cloaca: cloaca,
            Septico: septico,
            PlantaTratamiento: plantaTratamiento,
            Capt_Bomba: captBomba,
            Tels: tels,
            Cable: cable,
            Internet: internet,
            Alarma: alarma,
            CCTV: cctv,
            PortonElectrico: portonElect,
            Control: control,
            CercaElectrica: cercaElectrica,
            AlambreNavaja: alamNavaja,
            A_C: ac,
            LampEmergencia: $("#lampEmergencia").val(),
            DetectorHumo: detectorHumo,
            SistemaIncendio: sistIncendio,
            SistemaSonido: sistSonido,
            TableroDigital: tableroDigital,
            TableroElec: tableroElec,
            Sobres: $("#sobres").val(),
            Amueblado: amueblado,
            MueblesCocina: $("#mueblesCocina").val(),
            Electrod: $("#electrod").val(),
            Cerchas: $("#cerchas").val(),
            Cornizas: $("#cornizas").val(),
            Puertas: $("#puertas").val(),
            ContraMarcos: $("#contramarcos").val(),
            Ventaneria: $("#ventaneria").val(),
            Cobertura: $("#cobertura").val(),
            Est_Suelo: $("#estSuelo").val(),
            DispositivoH20: dispH20,
            UsoSuelo: usoSuelo,
            Planos_Construccion: planoConstruccion,
            Plano_Catastro: planoCatastro,
            Curvas_Nivel: $("#curvasNivel").val(),
            Avaluo: $("#avaluo").val(),
            Venta: venta,
            Alquiler: alquiler,
            T_Casa: tipoCasa,
            T_Terreno: tipoTerreno,
            T_Local: tipoLocal,
            T_Oficina: tipoOficina,
            T_Bodega: tipoBodega,
            T_Edificio: tipoEdificio,
            T_Hotel: tipoHotel,
            T_Condominio: tipoCondominio,
            T_Apartamento: tipoApartamento,
            Ocupado: ocupado,
            Llaves: $("#llaves").val(),
            MapsLat: $("#mapsLat").val(),
            MapsLong: $("#mapsLong").val(),
            Imagen: null,
            Estado: $("#estado").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Propiedadess/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}



var PropietariosActual;
function ActualizarPropiedad(prop) {
    var medidorAguaTemp = $("input:checkbox[name=medidorAgua]:checked").val(); var medidorAgua = false
    if (medidorAguaTemp == 0 || medidorAguaTemp == "on") { medidorAgua = true } else { if (medidorAguaTemp == undefined) { medidorAgua = false } }

    var ventaTemp = $("input:checkbox[name=venta]:checked").val(); var venta = false
    if (ventaTemp == 0 || ventaTemp == "on") { venta = true } else { if (ventaTemp == undefined) { venta = false } }

    var alquilerTemp = $("input:checkbox[name=alquiler]:checked").val(); var alquiler = false
    if (alquilerTemp == 0 || alquilerTemp == "on") { alquiler = true } else { if (alquilerTemp == undefined) { alquiler = false } }

    var ocupadoTemp = $("input:checkbox[name=ocupado]:checked").val(); var ocupado = false
    if (ocupadoTemp == 0 || ocupadoTemp == "on") { ocupado = true } else { if (ocupadoTemp == undefined) { ocupado = false } }

    var salaTemp = $("input:checkbox[name=sala]:checked").val(); var sala = false
    if (salaTemp == 0 || salaTemp == "on") { sala = true } else { if (salaTemp == undefined) { sala = false } }

    var comedorTemp = $("input:checkbox[name=comedor]:checked").val(); var comedor = false
    if (comedorTemp == 0 || comedorTemp == "on") { comedor = true } else { if (comedorTemp == undefined) { comedor = false } }

    var cocinaTemp = $("input:checkbox[name=cocina]:checked").val(); var cocina = false
    if (cocinaTemp == 0 || cocinaTemp == "on") { cocina = true } else { if (cocinaTemp == undefined) { cocina = false } }

    var desayunadorTemp = $("input:checkbox[name=desayunador]:checked").val(); var desayunador = false
    if (desayunadorTemp == 0 || desayunadorTemp == "on") { desayunador = true } else { if (desayunadorTemp == undefined) { desayunador = false } }

    var antecomTemp = $("input:checkbox[name=antecom]:checked").val(); var antecom = false
    if (antecomTemp == 0 || antecomTemp == "on") { antecom = true } else { if (antecomTemp == undefined) { antecom = false } }

    var salaTVTemp = $("input:checkbox[name=salaTV]:checked").val(); var salaTV = false
    if (salaTVTemp == 0 || salaTVTemp == "on") { salaTV = true } else { if (salaTVTemp == undefined) { salaTV = false } }

    var barTemp = $("input:checkbox[name=bar]:checked").val(); var bar = false
    if (barTemp == 0 || barTemp == "on") { bar = true } else { if (barTemp == undefined) { bar = false } }

    var cacinoTemp = $("input:checkbox[name=cacino]:checked").val(); var cacino = false
    if (cacinoTemp == 0 || cacinoTemp == "on") { cacino = true } else { if (cacinoTemp == undefined) { cacino = false } }

    var terrazaTemp = $("input:checkbox[name=terraza]:checked").val(); var terraza = false
    if (terrazaTemp == 0 || terrazaTemp == "on") { terraza = true } else { if (terrazaTemp == undefined) { terraza = false } }

    var balconTemp = $("input:checkbox[name=balcon]:checked").val(); var balcon = false
    if (balconTemp == 0 || balconTemp == "on") { balcon = true } else { if (balconTemp == undefined) { balcon = false } }

    var lavanderiaTemp = $("input:checkbox[name=lavanderia]:checked").val(); var lavanderia = false
    if (lavanderiaTemp == 0 || lavanderiaTemp == "on") { lavanderia = true } else { if (lavanderiaTemp == undefined) { lavanderia = false } }

    var despensaTemp = $("input:checkbox[name=despensa]:checked").val(); var despensa = false
    if (despensaTemp == 0 || despensaTemp == "on") { despensa = true } else { if (despensaTemp == undefined) { despensa = false } }

    var closetBlancoTemp = $("input:checkbox[name=closetBlanco]:checked").val(); var closetBlanco = false
    if (closetBlancoTemp == 0 || closetBlancoTemp == "on") { closetBlanco = true } else { if (closetBlancoTemp == undefined) { closetBlanco = false } }

    var walkInClosetTemp = $("input:checkbox[name=walkInCloset]:checked").val(); var walkInCloset = false
    if (walkInClosetTemp == 0 || walkInCloset == "on") { walkInCloset = true } else { if (walkInClosetTemp == undefined) { walkInCloset = false } }

    var puertasBañoTemp = $("input:checkbox[name=puertasBaño]:checked").val(); var puertasBaño = false
    if (puertasBañoTemp == 0 || puertasBañoTemp == "on") { puertasBaño = true } else { if (puertasBañoTemp == undefined) { puertasBaño = false } }

    var senderosTemp = $("input:checkbox[name=senderos]:checked").val(); var senderos = false
    if (senderosTemp == 0 || senderosTemp == "on") { senderos = true } else { if (senderosTemp == undefined) { senderos = false } }

    var mascotasTemp = $("input:checkbox[name=mascotas]:checked").val(); var mascotas = false
    if (mascotasTemp == 0 || mascotasTemp == "on") { mascotas = true } else { if (mascotasTemp == undefined) { mascotas = false } }

    var perreraTemp = $("input:checkbox[name=perrera]:checked").val(); var perrera = false
    if (perreraTemp == 0 || perreraTemp == "on") { perrera = true } else { if (perreraTemp == undefined) { perrera = false } }

    var seguridadTemp = $("input:checkbox[name=seguridad]:checked").val(); var seguridad = false
    if (seguridadTemp == 0 || seguridadTemp == "on") { seguridad = true } else { if (seguridadTemp == undefined) { seguridad = false } }

    var oficinaTemp = $("input:checkbox[name=oficina]:checked").val(); var oficina = false
    if (oficinaTemp == 0 || oficinaTemp == "on") { oficina = true } else { if (oficinaTemp == undefined) { oficina = false } }

    var bodegaTemp = $("input:checkbox[name=bodega]:checked").val(); var bodega = false
    if (bodegaTemp == 0 || bodegaTemp == "on") { bodega = true } else { if (bodegaTemp == undefined) { bodega = false } }

    var garageTemp = $("input:checkbox[name=garage]:checked").val(); var garage = false
    if (garageTemp == 0 || garageTemp == "on") { garage = true } else { if (garageTemp == undefined) { garage = false } }

    var jardinTemp = $("input:checkbox[name=jardin]:checked").val(); var jardin = false
    if (jardinTemp == 0 || jardinTemp == "on") { jardin = true } else { if (jardinTemp == undefined) { jardin = false } }

    var patioTemp = $("input:checkbox[name=patio]:checked").val(); var patio = false
    if (patioTemp == 0 || patioTemp == "on") { patio = true } else { if (patioTemp == undefined) { patio = false } }

    var patioLuzTemp = $("input:checkbox[name=patioLuz]:checked").val(); var patioLuz = false
    if (patioLuzTemp == 0 || patioLuzTemp == "on") { patioLuz = true } else { if (patioLuzTemp == undefined) { patioLuz = false } }

    var aticoTemp = $("input:checkbox[name=atico]:checked").val(); var atico = false
    if (aticoTemp == 0 || aticoTemp == "on") { atico = true } else { if (aticoTemp == undefined) { atico = false } }

    var piscinaTemp = $("input:checkbox[name=piscina]:checked").val(); var piscina = false
    if (piscinaTemp == 0 || piscinaTemp == "on") { piscina = true } else { if (piscinaTemp == undefined) { piscina = false } }

    var ranchoTemp = $("input:checkbox[name=rancho]:checked").val(); var rancho = false
    if (ranchoTemp == 0 || ranchoTemp == "on") { rancho = true } else { if (ranchoTemp == undefined) { rancho = false } }

    var canchasTemp = $("input:checkbox[name=canchas]:checked").val(); var canchas = false
    if (canchasTemp == 0 || canchasTemp == "on") { canchas = true } else { if (canchasTemp == undefined) { canchas = false } }

    var cloacaTemp = $("input:checkbox[name=cloaca]:checked").val(); var cloaca = false
    if (cloacaTemp == 0 || cloacaTemp == "on") { cloaca = true } else { if (cloacaTemp == undefined) { cloaca = false } }

    var septicoTemp = $("input:checkbox[name=septico]:checked").val(); var septico = false
    if (septicoTemp == 0 || septicoTemp == "on") { septico = true } else { if (septicoTemp == undefined) { septico = false } }

    var plantaTratamientoTemp = $("input:checkbox[name=plantaTratamiento]:checked").val(); var plantaTratamiento = false
    if (plantaTratamientoTemp == 0 || planoCatastroTemp == "on") { plantaTratamiento = true } else { if (plantaTratamientoTemp == undefined) { plantaTratamiento = false } }

    var captBombaTemp = $("input:checkbox[name=captBomba]:checked").val(); var captBomba = false
    if (captBombaTemp == 0 || captBombaTemp == "on") { captBomba = true } else { if (captBombaTemp == undefined) { captBomba = false } }

    var telsTemp = $("input:checkbox[name=tels]:checked").val(); var tels = false
    if (telsTemp == 0 || telsTemp == "on") { tels = true } else { if (telsTemp == undefined) { tels = false } }

    var cableTemp = $("input:checkbox[name=cable]:checked").val(); var cable = false
    if (cableTemp == 0 || cableTemp == "on") { cable = true } else { if (cableTemp == undefined) { cable = false } }

    var internetTemp = $("input:checkbox[name=internet]:checked").val(); var internet = false
    if (internetTemp == 0 || internetTemp == "on") { internet = true } else { if (internetTemp == undefined) { internet = false } }

    var alarmaTemp = $("input:checkbox[name=alarma]:checked").val(); var alarma = false
    if (alarmaTemp == 0 || alarmaTemp == "on") { alarma = true } else { if (alarmaTemp == undefined) { alarma = false } }

    var cctvTemp = $("input:checkbox[name=cctv]:checked").val(); var cctv = false
    if (cctvTemp == 0 || cctvTemp == "on") { cctv = true } else { if (cctvTemp == undefined) { cctv = false } }

    var portonElectTemp = $("input:checkbox[name=portonElect]:checked").val(); var portonElect = false
    if (portonElectTemp == 0 || portonElectTemp == "on") { portonElect = true } else { if (portonElectTemp == undefined) { portonElect = false } }

    var controlTemp = $("input:checkbox[name=control]:checked").val(); var control = false
    if (controlTemp == 0 || controlTemp == "on") { control = true } else { if (controlTemp == undefined) { control = false } }

    var cercaElectricaTemp = $("input:checkbox[name=cercaElectrica]:checked").val(); var cercaElectrica = false
    if (cercaElectricaTemp == 0 || cercaElectricaTemp == "on") { cercaElectrica = true } else { if (cercaElectricaTemp == undefined) { cercaElectrica = false } }

    var alamNavajaTemp = $("input:checkbox[name=alamNavaja]:checked").val(); var alamNavaja = false
    if (alamNavajaTemp == 0 || alamNavajaTemp == "on") { alamNavaja = true } else { if (alamNavajaTemp == undefined) { alamNavaja = false } }

    var acTemp = $("input:checkbox[name=ac]:checked").val(); var ac = false
    if (acTemp == 0 || acTemp == "on") { ac = true } else { if (acTemp == undefined) { ac = false } }


    var detectorHumoTemp = $("input:checkbox[name=detectorHumo]:checked").val(); var detectorHumo = false
    if (detectorHumoTemp == 0 || detectorHumoTemp == "on") { detectorHumo = true } else { if (detectorHumoTemp == undefined) { detectorHumo = false } }

    var sistIncendioTemp = $("input:checkbox[name=sistIncendio]:checked").val(); var sistIncendio = false
    if (sistIncendioTemp == 0 || sistIncendioTemp == "on") { sistIncendio = true } else { if (sistIncendioTemp == undefined) { sistIncendio = false } }

    var sistSonidoTemp = $("input:checkbox[name=sistSonido]:checked").val(); var sistSonido = false
    if (sistSonidoTemp == 0 || sistSonidoTemp == "on") { sistSonido = true } else { if (sistSonidoTemp == undefined) { sistSonido = false } }

    var tableroDigitalTemp = $("input:checkbox[name=tableroDigital]:checked").val(); var tableroDigital = false
    if (tableroDigitalTemp == 0 || tableroDigitalTemp == "on") { tableroDigital = true } else { if (tableroDigitalTemp == undefined) { tableroDigital = false } }

    var tableroElecTemp = $("input:checkbox[name=tableroElec]:checked").val(); var tableroElec = false
    if (tableroElecTemp == 0 || tableroElecTemp == "on") { tableroElec = true } else { if (tableroElecTemp == undefined) { tableroElec = false } }

    var dispH20Temp = $("input:checkbox[name=dispH20]:checked").val(); var dispH20 = false
    if (dispH20Temp == 0 || dispH20Temp == "on") { dispH20 = true } else { if (dispH20Temp == undefined) { dispH20 = false } }

    var amuebladoTemp = $("input:checkbox[name=amueblado]:checked").val(); var amueblado = false
    if (amuebladoTemp == 0 || amuebladoTemp == "on") { amueblado = true } else { if (amuebladoTemp == undefined) { amueblado = false } }

    var gimnacioTemp = $("input:checkbox[name=gimnacio]:checked").val(); var gimnacio = false
    if (gimnacioTemp == 0 || gimnacioTemp == "on") { gimnacio = true } else { if (gimnacioTemp == undefined) { gimnacio = false } }

    var loungeTemp = $("input:checkbox[name=lounge]:checked").val(); var lounge = false
    if (loungeTemp == 0 || loungeTemp == "on") { lounge = true } else { if (loungeTemp == undefined) { lounge = false } }

    var jacuzziTemp = $("input:checkbox[name=jacuzzi]:checked").val(); var jacuzzi = false
    if (jacuzziTemp == 0 || jacuzziTemp == "on") { jacuzzi = true } else { if (jacuzziTemp == undefined) { jacuzzi = false } }

    var cortinasTemp = $("input:checkbox[name=cortinas]:checked").val(); var cortinas = false
    if (cortinasTemp == 0 || cortinasTemp == "on") { cortinas = true } else { if (cortinasTemp == undefined) { cortinas = false } }

    var lamparasTemp = $("input:checkbox[name=lamparas]:checked").val(); var lamparas = false
    if (lamparasTemp == 0 || lamparasTemp == "on") { lamparas = true } else { if (lamparasTemp == undefined) { lamparas = false } }

    var chimeneaTemp = $("input:checkbox[name=chimenea]:checked").val(); var chimenea = false
    if (chimeneaTemp == 0 || chimeneaTemp == "on") { chimenea = true } else { if (chimeneaTemp == undefined) { chimenea = false } }

    var cedazosTemp = $("input:checkbox[name=cedazos]:checked").val(); var cedazos = false
    if (cedazosTemp == 0 || cedazosTemp == "on") { cedazos = true } else { if (cedazosTemp == undefined) { cedazos = false } }

    var intercomTemp = $("input:checkbox[name=intercom]:checked").val(); var intercom = false
    if (intercomTemp == 0 || intercomTemp == "on") { intercom = true } else { if (intercomTemp == undefined) { intercom = false } }

    var plantElectricaTemp = $("input:checkbox[name=plantElectrica]:checked").val(); var plantElectrica = false
    if (plantElectricaTemp == 0 || plantaTratamientoTemp == "on") { plantElectrica = true } else { if (plantElectricaTemp == undefined) { plantElectrica = false } }

    var asensorTemp = $("input:checkbox[name=asensor]:checked").val(); var asensor = false
    if (asensorTemp == 0 || asensorTemp == "on") { asensor = true } else { if (asensorTemp == undefined) { asensor = false } }

    var usoSueloTemp = $("input:checkbox[name=usoSuelo]:checked").val(); var usoSuelo = false
    if (usoSueloTemp == 0 || usoSueloTemp == "on") { usoSuelo = true } else { if (usoSueloTemp == undefined) { usoSuelo = false } }

    var planoCatastroTemp = $("input:checkbox[name=planoCatastro]:checked").val(); var planoCatastro = false
    if (planoCatastroTemp == 0 || planoCatastroTemp == "on") { planoCatastro = true } else { if (planoCatastroTemp == undefined) { planoCatastro = false } }

    var planoConstruccionTemp = $("input:checkbox[name=planoConstruccion]:checked").val(); var planoConstruccion = false
    if (planoConstruccionTemp == 0 || planoConstruccionTemp == "on") { planoConstruccion = true } else { if (planoConstruccionTemp == undefined) { planoConstruccion = false } }

    $.ajax({
        type: 'POST',
        url: '/Propiedadess/ActualizarPropiedad',
        data:
        {
            Codigo: $("#codigo").val(),
            Nombre: $("#nombre").val(),
            Propietario: parseInt(prop),
            LinkSitioWeb: $("#linkSitioWeb").val(),
            LinkGoogleMaps: $("#maps").val(),
            PrecioVenta: $("#precioVenta").val(),
            PrecioAlquiler: $("#precioAlquiler").val(),
            NoPlanoCatastro: $("#noPlanoCatastro").val(),
            NoFolioReal: $("#noFolioReal").val(),
            AreaTerrenoM2: $("#areaTerreno").val(),
            AreaConstruccionM2: $("#areaConstruccion").val(),
            Provincia: $("#provincia").val(),
            Canton: $("#canton").val(),
            Distrito: $("#distrito").val(),
            Direccion: $("#direccion").val(),
            CuotaMantenimiento: $("#cuotaMantenimiento").val(),
            CuotaSeguridad: $("#cuotaSeguridad").val(),
            ImpuestoSolidario: $("#impuestoSolidario").val(),
            Impuestos: $("#impuestos").val(),
            IVA: $("#iva").val(),
            CodigoPostal: $("#postal").val(),
            FrenteM2: $("#frente").val(),
            FondoM2: $("#fondo").val(),
            AlturaM2: $("#altura").val(),
            Niveles: $("#niveles").val(),
            Dormitorios: $("#dormitorios").val(),
            Baños: $("#baños").val(),
            Pisos: $("#pisos").val(),
            Cielos: $("#cielos").val(),
            MedidorAgua: medidorAgua,
            Sala: sala,
            Comedor: comedor,
            Cocina: cocina,
            Desayunador: desayunador,
            Antecom: antecom,
            SalaTV: salaTV,
            Bar: bar,
            Casino: cacino,
            Terraza: terraza,
            Balcon: balcon,
            Lavanderia: lavanderia,
            Despensa: despensa,
            ClosetBlanco: closetBlanco,
            Walk_in_Closet: walkInCloset,
            Puertas_Baño: puertasBaño,
            Senderos: senderos,
            Mascotas: mascotas,
            AreaMascotaM2: $("#areaMascota").val(),
            Perrera: perrera,
            Seguridad: seguridad,
            Oficina: oficina,
            Bodega: bodega,
            Garage: garage,
            Jardin: jardin,
            Patio: patio,
            Patio_Luz: patioLuz,
            Atico: atico,
            Piscina: piscina,
            Rancho: rancho,
            Canchas: canchas,
            Gimnacio: gimnacio,
            Lounge: lounge,
            Jacuzzi: jacuzzi,
            Cortinas: cortinas,
            Lamparas: $("#lamparas").val(),
            Chimenea: chimenea,
            Cedazos: cedazos,
            InterCom: intercom,
            PlantaElectrica: plantElectrica,
            Asensor: asensor,
            Parqueo: $("#parqueo").val(),
            HotH20: $("#hotH2O").val(),
            Cloaca: cloaca,
            Septico: septico,
            PlantaTratamiento: plantaTratamiento,
            Capt_Bomba: captBomba,
            Tels: tels,
            Cable: cable,
            Internet: internet,
            Alarma: alarma,
            CCTV: cctv,
            PortonElectrico: portonElect,
            Control: control,
            CercaElectrica: cercaElectrica,
            AlambreNavaja: alamNavaja,
            A_C: ac,
            LampEmergencia: $("#lamparasEmergencia").val(),
            DetectorHumo: detectorHumo,
            SistemaIncendio: sistIncendio,
            SistemaSonido: sistSonido,
            TableroDigital: tableroDigital,
            TableroElec: tableroElec,
            Sobres: $("#sobres").val(),
            Amueblado: amueblado,
            MueblesCocina: $("#mueblesCocina").val(),
            Electrod: $("#electrod").val(),
            Cerchas: $("#cerchas").val(),
            Cornizas: $("#cornizas").val(),
            Puertas: $("#puertas").val(),
            ContraMarcos: $("#contramarcos").val(),
            Ventaneria: $("#ventaneria").val(),
            Cobertura: $("#cobertura").val(),
            Est_Suelo: $("#estSuelo").val(),
            DispositivoH20: dispH20,
            UsoSuelo: usoSuelo,
            Planos_Construccion: planoConstruccion,
            Plano_Catastro: planoCatastro,
            Curvas_Nivel: $("#curvasNivel").val(),
            Avaluo: $("#avaluo").val(),
            Venta: venta,
            Alquiler: alquiler,
            T_Casa: tipoCasa,
            T_Terreno: tipoTerreno,
            T_Local: tipoLocal,
            T_Oficina: tipoOficina,
            T_Bodega: tipoBodega,
            T_Edificio: tipoEdificio,
            T_Hotel: tipoHotel,
            T_Condominio: tipoCondominio,
            T_Apartamento: tipoApartamento,
            Ocupado: ocupado,
            Llaves: $("#llaves").val(),
            MapsLat: $("#mapsLat").val(),
            MapsLong: $("#mapsLong").val(),
            Imagen: null,
            Estado: $("#estado").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Propiedadess/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}


function EliminarPropiedad() {
    $.ajax({
        type: 'POST',
        url: '/Propiedadess/EliminarPropiedad',
        data:
        {
            Codigo: $("#codigo").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/Propiedadess/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}



function GuardarPropiedadAgente() {
    var medidorAguaTemp = $("input:checkbox[name=medidorAgua]:checked").val(); var medidorAgua = false
    if (medidorAguaTemp == 0 || medidorAguaTemp == "on" ) { medidorAgua = true } else { if (medidorAguaTemp == undefined) { medidorAgua = false } }

    var ventaTemp = $("input:checkbox[name=venta]:checked").val(); var venta = false
    if (ventaTemp == 0 || ventaTemp=="on") { venta = true } else { if (ventaTemp == undefined) { venta = false } }

    var alquilerTemp = $("input:checkbox[name=alquiler]:checked").val(); var alquiler = false
    if (alquilerTemp == 0 || alquilerTemp=="on") { alquiler = true } else { if (alquilerTemp == undefined) { alquiler = false } }

    var ocupadoTemp = $("input:checkbox[name=ocupado]:checked").val(); var ocupado = false
    if (ocupadoTemp == 0 || ocupadoTemp=="on") { ocupado = true } else { if (ocupadoTemp == undefined) { ocupado = false } }

    var salaTemp = $("input:checkbox[name=sala]:checked").val(); var sala = false
    if (salaTemp == 0 || salaTemp=="on") { sala = true } else { if (salaTemp == undefined) { sala = false } }

    var comedorTemp = $("input:checkbox[name=comedor]:checked").val(); var comedor = false
    if (comedorTemp == 0 || comedorTemp=="on") { comedor = true } else { if (comedorTemp == undefined) { comedor = false } }

    var cocinaTemp = $("input:checkbox[name=cocina]:checked").val(); var cocina = false
    if (cocinaTemp == 0 || cocinaTemp=="on") { cocina = true } else { if (cocinaTemp == undefined) { cocina = false } }

    var desayunadorTemp = $("input:checkbox[name=desayunador]:checked").val(); var desayunador = false
    if (desayunadorTemp == 0 || desayunadorTemp=="on") { desayunador = true } else { if (desayunadorTemp == undefined) { desayunador = false } }

    var antecomTemp = $("input:checkbox[name=antecom]:checked").val(); var antecom = false
    if (antecomTemp == 0 || antecomTemp=="on") { antecom = true } else { if (antecomTemp == undefined) { antecom = false } }

    var salaTVTemp = $("input:checkbox[name=salaTV]:checked").val(); var salaTV = false
    if (salaTVTemp == 0 || salaTVTemp=="on") { salaTV = true } else { if (salaTVTemp == undefined) { salaTV = false } }

    var barTemp = $("input:checkbox[name=bar]:checked").val(); var bar = false
    if (barTemp == 0 || barTemp=="on") { bar = true } else { if (barTemp == undefined) { bar = false } }

    var cacinoTemp = $("input:checkbox[name=cacino]:checked").val(); var cacino = false
    if (cacinoTemp == 0 || cacinoTemp=="on") { cacino = true } else { if (cacinoTemp == undefined) { cacino = false } }

    var terrazaTemp = $("input:checkbox[name=terraza]:checked").val(); var terraza = false
    if (terrazaTemp == 0 || terrazaTemp=="on") { terraza = true } else { if (terrazaTemp == undefined) { terraza = false } }

    var balconTemp = $("input:checkbox[name=balcon]:checked").val(); var balcon = false
    if (balconTemp == 0 || balconTemp=="on") { balcon = true } else { if (balconTemp == undefined) { balcon = false } }

    var lavanderiaTemp = $("input:checkbox[name=lavanderia]:checked").val(); var lavanderia = false
    if (lavanderiaTemp == 0 || lavanderiaTemp=="on") { lavanderia = true } else { if (lavanderiaTemp == undefined) { lavanderia = false } }

    var despensaTemp = $("input:checkbox[name=despensa]:checked").val(); var despensa = false
    if (despensaTemp == 0 || despensaTemp=="on") { despensa = true } else { if (despensaTemp == undefined) { despensa = false } }

    var closetBlancoTemp = $("input:checkbox[name=closetBlanco]:checked").val(); var closetBlanco = false
    if (closetBlancoTemp == 0 || closetBlancoTemp=="on") { closetBlanco = true } else { if (closetBlancoTemp == undefined) { closetBlanco = false } }

    var walkInClosetTemp = $("input:checkbox[name=walkInCloset]:checked").val(); var walkInCloset = false
    if (walkInClosetTemp == 0 || walkInCloset=="on") { walkInCloset = true } else { if (walkInClosetTemp == undefined) { walkInCloset = false } }

    var puertasBañoTemp = $("input:checkbox[name=puertasBaño]:checked").val(); var puertasBaño = false
    if (puertasBañoTemp == 0 || puertasBañoTemp=="on") { puertasBaño = true } else { if (puertasBañoTemp == undefined) { puertasBaño = false } }

    var senderosTemp = $("input:checkbox[name=senderos]:checked").val(); var senderos = false
    if (senderosTemp == 0 || senderosTemp=="on") { senderos = true } else { if (senderosTemp == undefined) { senderos = false } }

    var mascotasTemp = $("input:checkbox[name=mascotas]:checked").val(); var mascotas = false
    if (mascotasTemp == 0 || mascotasTemp=="on") { mascotas = true } else { if (mascotasTemp == undefined) { mascotas = false } }

    var perreraTemp = $("input:checkbox[name=perrera]:checked").val(); var perrera = false
    if (perreraTemp == 0 || perreraTemp=="on") { perrera = true } else { if (perreraTemp == undefined) { perrera = false } }

    var seguridadTemp = $("input:checkbox[name=seguridad]:checked").val(); var seguridad = false
    if (seguridadTemp == 0 || seguridadTemp=="on") { seguridad = true } else { if (seguridadTemp == undefined) { seguridad = false } }

    var oficinaTemp = $("input:checkbox[name=oficina]:checked").val(); var oficina = false
    if (oficinaTemp == 0 || oficinaTemp=="on") { oficina = true } else { if (oficinaTemp == undefined) { oficina = false } }

    var bodegaTemp = $("input:checkbox[name=bodega]:checked").val(); var bodega = false
    if (bodegaTemp == 0 || bodegaTemp=="on") { bodega = true } else { if (bodegaTemp == undefined) { bodega = false } }

    var garageTemp = $("input:checkbox[name=garage]:checked").val(); var garage = false
    if (garageTemp == 0 || garageTemp=="on") { garage = true } else { if (garageTemp == undefined) { garage = false } }

    var jardinTemp = $("input:checkbox[name=jardin]:checked").val(); var jardin = false
    if (jardinTemp == 0 || jardinTemp=="on") { jardin = true } else { if (jardinTemp == undefined) { jardin = false } }

    var patioTemp = $("input:checkbox[name=patio]:checked").val(); var patio = false
    if (patioTemp == 0 || patioTemp=="on") { patio = true } else { if (patioTemp == undefined) { patio = false } }

    var patioLuzTemp = $("input:checkbox[name=patioLuz]:checked").val(); var patioLuz = false
    if (patioLuzTemp == 0 || patioLuzTemp=="on") { patioLuz = true } else { if (patioLuzTemp == undefined) { patioLuz = false } }

    var aticoTemp = $("input:checkbox[name=atico]:checked").val(); var atico = false
    if (aticoTemp == 0 || aticoTemp=="on") { atico = true } else { if (aticoTemp == undefined) { atico = false } }

    var piscinaTemp = $("input:checkbox[name=piscina]:checked").val(); var piscina = false
    if (piscinaTemp == 0 || piscinaTemp=="on") { piscina = true } else { if (piscinaTemp == undefined) { piscina = false } }

    var ranchoTemp = $("input:checkbox[name=rancho]:checked").val(); var rancho = false
    if (ranchoTemp == 0 || ranchoTemp=="on") { rancho = true } else { if (ranchoTemp == undefined) { rancho = false } }

    var canchasTemp = $("input:checkbox[name=canchas]:checked").val(); var canchas = false
    if (canchasTemp == 0 || canchasTemp=="on") { canchas = true } else { if (canchasTemp == undefined) { canchas = false } }

    var cloacaTemp = $("input:checkbox[name=cloaca]:checked").val(); var cloaca = false
    if (cloacaTemp == 0 || cloacaTemp=="on") { cloaca = true } else { if (cloacaTemp == undefined) { cloaca = false } }

    var septicoTemp = $("input:checkbox[name=septico]:checked").val(); var septico = false
    if (septicoTemp == 0 || septicoTemp=="on") { septico = true } else { if (septicoTemp == undefined) { septico = false } }

    var plantaTratamientoTemp = $("input:checkbox[name=plantaTratamiento]:checked").val(); var plantaTratamiento = false
    if (plantaTratamientoTemp == 0 || planoCatastroTemp=="on") { plantaTratamiento = true } else { if (plantaTratamientoTemp == undefined) { plantaTratamiento = false } }

    var captBombaTemp = $("input:checkbox[name=captBomba]:checked").val(); var captBomba = false
    if (captBombaTemp == 0 || captBombaTemp=="on") { captBomba = true } else { if (captBombaTemp == undefined) { captBomba = false } }

    var telsTemp = $("input:checkbox[name=tels]:checked").val(); var tels = false
    if (telsTemp == 0 || telsTemp=="on") { tels = true } else { if (telsTemp == undefined) { tels = false } }

    var cableTemp = $("input:checkbox[name=cable]:checked").val(); var cable = false
    if (cableTemp == 0 || cableTemp=="on") { cable = true } else { if (cableTemp == undefined) { cable = false } }

    var internetTemp = $("input:checkbox[name=internet]:checked").val(); var internet = false
    if (internetTemp == 0 || internetTemp=="on") { internet = true } else { if (internetTemp == undefined) { internet = false } }

    var alarmaTemp = $("input:checkbox[name=alarma]:checked").val(); var alarma = false
    if (alarmaTemp == 0 || alarmaTemp=="on") { alarma = true } else { if (alarmaTemp == undefined) { alarma = false } }

    var cctvTemp = $("input:checkbox[name=cctv]:checked").val(); var cctv = false
    if (cctvTemp == 0 || cctvTemp=="on") { cctv = true } else { if (cctvTemp == undefined) { cctv = false } }

    var portonElectTemp = $("input:checkbox[name=portonElect]:checked").val(); var portonElect = false
    if (portonElectTemp == 0 || portonElectTemp=="on") { portonElect = true } else { if (portonElectTemp == undefined) { portonElect = false } }

    var controlTemp = $("input:checkbox[name=control]:checked").val(); var control = false
    if (controlTemp == 0 || controlTemp=="on") { control = true } else { if (controlTemp == undefined) { control = false } }

    var cercaElectricaTemp = $("input:checkbox[name=cercaElectrica]:checked").val(); var cercaElectrica = false
    if (cercaElectricaTemp == 0 || cercaElectricaTemp=="on") { cercaElectrica = true } else { if (cercaElectricaTemp == undefined) { cercaElectrica = false } }

    var alamNavajaTemp = $("input:checkbox[name=alamNavaja]:checked").val(); var alamNavaja = false
    if (alamNavajaTemp == 0 || alamNavajaTemp=="on") { alamNavaja = true } else { if (alamNavajaTemp == undefined) { alamNavaja = false } }

    var acTemp = $("input:checkbox[name=ac]:checked").val(); var ac = false
    if (acTemp == 0 || acTemp=="on") { ac = true } else { if (acTemp == undefined) { ac = false } }


    var detectorHumoTemp = $("input:checkbox[name=detectorHumo]:checked").val(); var detectorHumo = false
    if (detectorHumoTemp == 0 || detectorHumoTemp=="on") { detectorHumo = true } else { if (detectorHumoTemp == undefined) { detectorHumo = false } }

    var sistIncendioTemp = $("input:checkbox[name=sistIncendio]:checked").val(); var sistIncendio = false
    if (sistIncendioTemp == 0 || sistIncendioTemp=="on") { sistIncendio = true } else { if (sistIncendioTemp == undefined) { sistIncendio = false } }

    var sistSonidoTemp = $("input:checkbox[name=sistSonido]:checked").val(); var sistSonido = false
    if (sistSonidoTemp == 0 || sistSonidoTemp=="on") { sistSonido = true } else { if (sistSonidoTemp == undefined) { sistSonido = false } }

    var tableroDigitalTemp = $("input:checkbox[name=tableroDigital]:checked").val(); var tableroDigital = false
    if (tableroDigitalTemp == 0 || tableroDigitalTemp=="on") { tableroDigital = true } else { if (tableroDigitalTemp == undefined) { tableroDigital = false } }

    var tableroElecTemp = $("input:checkbox[name=tableroElec]:checked").val(); var tableroElec = false
    if (tableroElecTemp == 0 || tableroElecTemp=="on") { tableroElec = true } else { if (tableroElecTemp == undefined) { tableroElec = false } }

    var dispH20Temp = $("input:checkbox[name=dispH20]:checked").val(); var dispH20 = false
    if (dispH20Temp == 0 || dispH20Temp=="on") { dispH20 = true } else { if (dispH20Temp == undefined) { dispH20 = false } }

    var amuebladoTemp = $("input:checkbox[name=amueblado]:checked").val(); var amueblado = false
    if (amuebladoTemp == 0 || amuebladoTemp=="on") { amueblado = true } else { if (amuebladoTemp == undefined) { amueblado = false } }

    var gimnacioTemp = $("input:checkbox[name=gimnacio]:checked").val(); var gimnacio = false
    if (gimnacioTemp == 0 || gimnacioTemp=="on") { gimnacio = true } else { if (gimnacioTemp == undefined) { gimnacio = false } }

    var loungeTemp = $("input:checkbox[name=lounge]:checked").val(); var lounge = false
    if (loungeTemp == 0 || loungeTemp=="on") { lounge = true } else { if (loungeTemp == undefined) { lounge = false } }

    var jacuzziTemp = $("input:checkbox[name=jacuzzi]:checked").val(); var jacuzzi = false
    if (jacuzziTemp == 0 || jacuzziTemp=="on") { jacuzzi = true } else { if (jacuzziTemp == undefined) { jacuzzi = false } }

    var cortinasTemp = $("input:checkbox[name=cortinas]:checked").val(); var cortinas = false
    if (cortinasTemp == 0 || cortinasTemp=="on") { cortinas = true } else { if (cortinasTemp == undefined) { cortinas = false } }

    var lamparasTemp = $("input:checkbox[name=lamparas]:checked").val(); var lamparas = false
    if (lamparasTemp == 0 || lamparasTemp=="on") { lamparas = true } else { if (lamparasTemp == undefined) { lamparas = false } }

    var chimeneaTemp = $("input:checkbox[name=chimenea]:checked").val(); var chimenea = false
    if (chimeneaTemp == 0 || chimeneaTemp=="on") { chimenea = true } else { if (chimeneaTemp == undefined) { chimenea = false } }

    var cedazosTemp = $("input:checkbox[name=cedazos]:checked").val(); var cedazos = false
    if (cedazosTemp == 0 || cedazosTemp=="on") { cedazos = true } else { if (cedazosTemp == undefined) { cedazos = false } }

    var intercomTemp = $("input:checkbox[name=intercom]:checked").val(); var intercom = false
    if (intercomTemp == 0 || intercomTemp=="on") { intercom = true } else { if (intercomTemp == undefined) { intercom = false } }

    var plantElectricaTemp = $("input:checkbox[name=plantElectrica]:checked").val(); var plantElectrica = false
    if (plantElectricaTemp == 0 || plantaTratamientoTemp=="on") { plantElectrica = true } else { if (plantElectricaTemp == undefined) { plantElectrica = false } }

    var asensorTemp = $("input:checkbox[name=asensor]:checked").val(); var asensor = false
    if (asensorTemp == 0 || asensorTemp=="on") { asensor = true } else { if (asensorTemp == undefined) { asensor = false } }

    var usoSueloTemp = $("input:checkbox[name=usoSuelo]:checked").val(); var usoSuelo = false
    if (usoSueloTemp == 0 || usoSueloTemp=="on") { usoSuelo = true } else { if (usoSueloTemp == undefined) { usoSuelo = false } }

    var planoCatastroTemp = $("input:checkbox[name=planoCatastro]:checked").val(); var planoCatastro = false
    if (planoCatastroTemp == 0 || planoCatastroTemp=="on") { planoCatastro = true } else { if (planoCatastroTemp == undefined) { planoCatastro = false } }

    var planoConstruccionTemp = $("input:checkbox[name=planoConstruccion]:checked").val(); var planoConstruccion = false
    if (planoConstruccionTemp == 0 || planoConstruccionTemp=="on") { planoConstruccion = true } else { if (planoConstruccionTemp == undefined) { planoConstruccion = false } }

    $.ajax({
        type: 'POST',
        url: '/PropiedadesAgente/InsertarPropiedad',
        data:
        {
            Codigo: $("#codigo").val(),
            Nombre: $("#nombre").val(),
            Agente: $('#agente').val(),
            LinkSitioWeb: $("#linkSitioWeb").val(),
            LinkGoogleMaps: $("#maps").val(),
            PrecioVenta: $("#precioVenta").val(),
            PrecioAlquiler: $("#precioAlquiler").val(),
            NoPlanoCatastro: $("#noPlanoCatastro").val(),
            NoFolioReal: $("#noFolioReal").val(),
            AreaTerrenoM2: $("#areaTerreno").val(),
            AreaConstruccionM2: $("#areaConstruccion").val(),
            Provincia: $("#provincia").val(),
            Canton: $("#canton").val(),
            Distrito: $("#distrito").val(),
            Direccion: $("#direccion").val(),
            CuotaMantenimiento: $("#cuotaMantenimiento").val(),
            CuotaSeguridad: $("#cuotaSeguridad").val(),
            ImpuestoSolidario: $("#impuestoSolidario").val(),
            Impuestos: $("#impuestos").val(),
            IVA: $("#iva").val(),
            CodigoPostal: $("#postal").val(),
            FrenteM2: $("#frente").val(),
            FondoM2: $("#fondo").val(),
            AlturaM2: $("#altura").val(),
            Niveles: $("#niveles").val(),
            Dormitorios: $("#dormitorios").val(),
            Baños: $("#baños").val(),
            Pisos: $("#pisos").val(),
            Cielos: $("#cielos").val(),
            MedidorAgua: medidorAgua,
            Sala: sala,
            Comedor: comedor,
            Cocina: cocina,
            Desayunador: desayunador,
            Antecom: antecom,
            SalaTV: salaTV,
            Bar: bar,
            Casino: cacino,
            Terraza: terraza,
            Balcon: balcon,
            Lavanderia: lavanderia,
            Despensa: despensa,
            ClosetBlanco: closetBlanco,
            Walk_in_Closet: walkInCloset,
            Puertas_Baño: puertasBaño,
            Senderos: senderos,
            Mascotas: mascotas,
            AreaMascotaM2: $("#areaMascota").val(),
            Perrera: perrera,
            Seguridad: seguridad,
            Oficina: oficina,
            Bodega: bodega,
            Garage: garage,
            Jardin: jardin,
            Patio: patio,
            Patio_Luz: patioLuz,
            Atico: atico,
            Piscina: piscina,
            Rancho: rancho,
            Canchas: canchas,
            Gimnacio: gimnacio,
            Lounge: lounge,
            Jacuzzi: jacuzzi,
            Cortinas: cortinas,
            Lamparas: $("#lamparas").val(),
            Chimenea: chimenea,
            Cedazos: cedazos,
            InterCom: intercom,
            PlantaElectrica: plantElectrica,
            Asensor: asensor,
            Parqueo: $("#parqueo").val(),
            HotH20: $("#hotH2O").val(),
            Cloaca: cloaca,
            Septico: septico,
            PlantaTratamiento: plantaTratamiento,
            Capt_Bomba: captBomba,
            Tels: tels,
            Cable: cable,
            Internet: internet,
            Alarma: alarma,
            CCTV: cctv,
            PortonElectrico: portonElect,
            Control: control,
            CercaElectrica: cercaElectrica,
            AlambreNavaja: alamNavaja,
            A_C: ac,
            LampEmergencia: $("#lampEmergencia").val(),
            DetectorHumo: detectorHumo,
            SistemaIncendio: sistIncendio,
            SistemaSonido: sistSonido,
            TableroDigital: tableroDigital,
            TableroElec: tableroElec,
            Sobres: $("#sobres").val(),
            Amueblado: amueblado,
            MueblesCocina: $("#mueblesCocina").val(),
            Electrod: $("#electrod").val(),
            Cerchas: $("#cerchas").val(),
            Cornizas: $("#cornizas").val(),
            Puertas: $("#puertas").val(),
            ContraMarcos: $("#contramarcos").val(),
            Ventaneria: $("#ventaneria").val(),
            Cobertura: $("#cobertura").val(),
            Est_Suelo: $("#estSuelo").val(),
            DispositivoH20: dispH20,
            UsoSuelo: usoSuelo,
            Planos_Construccion: planoConstruccion,
            Plano_Catastro: planoCatastro,
            Curvas_Nivel: $("#curvasNivel").val(),
            Avaluo: $("#avaluo").val(),
            Venta: venta,
            Alquiler: alquiler,
            T_Casa: tipoCasa,
            T_Terreno: tipoTerreno,
            T_Local: tipoLocal,
            T_Oficina: tipoOficina,
            T_Bodega: tipoBodega,
            T_Edificio: tipoEdificio,
            T_Hotel: tipoHotel,
            T_Condominio: tipoCondominio,
            T_Apartamento: tipoApartamento,
            Ocupado: ocupado,
            Llaves: $("#llaves").val(),
            MapsLat: $("#mapsLat").val(),
            MapsLong: $("#mapsLong").val(),
            Imagen: null,
            Estado: $("#estado").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/PropiedadesAgente/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}

function ActualizarPropiedadAgente(prop) {
    var medidorAguaTemp = $("input:checkbox[name=medidorAgua]:checked").val(); var medidorAgua = false
    if (medidorAguaTemp == 0 || medidorAguaTemp == "on") { medidorAgua = true } else { if (medidorAguaTemp == undefined) { medidorAgua = false } }

    var ventaTemp = $("input:checkbox[name=venta]:checked").val(); var venta = false
    if (ventaTemp == 0 || ventaTemp == "on") { venta = true } else { if (ventaTemp == undefined) { venta = false } }

    var alquilerTemp = $("input:checkbox[name=alquiler]:checked").val(); var alquiler = false
    if (alquilerTemp == 0 || alquilerTemp == "on") { alquiler = true } else { if (alquilerTemp == undefined) { alquiler = false } }

    var ocupadoTemp = $("input:checkbox[name=ocupado]:checked").val(); var ocupado = false
    if (ocupadoTemp == 0 || ocupadoTemp == "on") { ocupado = true } else { if (ocupadoTemp == undefined) { ocupado = false } }

    var salaTemp = $("input:checkbox[name=sala]:checked").val(); var sala = false
    if (salaTemp == 0 || salaTemp == "on") { sala = true } else { if (salaTemp == undefined) { sala = false } }

    var comedorTemp = $("input:checkbox[name=comedor]:checked").val(); var comedor = false
    if (comedorTemp == 0 || comedorTemp == "on") { comedor = true } else { if (comedorTemp == undefined) { comedor = false } }

    var cocinaTemp = $("input:checkbox[name=cocina]:checked").val(); var cocina = false
    if (cocinaTemp == 0 || cocinaTemp == "on") { cocina = true } else { if (cocinaTemp == undefined) { cocina = false } }

    var desayunadorTemp = $("input:checkbox[name=desayunador]:checked").val(); var desayunador = false
    if (desayunadorTemp == 0 || desayunadorTemp == "on") { desayunador = true } else { if (desayunadorTemp == undefined) { desayunador = false } }

    var antecomTemp = $("input:checkbox[name=antecom]:checked").val(); var antecom = false
    if (antecomTemp == 0 || antecomTemp == "on") { antecom = true } else { if (antecomTemp == undefined) { antecom = false } }

    var salaTVTemp = $("input:checkbox[name=salaTV]:checked").val(); var salaTV = false
    if (salaTVTemp == 0 || salaTVTemp == "on") { salaTV = true } else { if (salaTVTemp == undefined) { salaTV = false } }

    var barTemp = $("input:checkbox[name=bar]:checked").val(); var bar = false
    if (barTemp == 0 || barTemp == "on") { bar = true } else { if (barTemp == undefined) { bar = false } }

    var cacinoTemp = $("input:checkbox[name=cacino]:checked").val(); var cacino = false
    if (cacinoTemp == 0 || cacinoTemp == "on") { cacino = true } else { if (cacinoTemp == undefined) { cacino = false } }

    var terrazaTemp = $("input:checkbox[name=terraza]:checked").val(); var terraza = false
    if (terrazaTemp == 0 || terrazaTemp == "on") { terraza = true } else { if (terrazaTemp == undefined) { terraza = false } }

    var balconTemp = $("input:checkbox[name=balcon]:checked").val(); var balcon = false
    if (balconTemp == 0 || balconTemp == "on") { balcon = true } else { if (balconTemp == undefined) { balcon = false } }

    var lavanderiaTemp = $("input:checkbox[name=lavanderia]:checked").val(); var lavanderia = false
    if (lavanderiaTemp == 0 || lavanderiaTemp == "on") { lavanderia = true } else { if (lavanderiaTemp == undefined) { lavanderia = false } }

    var despensaTemp = $("input:checkbox[name=despensa]:checked").val(); var despensa = false
    if (despensaTemp == 0 || despensaTemp == "on") { despensa = true } else { if (despensaTemp == undefined) { despensa = false } }

    var closetBlancoTemp = $("input:checkbox[name=closetBlanco]:checked").val(); var closetBlanco = false
    if (closetBlancoTemp == 0 || closetBlancoTemp == "on") { closetBlanco = true } else { if (closetBlancoTemp == undefined) { closetBlanco = false } }

    var walkInClosetTemp = $("input:checkbox[name=walkInCloset]:checked").val(); var walkInCloset = false
    if (walkInClosetTemp == 0 || walkInCloset == "on") { walkInCloset = true } else { if (walkInClosetTemp == undefined) { walkInCloset = false } }

    var puertasBañoTemp = $("input:checkbox[name=puertasBaño]:checked").val(); var puertasBaño = false
    if (puertasBañoTemp == 0 || puertasBañoTemp == "on") { puertasBaño = true } else { if (puertasBañoTemp == undefined) { puertasBaño = false } }

    var senderosTemp = $("input:checkbox[name=senderos]:checked").val(); var senderos = false
    if (senderosTemp == 0 || senderosTemp == "on") { senderos = true } else { if (senderosTemp == undefined) { senderos = false } }

    var mascotasTemp = $("input:checkbox[name=mascotas]:checked").val(); var mascotas = false
    if (mascotasTemp == 0 || mascotasTemp == "on") { mascotas = true } else { if (mascotasTemp == undefined) { mascotas = false } }

    var perreraTemp = $("input:checkbox[name=perrera]:checked").val(); var perrera = false
    if (perreraTemp == 0 || perreraTemp == "on") { perrera = true } else { if (perreraTemp == undefined) { perrera = false } }

    var seguridadTemp = $("input:checkbox[name=seguridad]:checked").val(); var seguridad = false
    if (seguridadTemp == 0 || seguridadTemp == "on") { seguridad = true } else { if (seguridadTemp == undefined) { seguridad = false } }

    var oficinaTemp = $("input:checkbox[name=oficina]:checked").val(); var oficina = false
    if (oficinaTemp == 0 || oficinaTemp == "on") { oficina = true } else { if (oficinaTemp == undefined) { oficina = false } }

    var bodegaTemp = $("input:checkbox[name=bodega]:checked").val(); var bodega = false
    if (bodegaTemp == 0 || bodegaTemp == "on") { bodega = true } else { if (bodegaTemp == undefined) { bodega = false } }

    var garageTemp = $("input:checkbox[name=garage]:checked").val(); var garage = false
    if (garageTemp == 0 || garageTemp == "on") { garage = true } else { if (garageTemp == undefined) { garage = false } }

    var jardinTemp = $("input:checkbox[name=jardin]:checked").val(); var jardin = false
    if (jardinTemp == 0 || jardinTemp == "on") { jardin = true } else { if (jardinTemp == undefined) { jardin = false } }

    var patioTemp = $("input:checkbox[name=patio]:checked").val(); var patio = false
    if (patioTemp == 0 || patioTemp == "on") { patio = true } else { if (patioTemp == undefined) { patio = false } }

    var patioLuzTemp = $("input:checkbox[name=patioLuz]:checked").val(); var patioLuz = false
    if (patioLuzTemp == 0 || patioLuzTemp == "on") { patioLuz = true } else { if (patioLuzTemp == undefined) { patioLuz = false } }

    var aticoTemp = $("input:checkbox[name=atico]:checked").val(); var atico = false
    if (aticoTemp == 0 || aticoTemp == "on") { atico = true } else { if (aticoTemp == undefined) { atico = false } }

    var piscinaTemp = $("input:checkbox[name=piscina]:checked").val(); var piscina = false
    if (piscinaTemp == 0 || piscinaTemp == "on") { piscina = true } else { if (piscinaTemp == undefined) { piscina = false } }

    var ranchoTemp = $("input:checkbox[name=rancho]:checked").val(); var rancho = false
    if (ranchoTemp == 0 || ranchoTemp == "on") { rancho = true } else { if (ranchoTemp == undefined) { rancho = false } }

    var canchasTemp = $("input:checkbox[name=canchas]:checked").val(); var canchas = false
    if (canchasTemp == 0 || canchasTemp == "on") { canchas = true } else { if (canchasTemp == undefined) { canchas = false } }

    var cloacaTemp = $("input:checkbox[name=cloaca]:checked").val(); var cloaca = false
    if (cloacaTemp == 0 || cloacaTemp == "on") { cloaca = true } else { if (cloacaTemp == undefined) { cloaca = false } }

    var septicoTemp = $("input:checkbox[name=septico]:checked").val(); var septico = false
    if (septicoTemp == 0 || septicoTemp == "on") { septico = true } else { if (septicoTemp == undefined) { septico = false } }

    var plantaTratamientoTemp = $("input:checkbox[name=plantaTratamiento]:checked").val(); var plantaTratamiento = false
    if (plantaTratamientoTemp == 0 || planoCatastroTemp == "on") { plantaTratamiento = true } else { if (plantaTratamientoTemp == undefined) { plantaTratamiento = false } }

    var captBombaTemp = $("input:checkbox[name=captBomba]:checked").val(); var captBomba = false
    if (captBombaTemp == 0 || captBombaTemp == "on") { captBomba = true } else { if (captBombaTemp == undefined) { captBomba = false } }

    var telsTemp = $("input:checkbox[name=tels]:checked").val(); var tels = false
    if (telsTemp == 0 || telsTemp == "on") { tels = true } else { if (telsTemp == undefined) { tels = false } }

    var cableTemp = $("input:checkbox[name=cable]:checked").val(); var cable = false
    if (cableTemp == 0 || cableTemp == "on") { cable = true } else { if (cableTemp == undefined) { cable = false } }

    var internetTemp = $("input:checkbox[name=internet]:checked").val(); var internet = false
    if (internetTemp == 0 || internetTemp == "on") { internet = true } else { if (internetTemp == undefined) { internet = false } }

    var alarmaTemp = $("input:checkbox[name=alarma]:checked").val(); var alarma = false
    if (alarmaTemp == 0 || alarmaTemp == "on") { alarma = true } else { if (alarmaTemp == undefined) { alarma = false } }

    var cctvTemp = $("input:checkbox[name=cctv]:checked").val(); var cctv = false
    if (cctvTemp == 0 || cctvTemp == "on") { cctv = true } else { if (cctvTemp == undefined) { cctv = false } }

    var portonElectTemp = $("input:checkbox[name=portonElect]:checked").val(); var portonElect = false
    if (portonElectTemp == 0 || portonElectTemp == "on") { portonElect = true } else { if (portonElectTemp == undefined) { portonElect = false } }

    var controlTemp = $("input:checkbox[name=control]:checked").val(); var control = false
    if (controlTemp == 0 || controlTemp == "on") { control = true } else { if (controlTemp == undefined) { control = false } }

    var cercaElectricaTemp = $("input:checkbox[name=cercaElectrica]:checked").val(); var cercaElectrica = false
    if (cercaElectricaTemp == 0 || cercaElectricaTemp == "on") { cercaElectrica = true } else { if (cercaElectricaTemp == undefined) { cercaElectrica = false } }

    var alamNavajaTemp = $("input:checkbox[name=alamNavaja]:checked").val(); var alamNavaja = false
    if (alamNavajaTemp == 0 || alamNavajaTemp == "on") { alamNavaja = true } else { if (alamNavajaTemp == undefined) { alamNavaja = false } }

    var acTemp = $("input:checkbox[name=ac]:checked").val(); var ac = false
    if (acTemp == 0 || acTemp == "on") { ac = true } else { if (acTemp == undefined) { ac = false } }


    var detectorHumoTemp = $("input:checkbox[name=detectorHumo]:checked").val(); var detectorHumo = false
    if (detectorHumoTemp == 0 || detectorHumoTemp == "on") { detectorHumo = true } else { if (detectorHumoTemp == undefined) { detectorHumo = false } }

    var sistIncendioTemp = $("input:checkbox[name=sistIncendio]:checked").val(); var sistIncendio = false
    if (sistIncendioTemp == 0 || sistIncendioTemp == "on") { sistIncendio = true } else { if (sistIncendioTemp == undefined) { sistIncendio = false } }

    var sistSonidoTemp = $("input:checkbox[name=sistSonido]:checked").val(); var sistSonido = false
    if (sistSonidoTemp == 0 || sistSonidoTemp == "on") { sistSonido = true } else { if (sistSonidoTemp == undefined) { sistSonido = false } }

    var tableroDigitalTemp = $("input:checkbox[name=tableroDigital]:checked").val(); var tableroDigital = false
    if (tableroDigitalTemp == 0 || tableroDigitalTemp == "on") { tableroDigital = true } else { if (tableroDigitalTemp == undefined) { tableroDigital = false } }

    var tableroElecTemp = $("input:checkbox[name=tableroElec]:checked").val(); var tableroElec = false
    if (tableroElecTemp == 0 || tableroElecTemp == "on") { tableroElec = true } else { if (tableroElecTemp == undefined) { tableroElec = false } }

    var dispH20Temp = $("input:checkbox[name=dispH20]:checked").val(); var dispH20 = false
    if (dispH20Temp == 0 || dispH20Temp == "on") { dispH20 = true } else { if (dispH20Temp == undefined) { dispH20 = false } }

    var amuebladoTemp = $("input:checkbox[name=amueblado]:checked").val(); var amueblado = false
    if (amuebladoTemp == 0 || amuebladoTemp == "on") { amueblado = true } else { if (amuebladoTemp == undefined) { amueblado = false } }

    var gimnacioTemp = $("input:checkbox[name=gimnacio]:checked").val(); var gimnacio = false
    if (gimnacioTemp == 0 || gimnacioTemp == "on") { gimnacio = true } else { if (gimnacioTemp == undefined) { gimnacio = false } }

    var loungeTemp = $("input:checkbox[name=lounge]:checked").val(); var lounge = false
    if (loungeTemp == 0 || loungeTemp == "on") { lounge = true } else { if (loungeTemp == undefined) { lounge = false } }

    var jacuzziTemp = $("input:checkbox[name=jacuzzi]:checked").val(); var jacuzzi = false
    if (jacuzziTemp == 0 || jacuzziTemp == "on") { jacuzzi = true } else { if (jacuzziTemp == undefined) { jacuzzi = false } }

    var cortinasTemp = $("input:checkbox[name=cortinas]:checked").val(); var cortinas = false
    if (cortinasTemp == 0 || cortinasTemp == "on") { cortinas = true } else { if (cortinasTemp == undefined) { cortinas = false } }

    var lamparasTemp = $("input:checkbox[name=lamparas]:checked").val(); var lamparas = false
    if (lamparasTemp == 0 || lamparasTemp == "on") { lamparas = true } else { if (lamparasTemp == undefined) { lamparas = false } }

    var chimeneaTemp = $("input:checkbox[name=chimenea]:checked").val(); var chimenea = false
    if (chimeneaTemp == 0 || chimeneaTemp == "on") { chimenea = true } else { if (chimeneaTemp == undefined) { chimenea = false } }

    var cedazosTemp = $("input:checkbox[name=cedazos]:checked").val(); var cedazos = false
    if (cedazosTemp == 0 || cedazosTemp == "on") { cedazos = true } else { if (cedazosTemp == undefined) { cedazos = false } }

    var intercomTemp = $("input:checkbox[name=intercom]:checked").val(); var intercom = false
    if (intercomTemp == 0 || intercomTemp == "on") { intercom = true } else { if (intercomTemp == undefined) { intercom = false } }

    var plantElectricaTemp = $("input:checkbox[name=plantElectrica]:checked").val(); var plantElectrica = false
    if (plantElectricaTemp == 0 || plantaTratamientoTemp == "on") { plantElectrica = true } else { if (plantElectricaTemp == undefined) { plantElectrica = false } }

    var asensorTemp = $("input:checkbox[name=asensor]:checked").val(); var asensor = false
    if (asensorTemp == 0 || asensorTemp == "on") { asensor = true } else { if (asensorTemp == undefined) { asensor = false } }

    var usoSueloTemp = $("input:checkbox[name=usoSuelo]:checked").val(); var usoSuelo = false
    if (usoSueloTemp == 0 || usoSueloTemp == "on") { usoSuelo = true } else { if (usoSueloTemp == undefined) { usoSuelo = false } }

    var planoCatastroTemp = $("input:checkbox[name=planoCatastro]:checked").val(); var planoCatastro = false
    if (planoCatastroTemp == 0 || planoCatastroTemp == "on") { planoCatastro = true } else { if (planoCatastroTemp == undefined) { planoCatastro = false } }

    var planoConstruccionTemp = $("input:checkbox[name=planoConstruccion]:checked").val(); var planoConstruccion = false
    if (planoConstruccionTemp == 0 || planoConstruccionTemp == "on") { planoConstruccion = true } else { if (planoConstruccionTemp == undefined) { planoConstruccion = false } }

    $.ajax({
        type: 'POST',
        url: '/PropiedadesAgente/ActualizarPropiedad',
        data:
        {
            Codigo: $("#codigo").val(),
            Nombre: $("#nombre").val(),
            Agente: parseInt(prop),
            LinkSitioWeb: $("#linkSitioWeb").val(),
            LinkGoogleMaps: $("#maps").val(),
            PrecioVenta: $("#precioVenta").val(),
            PrecioAlquiler: $("#precioAlquiler").val(),
            NoPlanoCatastro: $("#noPlanoCatastro").val(),
            NoFolioReal: $("#noFolioReal").val(),
            AreaTerrenoM2: $("#areaTerreno").val(),
            AreaConstruccionM2: $("#areaConstruccion").val(),
            Provincia: $("#provincia").val(),
            Canton: $("#canton").val(),
            Distrito: $("#distrito").val(),
            Direccion: $("#direccion").val(),
            CuotaMantenimiento: $("#cuotaMantenimiento").val(),
            CuotaSeguridad: $("#cuotaSeguridad").val(),
            ImpuestoSolidario: $("#impuestoSolidario").val(),
            Impuestos: $("#impuestos").val(),
            IVA: $("#iva").val(),
            CodigoPostal: $("#postal").val(),
            FrenteM2: $("#frente").val(),
            FondoM2: $("#fondo").val(),
            AlturaM2: $("#altura").val(),
            Niveles: $("#niveles").val(),
            Dormitorios: $("#dormitorios").val(),
            Baños: $("#baños").val(),
            Pisos: $("#pisos").val(),
            Cielos: $("#cielos").val(),
            MedidorAgua: medidorAgua,
            Sala: sala,
            Comedor: comedor,
            Cocina: cocina,
            Desayunador: desayunador,
            Antecom: antecom,
            SalaTV: salaTV,
            Bar: bar,
            Casino: cacino,
            Terraza: terraza,
            Balcon: balcon,
            Lavanderia: lavanderia,
            Despensa: despensa,
            ClosetBlanco: closetBlanco,
            Walk_in_Closet: walkInCloset,
            Puertas_Baño: puertasBaño,
            Senderos: senderos,
            Mascotas: mascotas,
            AreaMascotaM2: $("#areaMascota").val(),
            Perrera: perrera,
            Seguridad: seguridad,
            Oficina: oficina,
            Bodega: bodega,
            Garage: garage,
            Jardin: jardin,
            Patio: patio,
            Patio_Luz: patioLuz,
            Atico: atico,
            Piscina: piscina,
            Rancho: rancho,
            Canchas: canchas,
            Gimnacio: gimnacio,
            Lounge: lounge,
            Jacuzzi: jacuzzi,
            Cortinas: cortinas,
            Lamparas: $("#lamparas").val(),
            Chimenea: chimenea,
            Cedazos: cedazos,
            InterCom: intercom,
            PlantaElectrica: plantElectrica,
            Asensor: asensor,
            Parqueo: $("#parqueo").val(),
            HotH20: $("#hotH2O").val(),
            Cloaca: cloaca,
            Septico: septico,
            PlantaTratamiento: plantaTratamiento,
            Capt_Bomba: captBomba,
            Tels: tels,
            Cable: cable,
            Internet: internet,
            Alarma: alarma,
            CCTV: cctv,
            PortonElectrico: portonElect,
            Control: control,
            CercaElectrica: cercaElectrica,
            AlambreNavaja: alamNavaja,
            A_C: ac,
            LampEmergencia: $("#lamparasEmergencia").val(),
            DetectorHumo: detectorHumo,
            SistemaIncendio: sistIncendio,
            SistemaSonido: sistSonido,
            TableroDigital: tableroDigital,
            TableroElec: tableroElec,
            Sobres: $("#sobres").val(),
            Amueblado: amueblado,
            MueblesCocina: $("#mueblesCocina").val(),
            Electrod: $("#electrod").val(),
            Cerchas: $("#cerchas").val(),
            Cornizas: $("#cornizas").val(),
            Puertas: $("#puertas").val(),
            ContraMarcos: $("#contramarcos").val(),
            Ventaneria: $("#ventaneria").val(),
            Cobertura: $("#cobertura").val(),
            Est_Suelo: $("#estSuelo").val(),
            DispositivoH20: dispH20,
            UsoSuelo: usoSuelo,
            Planos_Construccion: planoConstruccion,
            Plano_Catastro: planoCatastro,
            Curvas_Nivel: $("#curvasNivel").val(),
            Avaluo: $("#avaluo").val(),
            Venta: venta,
            Alquiler: alquiler,
            T_Casa: tipoCasa,
            T_Terreno: tipoTerreno,
            T_Local: tipoLocal,
            T_Oficina: tipoOficina,
            T_Bodega: tipoBodega,
            T_Edificio: tipoEdificio,
            T_Hotel: tipoHotel,
            T_Condominio: tipoCondominio,
            T_Apartamento: tipoApartamento,
            Ocupado: ocupado,
            Llaves: $("#llaves").val(),
            MapsLat: $("#mapsLat").val(),
            MapsLong: $("#mapsLong").val(),
            Imagen: null,
            Estado: $("#estado").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/PropiedadesAgente/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}



function EliminarPropiedadAgente() {
    $.ajax({
        type: 'POST',
        url: '/PropiedadesAgente/EliminarPropiedad',
        data:
        {
            Codigo: $("#codigo").val()
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            Swal.fire(data);
            var url = "/PropiedadesAgente/index"
            window.location.href = url;
        },
        error: function (data) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error...',
                text: data,
            })
        }
    });
}
