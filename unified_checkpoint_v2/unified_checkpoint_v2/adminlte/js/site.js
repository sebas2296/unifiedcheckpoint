﻿//Autor: Sebastián Gutiérrez - Cambio de zoom para adpatar tamaño de la tabla
var pixel_ratio = window.devicePixelRatio;
$(window).resize(function () {
    if (pixel_ratio != window.devicePixelRatio) {
        pixel_ratio = window.devicePixelRatio
        console.log(window.devicePixelRatio);
        if (window.devicePixelRatio == 1.25) {
            $('.contenedorTabla').css("height", "345px")
        }
        if (window.devicePixelRatio == 1.125) {
            $('.contenedorTabla').css("height", "52vh")
        }
        if (window.devicePixelRatio == 1) {
            $('.contenedorTabla').css("height", "560px")
        }
        if (window.devicePixelRatio == 0.9375) {
            $('.contenedorTabla').css("height", "645px")
        }
        if (window.devicePixelRatio == 0.8333333730697632) {
            $('.contenedorTabla').css("height", "730px")
        }
        if (window.devicePixelRatio == 0.625) {
            $('.contenedorTabla').css("height", "1040px")
        }
    }
});

$(document).ready(function () {
    pixel_ratio = window.devicePixelRatio
    console.log(window.devicePixelRatio);
    if (window.devicePixelRatio == 1.25) {
        $('.contenedorTabla').css("height", "345px")
    }
    if (window.devicePixelRatio == 1.125) {
        $('.contenedorTabla').css("height", "52vh")
    }
    if (window.devicePixelRatio == 1) {
        $('.contenedorTabla').css("height", "560px")
    }
    if (window.devicePixelRatio == 0.9375) {
        $('.contenedorTabla').css("height", "645px")
    }
    if (window.devicePixelRatio == 0.8333333730697632) {
        $('.contenedorTabla').css("height", "730px")
    }
    if (window.devicePixelRatio == 0.625) {
        $('.contenedorTabla').css("height", "1040px")
    }
});