﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using unified_checkpoint_v2.Models;

namespace unified_checkpoint_v2.Controllers
{
    public class EventController : Controller
    {
        [Authorize(Roles = "Admin,Vendedor")]
        // GET: Event
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetEvents()
        {
            using (DBUnifiedCheckpointEntities2 dc = new DBUnifiedCheckpointEntities2())
            {
                var events = dc.Events.ToList();
                return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpPost]
        public JsonResult SaveEvent(Event e)
        {
            var status = false;
            using (DBUnifiedCheckpointEntities2 dc = new DBUnifiedCheckpointEntities2())
            {
                if (e.EventID > 0)
                {
                    //Update the event
                    var v = dc.Events.Where(a => a.EventID == e.EventID).FirstOrDefault();
                    if (v != null)
                    {
                        v.Subject = e.Subject;
                        v.Start = e.Start;
                        v.End = e.End;
                        v.Description = e.Description;
                    }
                }
                else
                {
                    dc.Events.Add(e);
                }

                dc.SaveChanges();
                status = true;

            }
            return new JsonResult { Data = new { status = status } };
        }

        [HttpPost]
        public JsonResult DeleteEvent(int eventID)
        {
            var status = false;
            using (DBUnifiedCheckpointEntities2 dc = new DBUnifiedCheckpointEntities2())
            {
                var v = dc.Events.Where(a => a.EventID == eventID).FirstOrDefault();
                if (v != null)
                {
                    dc.Events.Remove(v);
                    dc.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }
    }
}