﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.IO;
using System.Text;
using System.Drawing;
using unified_checkpoint_v2.Models;

namespace unified_checkpoint_v2.Controllers
{
    public class EstadisticaController : Controller
    {
        DBUnifiedCheckpointEntities2 db = new DBUnifiedCheckpointEntities2();

        //grafico Face x Instagram
        public ActionResult ChartInstFB()
        {
            var data = db.sp_AvgPresupuestos().ToList();
            var chart = new Chart();
            chart.Height = 400;
            chart.Width = 400;
            var area = new ChartArea();
            area.BackColor = Color.FromArgb(174, 214, 241);
            chart.ChartAreas.Add(area);
            var series = new Series();
            foreach (var item in data)
            {
                series.Points.AddXY(0, item.PresFB);
                series.Points[0].LegendText = "Facebook";
                series.Points.AddXY(1, item.PresINS);
                series.Points[1].LegendText = "Instagram";
            }

            //Area de las Leyendas 
            Legend chartLegend = new Legend();
            chartLegend.Name = "Result";
            chartLegend.LegendStyle = LegendStyle.Table;
            chart.Legends.Add(chartLegend);

            //carga de datos 
            series.Label = "#PERCENT{P0}";
            series.Font = new Font("Arial", 8.0f, FontStyle.Bold);
            series.ChartType = SeriesChartType.Pie;
            series["PieLabelStyle"] = "Outside";
            chart.Series.Add(series);
            var returnStream = new MemoryStream();
            chart.ImageType = ChartImageType.Png;
            chart.SaveImage(returnStream);
            returnStream.Position = 0;
            return new FileStreamResult(returnStream, "image/png");
        }

        [Authorize(Roles = "Admin")]
        // GET: Estadistica
        public ActionResult Index()
        {
            return View();
        }

        //grafico Wasi x Presi x encuentra 24
        public ActionResult ChartWaPr()
        {
            var data = db.sp_AvgPresupuestos().ToList();
            var chart = new Chart();
            chart.Height = 400;
            chart.Width = 400;
            var area = new ChartArea();
            area.BackColor = Color.FromArgb(163, 128, 197);
            chart.ChartAreas.Add(area);
            var series = new Series();
            foreach (var item in data)
            {
                series.Points.AddXY(0, item.PresWz);
                series.Points[0].LegendText = "Wazi";
                series.Points.AddXY(1, item.PresPR);
                series.Points[1].LegendText = "Prezi";
                series.Points.AddXY(2, item.PresEN);
                series.Points[2].LegendText = "Encuentra 24";
            }

            //Area de las Leyendas 
            Legend chartLegend = new Legend();
            chartLegend.Name = "Result";
            chartLegend.LegendStyle = LegendStyle.Table;
            chart.Legends.Add(chartLegend);

            //carga de datos 
            series.Label = "#PERCENT{P0}";
            series.Font = new Font("Arial", 8.0f, FontStyle.Bold);
            series.ChartType = SeriesChartType.Pie;
            series["PieLabelStyle"] = "Outside";
            chart.Series.Add(series);
            var returnStream = new MemoryStream();
            chart.ImageType = ChartImageType.Png;
            chart.SaveImage(returnStream);
            returnStream.Position = 0;
            return new FileStreamResult(returnStream, "image/png");
        }

        //grafico Video x Letrero
        public ActionResult ChartVidLet()
        {
            var data = db.sp_AvgPresupuestos().ToList();
            var chart = new Chart();
            var area = new ChartArea();
            area.BackColor = Color.FromArgb(197, 144, 128);
            chart.Height = 400;
            chart.Width = 400;
            chart.ChartAreas.Add(area);
            var series = new Series();
            foreach (var item in data)
            {
                series.Points.AddXY(0, item.PresVD);
                series.Points[0].LegendText = "Video";
                series.Points.AddXY(1, item.costoletrero);
                series.Points[1].LegendText = "Letrero Fisico";

            }

            //Area de las Leyendas 
            Legend chartLegend = new Legend();
            chartLegend.Name = "Result";
            chartLegend.LegendStyle = LegendStyle.Table;
            chart.Legends.Add(chartLegend);

            //carga de datos 
            series.Label = "#PERCENT{P0}";
            series.Font = new Font("Arial", 8.0f, FontStyle.Bold);
            series.ChartType = SeriesChartType.Pie;
            series["PieLabelStyle"] = "Outside";
            chart.Series.Add(series);
            var returnStream = new MemoryStream();
            chart.ImageType = ChartImageType.Png;
            chart.SaveImage(returnStream);
            returnStream.Position = 0;
            return new FileStreamResult(returnStream, "image/png");
        }

        //grafico Totales 
        public ActionResult ChartTotal()
        {
            var data = db.sp_AvgPresupuestos().ToList();
            var chart = new Chart();
            chart.Height = 480;
            chart.Width = 640;
            var area = new ChartArea();
            area.BackColor = Color.FromArgb(128, 197, 184);
            chart.ChartAreas.Add(area);
            var series = new Series();
            foreach (var item in data)
            {
                series.Points.AddXY(0, item.PresFB);
                series.Points[0].LegendText = "Facebook";
                series.Points.AddXY(1, item.PresINS);
                series.Points[1].LegendText = "Instagram";
                series.Points.AddXY(2, item.PresWz);
                series.Points[2].LegendText = "Wazi";
                series.Points.AddXY(3, item.PresPR);
                series.Points[3].LegendText = "Prezi";
                series.Points.AddXY(4, item.PresEN);
                series.Points[4].LegendText = "Encuentra 24";
                series.Points.AddXY(5, item.PresVD);
                series.Points[5].LegendText = "Video";
                series.Points.AddXY(6, item.costoletrero);
                series.Points[6].LegendText = "Letrero Fisico";

            }

            //Area de las Leyendas 
            Legend chartLegend = new Legend();
            chartLegend.Name = "Result";
            chartLegend.LegendStyle = LegendStyle.Table;
            chart.Legends.Add(chartLegend);

            //carga de datos 
            series.Label = "#PERCENT{P0}";
            series.Font = new Font("Arial", 8.0f, FontStyle.Bold);
            series.ChartType = SeriesChartType.Pie;
            series["PieLabelStyle"] = "Outside";
            chart.Series.Add(series);
            var returnStream = new MemoryStream();
            chart.ImageType = ChartImageType.Png;
            chart.SaveImage(returnStream);
            returnStream.Position = 0;
            return new FileStreamResult(returnStream, "image/png");
        }
    }
}