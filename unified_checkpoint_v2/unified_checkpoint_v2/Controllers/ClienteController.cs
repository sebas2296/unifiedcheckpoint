﻿using ClosedXML.Excel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using unified_checkpoint_v2.Models;
using unified_checkpoint_v2.Negocio;


namespace unified_checkpoint_v2.Controllers
{
    public class ClienteController : Controller
    {
        DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();
        List<Cliente> _Clientes = new List<Cliente>();

        [Authorize(Roles = "Admin,Vendedor")]
        public ActionResult Index()
        {
            using (model)
            {
                var Clientess = (from x in model.Clientes select x).ToList();

                return View(Clientess);
            }
        }

        [HttpGet]
        public ActionResult InsertarClientes()
        {
            string FechaActual = DateTime.Now.ToString("M/d/yyyy");
            ViewBag.FechaActual = FechaActual;
            using (model)
            {
                var propiedades = (from x in model.Propiedades select x).ToList();

                return View(propiedades);
            }
        }

        [HttpPost]
        public ActionResult InsertarClientes(Cliente Clientes)
        {
            try
            {
                using (model)
                {
                    var propiedades = (from x in model.Propiedades select x).ToList();
                    foreach (var item in propiedades)
                    {
                        if (item.Codigo == Clientes.Propiedad)
                        {
                            if (item.Propietario != null)
                            {
                                Clientes.PropiedadGRS = true;
                            }
                            else
                            {
                                Clientes.PropiedadGRS = false;
                            }
                        }
                    }
                }
                   
                ClientesHelpers ClientesHelpers = new ClientesHelpers();
                ClientesHelpers.sp_InsertarClientes(Clientes);

                Auditoria auditoria = new Auditoria();
                AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId;
                    auditoria.Modulo = "Clientes";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " insertó el nuevo cliente " + Clientes.Nombre + " " + Clientes.Apellido;
                }
                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Clientes agregado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al ingresar este Clientes, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult ActualizarCliente(int? cedula, string propiedad)
        {
            try
            {
                var getClientes = new Cliente();
                getClientes = (from age in model.Clientes
                              where age.Cedula == cedula && age.Propiedad == propiedad
                              select age).FirstOrDefault();
                return View(getClientes);
                //return Json(getAgente, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult ActualizarCliente(Cliente Clientes)
        {
            try
            {
                using (model)
                {
                    var propiedades = (from x in model.Propiedades select x).ToList();
                    foreach (var item in propiedades)
                    {
                        if (item.Codigo == Clientes.Propiedad)
                        {
                            if (item.Propietario != null)
                            {
                                Clientes.PropiedadGRS = true;
                            }
                            else
                            {
                                Clientes.PropiedadGRS = false;
                            }
                        }
                    }
                }

                ClientesHelpers ClientesHelpers = new ClientesHelpers();
                ClientesHelpers.sp_ActualizarClientes(Clientes);

                Auditoria auditoria = new Auditoria();
                AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId;
                    auditoria.Modulo = "Clientes";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " actualizó el cliente " + Clientes.Nombre + " " + Clientes.Apellido;
                }
                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Clientes actualizado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al actualizar este Clientes, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult EliminarCliente(int? cedula, string propiedad)
        {
            try
            {
                var getClientes = new Cliente();
                getClientes = (from age in model.Clientes
                              where age.Cedula == cedula && age.Propiedad == propiedad
                              select age).FirstOrDefault();
                return View(getClientes);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult EliminarCliente(Cliente Clientes)
        {
            try
            {
                ClientesHelpers ClientesHelpers = new ClientesHelpers();
                ClientesHelpers.sp_EliminarClientes(Clientes);

                Auditoria auditoria = new Auditoria();
                AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId;
                    auditoria.Modulo = "Clientes";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " eliminó el cliente " + Clientes.Nombre + " " + Clientes.Apellido;
                }
                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Clientes eliminado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al eliminar este Clientes, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult DetallesCliente(int? cedula, string propiedad)
        {
            try
            {
                var getClientes = new Cliente();
                getClientes = (from age in model.Clientes
                              where age.Cedula == cedula && age.Propiedad == propiedad
                              select age).FirstOrDefault();
                return View(getClientes);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        public void CargarListaClientes()
        {
            using (model)
            {
                var Clientes = (from x in model.Clientes select x).ToList();
                foreach (var item in Clientes)
                {
                    _Clientes.Add(new Cliente
                    {
                        Cedula = item.Cedula,
                        Nombre = item.Nombre,
                        Apellido = item.Apellido,
                        Telefono = item.Telefono,
                        Correo = item.Correo,
                        PropiedadGRS = item.PropiedadGRS,
                        Propiedad = item.Propiedad,
                        FechaInteres = item.FechaInteres,
                        Estado = item.Estado
                    });
                }
            }
        }

        public ActionResult ExportarExcel()
        {
            CargarListaClientes();
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Clientess");
                worksheet.ColumnWidth = 20;
                var currentRow = 1;

                //Header del Excel
                worksheet.Cell(currentRow, 1).Value = "Cédula";
                worksheet.Cell(currentRow, 2).Value = "Nombre";
                worksheet.Cell(currentRow, 3).Value = "Apellidos";
                worksheet.Cell(currentRow, 4).Value = "Correo";
                worksheet.Cell(currentRow, 5).Value = "Interesado en";
                worksheet.Cell(currentRow, 6).Value = "Código Propiedad";
                worksheet.Cell(currentRow, 7).Value = "Fecha de interés";
                worksheet.Cell(currentRow, 8).Value = "Estado";

                //Body del Excel
                foreach (var item in _Clientes)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = item.Cedula;
                    worksheet.Cell(currentRow, 2).Value = item.Nombre;
                    worksheet.Cell(currentRow, 3).Value = item.Apellido;
                    worksheet.Cell(currentRow, 4).Value = item.Correo;
                    if (item.PropiedadGRS == true)
                    {
                        worksheet.Cell(currentRow, 5).Value = "Propiedad Gutiérrez";
                    }
                    else 
                    {
                        worksheet.Cell(currentRow, 5).Value = "Propiedad de Agente";
                    }
                    worksheet.Cell(currentRow, 6).Value = item.Propiedad;
                    worksheet.Cell(currentRow, 7).Value = item.FechaInteres;
                    if (item.Estado == true)
                    {
                        worksheet.Cell(currentRow, 8).Value = "Interesado Actualmente";
                    }
                    else
                    {
                        worksheet.Cell(currentRow, 8).Value = "No está interesado actualmente";
                    }
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();


                    return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Clientes.xlsx");
                }
            }
        }
    }
}