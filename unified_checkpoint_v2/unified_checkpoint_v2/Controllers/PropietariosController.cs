﻿using ClosedXML.Excel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using unified_checkpoint_v2.Models;
using unified_checkpoint_v2.Negocio;

namespace unified_checkpoint_v2.Controllers
{
    public class PropietariosController : Controller
    {
        DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();
        List<Propietario> _Propietarios = new List<Propietario>();

        [Authorize(Roles = "Admin,Vendedor")]
        public ActionResult Index()
        {
            using (model)
            {
                var Propietarios = (from x in model.Propietarios select x).ToList();

                return View(Propietarios);
            }
        }

        [HttpGet]
        public ActionResult InsertarPropietario()
        {
            return View();
        }

        [HttpPost]
        public ActionResult InsertarPropietario(Propietario Propietarios)
        {
            try
            {
                PropietariosHelpers PropietariosHelpers = new PropietariosHelpers();
                PropietariosHelpers.sp_InsertarPropietarios(Propietarios);

                Auditoria auditoria = new Auditoria();
                AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId;
                    auditoria.Modulo = "Propietarios";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " insertó el nuevo propietario " + Propietarios.Nombre + " " + Propietarios.Apellido;
                }
                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Propietarios agregado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al ingresar este Propietarios, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult ActualizarPropietario(int? cedula)
        {
            try
            {
                var getPropietarios = new Propietario();
                getPropietarios = (from age in model.Propietarios
                                   where age.Cedula == cedula
                                   select age).FirstOrDefault();
                return View(getPropietarios);
                //return Json(getAgente, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult ActualizarPropietario(Propietario Propietarios)
        {
            try
            {
                PropietariosHelpers PropietariosHelpers = new PropietariosHelpers();
                PropietariosHelpers.sp_ActualizarPropietarios(Propietarios);

                Auditoria auditoria = new Auditoria();
                AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId;
                    auditoria.Modulo = "Propietarios";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " actualizó el propietario " + Propietarios.Nombre + " " + Propietarios.Apellido;
                }
                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Propietarios actualizado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al actualizar este Propietarios, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult EliminarPropietario(int? cedula)
        {
            try
            {
                var getPropietarios = new Propietario();
                getPropietarios = (from age in model.Propietarios
                                   where age.Cedula == cedula
                                   select age).FirstOrDefault();
                return View(getPropietarios);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult EliminarPropietario(Propietario Propietarios)
        {
            try
            {
                PropietariosHelpers PropietariosHelpers = new PropietariosHelpers();
                PropietariosHelpers.sp_EliminarPropietarios(Propietarios);

                Auditoria auditoria = new Auditoria();
                AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId;
                    auditoria.Modulo = "Propietarios";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " eliminó el propietario " + Propietarios.Nombre + " " + Propietarios.Apellido;
                }
                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Propietarios eliminado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al eliminar este Propietarios, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult DetallesPropietario(int? cedula)
        {
            try
            {
                var getPropietarios = new Propietario();
                getPropietarios = (from age in model.Propietarios
                                   where age.Cedula == cedula
                                   select age).FirstOrDefault();
                return View(getPropietarios);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        public void CargarListaPropietarios()
        {
            using (model)
            {
                var Propietarios = (from x in model.Propietarios select x).ToList();
                foreach (var item in Propietarios)
                {
                    _Propietarios.Add(new Propietario
                    {
                        TipoCedula = item.TipoCedula,
                        Cedula = item.Cedula,
                        Nombre = item.Nombre,
                        Apellido = item.Apellido,
                        Telefono = item.Telefono,
                        Correo = item.Correo,
                        InformacionAdicional = item.InformacionAdicional,
                        NombreCompleto_Sec = item.NombreCompleto_Sec,
                        Email_Sec = item.Email_Sec,
                        Telefono_Sec = item.Telefono_Sec,
                        Parentezco_Sec = item.Parentezco_Sec
                    });
                }
            }
        }

        public ActionResult ExportarExcel()
        {
            CargarListaPropietarios();
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Propietarios");
                worksheet.ColumnWidth = 20;
                var currentRow = 1;

                //Header del Excel
                worksheet.Cell(currentRow, 1).Value = "Tipo de Cédula";
                worksheet.Cell(currentRow, 2).Value = "Cédula";
                worksheet.Cell(currentRow, 3).Value = "Nombre";
                worksheet.Cell(currentRow, 4).Value = "Apellidos";
                worksheet.Cell(currentRow, 5).Value = "Correo";
                worksheet.Cell(currentRow, 6).Value = "Información Adicional";
                worksheet.Cell(currentRow, 7).Value = "Contacto Secundario";
                worksheet.Cell(currentRow, 8).Value = "Email Cont.Secundario";
                worksheet.Cell(currentRow, 9).Value = "Teléfono Cont.Secundario";
                worksheet.Cell(currentRow, 10).Value = "Parentesco";

                //Body del Excel
                foreach (var item in _Propietarios)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = item.TipoCedula;
                    worksheet.Cell(currentRow, 2).Value = item.Cedula;
                    worksheet.Cell(currentRow, 3).Value = item.Nombre;
                    worksheet.Cell(currentRow, 4).Value = item.Apellido;
                    worksheet.Cell(currentRow, 5).Value = item.Correo;
                    worksheet.Cell(currentRow, 6).Value = item.InformacionAdicional;
                    worksheet.Cell(currentRow, 7).Value = item.NombreCompleto_Sec;
                    worksheet.Cell(currentRow, 8).Value = item.Email_Sec;
                    worksheet.Cell(currentRow, 9).Value = item.Telefono_Sec;
                    worksheet.Cell(currentRow, 10).Value = item.Parentezco_Sec;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();


                    return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Propietarios.xlsx");
                }
            }
        }
    }
}