﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using unified_checkpoint_v2.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.IO;
using ClosedXML.Excel;
using unified_checkpoint_v2.Negocio;

namespace unified_checkpoint_v2.Controllers
{
    public class AgenteController : Controller
    {
        DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();

        List<Agente> _agentes = new List<Agente>();

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            using (model)
            {
                var agentes = (from x in model.Agentes select x).ToList();

                return View(agentes);
            }
        }

        [HttpGet]
        public ActionResult InsertarAgente()
        {
            return View();
        }

        [HttpPost]
        public ActionResult InsertarAgente(Agente agente)
        {
            Auditoria auditoria = new Auditoria();
            AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();
            try
            {
                AgenteHelpers agenteHelpers = new AgenteHelpers();
                agenteHelpers.sp_InsertarAgente(agente);

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId; 
                    auditoria.Modulo = "Agentes";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " insertó el nuevo agente " + agente.Nombre + " " + agente.Apellido;
                }

                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Agente agregado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al ingresar este agente, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult ActualizarAgente(int? cedula)
        {
            try
            {
                var getAgente = new Agente();
                getAgente = (from age in model.Agentes
                             where age.Cedula == cedula
                             select age).FirstOrDefault();
                return View(getAgente);
                //return Json(getAgente, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult ActualizarAgente(Agente agente)
        {
            try
            {
                AgenteHelpers agenteHelpers = new AgenteHelpers();
                agenteHelpers.sp_ActualizarAgente(agente);

                Auditoria auditoria = new Auditoria();
                AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId; 
                    auditoria.Modulo = "Agentes";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " actualizó el agente " + agente.Nombre + " " + agente.Apellido;
                }
                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Agente actualizado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al actualizar este agente, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult EliminarAgente(int? cedula)
        {
            try
            {
                var getAgente = new Agente();
                getAgente = (from age in model.Agentes
                             where age.Cedula == cedula
                             select age).FirstOrDefault();
                return View(getAgente);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult EliminarAgente(Agente agente)
        {
            try
            {
                AgenteHelpers agenteHelpers = new AgenteHelpers();
                agenteHelpers.sp_EliminarAgente(agente);

                Auditoria auditoria = new Auditoria();
                AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId;
                    auditoria.Modulo = "Agentes";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " eliminó el agente " + agente.Nombre + " "+ agente.Apellido;
                }
                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Agente eliminado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al eliminar este agente, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult DetallesAgente(int? cedula)
        {
            try
            {
                var getAgente = new Agente();
                getAgente = (from age in model.Agentes
                             where age.Cedula == cedula
                             select age).FirstOrDefault();
                return View(getAgente);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        public void CargarListaAgentes()
        {
            using (model)
            {
                var agente = (from x in model.Agentes select x).ToList();
                foreach (var item in agente)
                {
                    _agentes.Add(new Agente
                    {
                        TipoCedula = item.TipoCedula,
                        Cedula = item.Cedula,
                        Nombre = item.Nombre,
                        Apellido = item.Apellido,
                        Telefono = item.Telefono,
                        Correo = item.Correo,
                        Agencia = item.Agencia,
                        InformacionAdicional = item.InformacionAdicional
                    });
                }
            }
        }

        public ActionResult ExportarExcel()
        {
            CargarListaAgentes();
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Agentes");
                worksheet.ColumnWidth = 20;
                var currentRow = 1;

                //Header del Excel
                worksheet.Cell(currentRow, 1).Value = "Tipo de Cédula";
                worksheet.Cell(currentRow, 2).Value = "Cédula";
                worksheet.Cell(currentRow, 3).Value = "Nombre";
                worksheet.Cell(currentRow, 4).Value = "Apellidos";
                worksheet.Cell(currentRow, 5).Value = "Correo";
                worksheet.Cell(currentRow, 6).Value = "Agencia";
                worksheet.Cell(currentRow, 7).Value = "Información Adicional";

                //Body del Excel
                foreach (var item in _agentes)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = item.TipoCedula;
                    worksheet.Cell(currentRow, 2).Value = item.Cedula;
                    worksheet.Cell(currentRow, 3).Value = item.Nombre;
                    worksheet.Cell(currentRow, 4).Value = item.Apellido;
                    worksheet.Cell(currentRow, 5).Value = item.Correo;
                    worksheet.Cell(currentRow, 6).Value = item.Agencia;
                    worksheet.Cell(currentRow, 7).Value = item.InformacionAdicional;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();


                    return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Agentes.xlsx");
                }
            }
        }
    }
}