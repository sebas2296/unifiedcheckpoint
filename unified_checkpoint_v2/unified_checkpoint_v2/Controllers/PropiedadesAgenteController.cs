﻿using ClosedXML.Excel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using unified_checkpoint_v2.Models;
using unified_checkpoint_v2.Negocio;

namespace unified_checkpoint_v2.Controllers
{
    public class PropiedadesAgenteController : Controller
    {
        DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();
        List<Propiedade> _Propiedades = new List<Propiedade>();

        [Authorize(Roles = "Admin,Vendedor,Agente")]
        public ActionResult Index()
        {
            using (model)
            {
                var Propiedadess = (from x in model.Propiedades.Include("Propietario1").Include("Agente1") where x.Agente != null select x).ToList();

                return View(Propiedadess);
            }
        }

        [HttpGet]
        public ActionResult InsertarPropiedad()
        {
            using (model)
            {
                var propietarios = (from x in model.Agentes select x).ToList();

                return View(propietarios);
            }
        }

        [HttpPost]
        public ActionResult InsertarPropiedad(HttpPostedFileBase image_path, Propiedade propiedad)
        {
            try
            {
                if (image_path != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        image_path.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                        propiedad.Imagen = array;
                    }
                }

                PropiedadAgenteHelpers propiedadHelpers = new PropiedadAgenteHelpers();
                propiedadHelpers.sp_InsertarPropiedad(propiedad);

                Auditoria auditoria = new Auditoria();
                AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId;
                    auditoria.Modulo = "PropiedadesAgentes";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " insertó una nueva propiedad de agente " + propiedad.Codigo + " - " + propiedad.Nombre;
                }
                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Propiedad agregada corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al ingresar esta propiedad, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        public ActionResult ActualizarPropiedad(string codigo)
        {
            try
            {
                var getPropiedad = new Propiedade();
                getPropiedad = (from age in model.Propiedades.Include("Propietario1").Include("Agente1")
                                where age.Codigo == codigo
                                select age).FirstOrDefault();
                return View(getPropiedad);
                //return Json(getAgente, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult ActualizarPropiedad(HttpPostedFileBase image_path, Propiedade propiedad)
        {
            try
            {
                if (image_path != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        image_path.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                        propiedad.Imagen = array;
                    }
                }
                PropiedadAgenteHelpers propiedadHelpers = new PropiedadAgenteHelpers();
                propiedadHelpers.sp_Actualizarpropiedad(propiedad);

                Auditoria auditoria = new Auditoria();
                AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId;
                    auditoria.Modulo = "PropiedadesAgentes";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " actualizó la propiedad de agente " + propiedad.Codigo + " - " + propiedad.Nombre;
                }
                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Propiedad actualizada corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al actualizar esta propiedad, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult DetallesPropiedad(string codigo)
        {
            try
            {
                var getPropiedad = new Propiedade();
                getPropiedad = (from age in model.Propiedades.Include("Propietario1").Include("Agente1")
                                where age.Codigo == codigo
                                select age).FirstOrDefault();
                return View(getPropiedad);
                //return Json(getAgente, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult EliminarPropiedad(string codigo)
        {
            try
            {
                var getPropiedad = new Propiedade();
                getPropiedad = (from age in model.Propiedades.Include("Propietario1").Include("Agente1")
                                where age.Codigo == codigo
                                select age).FirstOrDefault();
                return View(getPropiedad);
                //return Json(getAgente, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult EliminarPropiedad(Propiedade propiedad)
        {
            try
            {
                PropiedadHelpers propiedadHelpers = new PropiedadHelpers();
                propiedadHelpers.sp_Eliminarpropiedad(propiedad);

                Auditoria auditoria = new Auditoria();
                AuditoriaHelpers auditoriaHelpers = new AuditoriaHelpers();

                using (model)
                {
                    var userId = User.Identity.GetUserId();
                    var usuarioAuditoria = (from x in model.AspNetUsers where x.Id == userId select x).FirstOrDefault();
                    auditoria.Usuario = userId;
                    auditoria.Modulo = "PropiedadesAgentes";
                    auditoria.Accion = "El usuario " + usuarioAuditoria.UserName + " eliminó la propiedad de agente " + propiedad.Codigo + " - " + propiedad.Nombre;
                }
                auditoriaHelpers.spAuditoria(auditoria);

                string mensaje = "Propiedad eliminada corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al eliminar esta propiedad, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        public void CargarListaPropiedades()
        {
            using (model)
            {
                var Propiedadess = (from x in model.Propiedades where x.Agente != null select x).ToList();
                foreach (var item in Propiedadess)
                {
                    _Propiedades.Add(new Propiedade
                    {
                        Codigo = item.Codigo,
                        Nombre = item.Nombre,
                        Propietario = item.Propietario,
                        Agente = item.Agente,
                        LinkSitioWeb = item.LinkSitioWeb,
                        LinkGoogleMaps = item.LinkGoogleMaps,
                        PrecioVenta = item.PrecioVenta,
                        PrecioAlquiler = item.PrecioAlquiler,
                        NoPlanoCatastro = item.NoPlanoCatastro,
                        NoFolioReal = item.NoFolioReal,
                        AreaTerrenoM2 = item.AreaTerrenoM2,
                        AreaConstruccionM2 = item.AreaConstruccionM2,
                        Provincia = item.Provincia,
                        Canton = item.Canton,
                        Distrito = item.Distrito,
                        Direccion = item.Direccion,
                        CuotaMantenimiento = item.CuotaMantenimiento,
                        CuotaSeguridad = item.CuotaSeguridad,
                        ImpuestoSolidario = item.ImpuestoSolidario,
                        Impuestos = item.Impuestos,
                        IVA = item.IVA,
                        CodigoPostal = item.CodigoPostal,
                        FrenteM2 = item.FrenteM2,
                        FondoM2 = item.FondoM2,
                        AlturaM2 = item.AlturaM2,
                        Niveles = item.Niveles,
                        Dormitorios = item.Dormitorios,
                        Baños = item.Baños,
                        Pisos = item.Pisos,
                        Cielos = item.Cielos,
                        MedidorAgua = item.MedidorAgua,
                        Sala = item.Sala,
                        Comedor = item.Comedor,
                        Cocina = item.Cocina,
                        Desayunador = item.Desayunador,
                        Antecom = item.Antecom,
                        SalaTV = item.SalaTV,
                        Bar = item.Bar,
                        Casino = item.Casino,
                        Terraza = item.Terraza,
                        Balcon = item.Balcon,
                        Lavanderia = item.Lavanderia,
                        Despensa = item.Despensa,
                        ClosetBlanco = item.ClosetBlanco,
                        Walk_in_Closet = item.Walk_in_Closet,
                        Puertas_Baño = item.Puertas_Baño,
                        Senderos = item.Senderos,
                        Mascotas = item.Mascotas,
                        AreaMascotaM2 = item.AreaMascotaM2,
                        Perrera = item.Perrera,
                        Seguridad = item.Seguridad,
                        Oficina = item.Oficina,
                        Bodega = item.Bodega,
                        Garage = item.Garage,
                        Jardin = item.Jardin,
                        Patio = item.Patio,
                        Patio_Luz = item.Patio_Luz,
                        Atico = item.Atico,
                        Piscina = item.Piscina,
                        Rancho = item.Rancho,
                        Canchas = item.Canchas,
                        Gimnacio = item.Gimnacio,
                        Lounge = item.Lounge,
                        Jacuzzi = item.Jacuzzi,
                        Cortinas = item.Cortinas,
                        Lamparas = item.Lamparas,
                        Chimenea = item.Chimenea,
                        Cedazos = item.Cedazos,
                        InterCom = item.InterCom,
                        PlantaElectrica = item.PlantaElectrica,
                        Asensor = item.Asensor,
                        Parqueo = item.Parqueo,
                        HotH20 = item.HotH20,
                        Cloaca = item.Cloaca,
                        Septico = item.Septico,
                        PlantaTratamiento = item.PlantaTratamiento,
                        Capt_Bomba = item.Capt_Bomba,
                        Tels = item.Tels,
                        Cable = item.Cable,
                        Internet = item.Internet,
                        Alarma = item.Alarma,
                        CCTV = item.CCTV,
                        PortonElectrico = item.PortonElectrico,
                        Control = item.Control,
                        CercaElectrica = item.CercaElectrica,
                        AlambreNavaja = item.AlambreNavaja,
                        A_C = item.A_C,
                        LampEmergencia = item.LampEmergencia,
                        DetectorHumo = item.DetectorHumo,
                        SistemaIncendio = item.SistemaIncendio,
                        SistemaSonido = item.SistemaSonido,
                        TableroDigital = item.TableroDigital,
                        TableroElec = item.TableroElec,
                        Sobres = item.Sobres,
                        Amueblado = item.Amueblado,
                        MueblesCocina = item.MueblesCocina,
                        Electrod = item.Electrod,
                        Cerchas = item.Cerchas,
                        Cornizas = item.Cornizas,
                        Puertas = item.Puertas,
                        ContraMarcos = item.ContraMarcos,
                        Ventaneria = item.Ventaneria,
                        Cobertura = item.Cobertura,
                        Est_Suelo = item.Est_Suelo,
                        DispositivoH2O = item.DispositivoH2O,
                        UsoSuelo = item.UsoSuelo,
                        Planos_Construccion = item.Planos_Construccion,
                        Plano_Catastro = item.Plano_Catastro,
                        Curvas_Nivel = item.Curvas_Nivel,
                        Avaluo = item.Avaluo,
                        Venta = item.Venta,
                        Alquiler = item.Alquiler,
                        T_Casa = item.T_Casa,
                        T_Terreno = item.T_Terreno,
                        T_Local = item.T_Local,
                        T_Oficina = item.T_Oficina,
                        T_Bodega = item.T_Bodega,
                        T_Edificio = item.T_Edificio,
                        T_Hotel = item.T_Hotel,
                        T_Condominio = item.T_Condominio,
                        T_Apartamento = item.T_Apartamento,
                        Ocupado = item.Ocupado,
                        Llaves = item.Llaves,
                        MapsLat = item.MapsLat,
                        MapsLong = item.MapsLong,
                        Imagen = null,
                        Estado = item.Estado,
                    });
                }
            }
        }

        public ActionResult ExportarExcel()
        {
            CargarListaPropiedades();
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("PropiedadesAgente");
                worksheet.ColumnWidth = 20;
                var currentRow = 1;

                //Header del Excel
                worksheet.Cell(currentRow, 1).Value = "Código";
                worksheet.Cell(currentRow, 2).Value = "Nombre";
                worksheet.Cell(currentRow, 3).Value = "Agente";
                worksheet.Cell(currentRow, 4).Value = "LinkSitioWeb";
                worksheet.Cell(currentRow, 5).Value = "LinkGoogleMaps";
                worksheet.Cell(currentRow, 6).Value = "PrecioVenta";
                worksheet.Cell(currentRow, 7).Value = "PrecioAlquiler";
                worksheet.Cell(currentRow, 8).Value = "NoPlanoCatastro";
                worksheet.Cell(currentRow, 9).Value = "NoFolioReal";
                worksheet.Cell(currentRow, 10).Value = "AreaTerrenoM2";
                worksheet.Cell(currentRow, 11).Value = "AreaConstruccionM2";
                worksheet.Cell(currentRow, 12).Value = "Provincia";
                worksheet.Cell(currentRow, 13).Value = "Cantón";
                worksheet.Cell(currentRow, 14).Value = "Distrito";
                worksheet.Cell(currentRow, 15).Value = "Dirección";
                worksheet.Cell(currentRow, 16).Value = "CuotaMantenimiento";
                worksheet.Cell(currentRow, 17).Value = "CuotaSeguridad";
                worksheet.Cell(currentRow, 18).Value = "ImpuestoSolidario";
                worksheet.Cell(currentRow, 19).Value = "Impuestos";
                worksheet.Cell(currentRow, 20).Value = "IVA";
                worksheet.Cell(currentRow, 21).Value = "CodigoPostal";
                worksheet.Cell(currentRow, 22).Value = "FrenteM2";
                worksheet.Cell(currentRow, 23).Value = "FondoM2";
                worksheet.Cell(currentRow, 24).Value = "AlturaM2";
                worksheet.Cell(currentRow, 25).Value = "Niveles";
                worksheet.Cell(currentRow, 26).Value = "Dormitorios";
                worksheet.Cell(currentRow, 27).Value = "Baños";
                worksheet.Cell(currentRow, 28).Value = "Pisos";
                worksheet.Cell(currentRow, 29).Value = "Cielos";
                worksheet.Cell(currentRow, 30).Value = "MedidorAgua";
                worksheet.Cell(currentRow, 31).Value = "Sala";
                worksheet.Cell(currentRow, 32).Value = "Comedor";
                worksheet.Cell(currentRow, 33).Value = "Cocina";
                worksheet.Cell(currentRow, 34).Value = "Desayunador";
                worksheet.Cell(currentRow, 35).Value = "Antecom";
                worksheet.Cell(currentRow, 36).Value = "SalaTV";
                worksheet.Cell(currentRow, 37).Value = "Bar";
                worksheet.Cell(currentRow, 38).Value = "Casino";
                worksheet.Cell(currentRow, 39).Value = "Terraza";
                worksheet.Cell(currentRow, 40).Value = "Balcón";
                worksheet.Cell(currentRow, 41).Value = "Lavandería";
                worksheet.Cell(currentRow, 42).Value = "Despensa";
                worksheet.Cell(currentRow, 43).Value = "ClosetBlanco";
                worksheet.Cell(currentRow, 44).Value = "WalkInCloset";
                worksheet.Cell(currentRow, 45).Value = "PuertasBaño";
                worksheet.Cell(currentRow, 46).Value = "Senderos";
                worksheet.Cell(currentRow, 47).Value = "Mascotas";
                worksheet.Cell(currentRow, 48).Value = "AreaMascotaM2";
                worksheet.Cell(currentRow, 49).Value = "Perrera";
                worksheet.Cell(currentRow, 50).Value = "Seguridad";
                worksheet.Cell(currentRow, 51).Value = "Oficina";
                worksheet.Cell(currentRow, 52).Value = "Bodega";
                worksheet.Cell(currentRow, 53).Value = "Garage";
                worksheet.Cell(currentRow, 54).Value = "Jardín";
                worksheet.Cell(currentRow, 55).Value = "Patio";
                worksheet.Cell(currentRow, 56).Value = "PatioLuz";
                worksheet.Cell(currentRow, 57).Value = "Atico";
                worksheet.Cell(currentRow, 58).Value = "Piscina";
                worksheet.Cell(currentRow, 59).Value = "Rancho";
                worksheet.Cell(currentRow, 60).Value = "Canchas";
                worksheet.Cell(currentRow, 61).Value = "Gimnacio";
                worksheet.Cell(currentRow, 62).Value = "Lounge";
                worksheet.Cell(currentRow, 63).Value = "Jacuzzi";
                worksheet.Cell(currentRow, 64).Value = "Cortinas";
                worksheet.Cell(currentRow, 65).Value = "Lamparas";
                worksheet.Cell(currentRow, 66).Value = "Chimenea";
                worksheet.Cell(currentRow, 67).Value = "Cedazos";
                worksheet.Cell(currentRow, 68).Value = "Intercom";
                worksheet.Cell(currentRow, 69).Value = "PlantaElectrica";
                worksheet.Cell(currentRow, 70).Value = "Asensor";
                worksheet.Cell(currentRow, 71).Value = "Parqueo";
                worksheet.Cell(currentRow, 72).Value = "HotH2O";
                worksheet.Cell(currentRow, 73).Value = "Cloaca";
                worksheet.Cell(currentRow, 74).Value = "Septico";
                worksheet.Cell(currentRow, 75).Value = "PlantaTratamiento";
                worksheet.Cell(currentRow, 76).Value = "Capt_Bomba";
                worksheet.Cell(currentRow, 77).Value = "Tels";
                worksheet.Cell(currentRow, 78).Value = "Cable";
                worksheet.Cell(currentRow, 79).Value = "Internet";
                worksheet.Cell(currentRow, 80).Value = "Alarma";
                worksheet.Cell(currentRow, 81).Value = "CCTV";
                worksheet.Cell(currentRow, 82).Value = "PortonElectrico";
                worksheet.Cell(currentRow, 83).Value = "Control";
                worksheet.Cell(currentRow, 84).Value = "CercaElectrica";
                worksheet.Cell(currentRow, 85).Value = "AlambreNavaja";
                worksheet.Cell(currentRow, 86).Value = "A_C";
                worksheet.Cell(currentRow, 87).Value = "LampEmergencia";
                worksheet.Cell(currentRow, 88).Value = "DetectorHumo";
                worksheet.Cell(currentRow, 89).Value = "SistemaIncendio";
                worksheet.Cell(currentRow, 90).Value = "SistemaSonido";
                worksheet.Cell(currentRow, 91).Value = "TableroDigital";
                worksheet.Cell(currentRow, 92).Value = "TableroElec";
                worksheet.Cell(currentRow, 93).Value = "Sobres";
                worksheet.Cell(currentRow, 94).Value = "Amueblado";
                worksheet.Cell(currentRow, 95).Value = "MueblesCocina";
                worksheet.Cell(currentRow, 96).Value = "Electrod";
                worksheet.Cell(currentRow, 97).Value = "Cerchas";
                worksheet.Cell(currentRow, 98).Value = "Cornizas";
                worksheet.Cell(currentRow, 99).Value = "Puertas";
                worksheet.Cell(currentRow, 100).Value = "Contramarcos";
                worksheet.Cell(currentRow, 101).Value = "Ventanería";
                worksheet.Cell(currentRow, 102).Value = "Cobertura";
                worksheet.Cell(currentRow, 103).Value = "Est_Suelo";
                worksheet.Cell(currentRow, 104).Value = "DispositivoH2O";
                worksheet.Cell(currentRow, 105).Value = "UsoSuelo";
                worksheet.Cell(currentRow, 106).Value = "PlanosConstruccion";
                worksheet.Cell(currentRow, 107).Value = "PlanoCatastro";
                worksheet.Cell(currentRow, 108).Value = "CurvasNivel";
                worksheet.Cell(currentRow, 109).Value = "Avaluo";
                worksheet.Cell(currentRow, 110).Value = "Venta";
                worksheet.Cell(currentRow, 111).Value = "Alquiler";
                worksheet.Cell(currentRow, 112).Value = "T_Casa";
                worksheet.Cell(currentRow, 113).Value = "T_Terreno";
                worksheet.Cell(currentRow, 114).Value = "T_Local";
                worksheet.Cell(currentRow, 115).Value = "T_Oficina";
                worksheet.Cell(currentRow, 116).Value = "T_Bodega";
                worksheet.Cell(currentRow, 117).Value = "T_Edificio";
                worksheet.Cell(currentRow, 118).Value = "T_Hotel";
                worksheet.Cell(currentRow, 119).Value = "T_Condominio";
                worksheet.Cell(currentRow, 120).Value = "T_Aparteamento";
                worksheet.Cell(currentRow, 121).Value = "Ocupado";
                worksheet.Cell(currentRow, 122).Value = "Llaves";
                worksheet.Cell(currentRow, 123).Value = "MapsLat";
                worksheet.Cell(currentRow, 124).Value = "MapsLong";
                worksheet.Cell(currentRow, 125).Value = "Estado";


                //Body del Excel
                foreach (var item in _Propiedades)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = item.Codigo;
                    worksheet.Cell(currentRow, 2).Value = item.Nombre;
                    worksheet.Cell(currentRow, 3).Value = item.Agente;
                    worksheet.Cell(currentRow, 4).Value = item.LinkSitioWeb;
                    worksheet.Cell(currentRow, 5).Value = item.LinkGoogleMaps;
                    worksheet.Cell(currentRow, 6).Value = item.PrecioVenta;
                    worksheet.Cell(currentRow, 7).Value = item.PrecioAlquiler;
                    worksheet.Cell(currentRow, 8).Value = item.NoPlanoCatastro;
                    worksheet.Cell(currentRow, 9).Value = item.NoFolioReal;
                    worksheet.Cell(currentRow, 10).Value = item.AreaTerrenoM2;
                    worksheet.Cell(currentRow, 11).Value = item.AreaConstruccionM2;
                    worksheet.Cell(currentRow, 12).Value = item.Provincia;
                    worksheet.Cell(currentRow, 13).Value = item.Canton;
                    worksheet.Cell(currentRow, 14).Value = item.Distrito;
                    worksheet.Cell(currentRow, 15).Value = item.Direccion;
                    worksheet.Cell(currentRow, 16).Value = item.CuotaMantenimiento;
                    worksheet.Cell(currentRow, 17).Value = item.CuotaSeguridad;
                    worksheet.Cell(currentRow, 18).Value = item.ImpuestoSolidario;
                    worksheet.Cell(currentRow, 19).Value = item.Impuestos;
                    worksheet.Cell(currentRow, 20).Value = item.IVA;
                    worksheet.Cell(currentRow, 21).Value = item.CodigoPostal;
                    worksheet.Cell(currentRow, 22).Value = item.FrenteM2;
                    worksheet.Cell(currentRow, 23).Value = item.FondoM2;
                    worksheet.Cell(currentRow, 24).Value = item.AlturaM2;
                    worksheet.Cell(currentRow, 25).Value = item.Niveles;
                    worksheet.Cell(currentRow, 26).Value = item.Dormitorios;
                    worksheet.Cell(currentRow, 27).Value = item.Baños;
                    worksheet.Cell(currentRow, 28).Value = item.Pisos;
                    worksheet.Cell(currentRow, 29).Value = item.Cielos;
                    if (item.MedidorAgua == true) { worksheet.Cell(currentRow, 30).Value = "Si"; } else { worksheet.Cell(currentRow, 30).Value = "No"; }
                    worksheet.Cell(currentRow, 31).Value = item.Sala;
                    worksheet.Cell(currentRow, 32).Value = item.Comedor;
                    worksheet.Cell(currentRow, 33).Value = item.Cocina;
                    worksheet.Cell(currentRow, 34).Value = item.Desayunador;
                    worksheet.Cell(currentRow, 35).Value = item.Antecom;
                    worksheet.Cell(currentRow, 36).Value = item.SalaTV;
                    worksheet.Cell(currentRow, 37).Value = item.Bar;
                    worksheet.Cell(currentRow, 38).Value = item.Casino;
                    worksheet.Cell(currentRow, 39).Value = item.Terraza;
                    worksheet.Cell(currentRow, 40).Value = item.Balcon;
                    worksheet.Cell(currentRow, 41).Value = item.Lavanderia;
                    worksheet.Cell(currentRow, 42).Value = item.Despensa;
                    worksheet.Cell(currentRow, 43).Value = item.ClosetBlanco;
                    worksheet.Cell(currentRow, 44).Value = item.Walk_in_Closet;
                    worksheet.Cell(currentRow, 45).Value = item.Puertas_Baño;
                    worksheet.Cell(currentRow, 46).Value = item.Senderos;
                    worksheet.Cell(currentRow, 47).Value = item.Mascotas;
                    worksheet.Cell(currentRow, 48).Value = item.AreaMascotaM2;
                    worksheet.Cell(currentRow, 49).Value = item.Perrera;
                    worksheet.Cell(currentRow, 50).Value = item.Seguridad;
                    worksheet.Cell(currentRow, 51).Value = item.Oficina;
                    worksheet.Cell(currentRow, 52).Value = item.Bodega;
                    worksheet.Cell(currentRow, 53).Value = item.Garage;
                    worksheet.Cell(currentRow, 54).Value = item.Jardin;
                    worksheet.Cell(currentRow, 55).Value = item.Patio;
                    worksheet.Cell(currentRow, 56).Value = item.Patio_Luz;
                    worksheet.Cell(currentRow, 57).Value = item.Atico;
                    worksheet.Cell(currentRow, 58).Value = item.Piscina;
                    worksheet.Cell(currentRow, 59).Value = item.Rancho;
                    worksheet.Cell(currentRow, 60).Value = item.Canchas;
                    worksheet.Cell(currentRow, 61).Value = item.Gimnacio;
                    worksheet.Cell(currentRow, 62).Value = item.Lounge;
                    worksheet.Cell(currentRow, 63).Value = item.Jacuzzi;
                    worksheet.Cell(currentRow, 64).Value = item.Cortinas;
                    worksheet.Cell(currentRow, 65).Value = item.Lamparas;
                    worksheet.Cell(currentRow, 66).Value = item.Chimenea;
                    worksheet.Cell(currentRow, 67).Value = item.Cedazos;
                    worksheet.Cell(currentRow, 68).Value = item.InterCom;
                    worksheet.Cell(currentRow, 69).Value = item.PlantaElectrica;
                    worksheet.Cell(currentRow, 70).Value = item.Asensor;
                    worksheet.Cell(currentRow, 71).Value = item.Parqueo;
                    worksheet.Cell(currentRow, 72).Value = item.HotH20;
                    worksheet.Cell(currentRow, 73).Value = item.Cloaca;
                    worksheet.Cell(currentRow, 74).Value = item.Septico;
                    worksheet.Cell(currentRow, 75).Value = item.PlantaTratamiento;
                    worksheet.Cell(currentRow, 76).Value = item.Capt_Bomba;
                    worksheet.Cell(currentRow, 77).Value = item.Tels;
                    worksheet.Cell(currentRow, 78).Value = item.Cable;
                    worksheet.Cell(currentRow, 79).Value = item.Internet;
                    worksheet.Cell(currentRow, 80).Value = item.Alarma;
                    worksheet.Cell(currentRow, 81).Value = item.CCTV;
                    worksheet.Cell(currentRow, 82).Value = item.PortonElectrico;
                    worksheet.Cell(currentRow, 83).Value = item.Control;
                    worksheet.Cell(currentRow, 84).Value = item.CercaElectrica;
                    worksheet.Cell(currentRow, 85).Value = item.AlambreNavaja;
                    worksheet.Cell(currentRow, 86).Value = item.A_C;
                    worksheet.Cell(currentRow, 87).Value = item.LampEmergencia;
                    worksheet.Cell(currentRow, 88).Value = item.DetectorHumo;
                    worksheet.Cell(currentRow, 89).Value = item.SistemaIncendio;
                    worksheet.Cell(currentRow, 90).Value = item.SistemaSonido;
                    worksheet.Cell(currentRow, 91).Value = item.TableroDigital;
                    worksheet.Cell(currentRow, 92).Value = item.TableroElec;
                    worksheet.Cell(currentRow, 93).Value = item.Sobres;
                    worksheet.Cell(currentRow, 94).Value = item.Amueblado;
                    worksheet.Cell(currentRow, 95).Value = item.MueblesCocina;
                    worksheet.Cell(currentRow, 96).Value = item.Electrod;
                    worksheet.Cell(currentRow, 97).Value = item.Cerchas;
                    worksheet.Cell(currentRow, 98).Value = item.Cornizas;
                    worksheet.Cell(currentRow, 99).Value = item.Puertas;
                    worksheet.Cell(currentRow, 100).Value = item.ContraMarcos;
                    worksheet.Cell(currentRow, 101).Value = item.Ventaneria;
                    worksheet.Cell(currentRow, 102).Value = item.Cobertura;
                    worksheet.Cell(currentRow, 103).Value = item.Est_Suelo;
                    worksheet.Cell(currentRow, 104).Value = item.DispositivoH2O;
                    worksheet.Cell(currentRow, 105).Value = item.UsoSuelo;
                    worksheet.Cell(currentRow, 106).Value = item.Planos_Construccion;
                    worksheet.Cell(currentRow, 107).Value = item.Plano_Catastro;
                    worksheet.Cell(currentRow, 108).Value = item.Curvas_Nivel;
                    worksheet.Cell(currentRow, 109).Value = item.Avaluo;
                    worksheet.Cell(currentRow, 110).Value = item.Venta;
                    worksheet.Cell(currentRow, 111).Value = item.Alquiler;
                    worksheet.Cell(currentRow, 112).Value = item.T_Casa;
                    worksheet.Cell(currentRow, 113).Value = item.T_Terreno;
                    worksheet.Cell(currentRow, 114).Value = item.T_Local;
                    worksheet.Cell(currentRow, 115).Value = item.T_Oficina;
                    worksheet.Cell(currentRow, 116).Value = item.T_Bodega;
                    worksheet.Cell(currentRow, 117).Value = item.T_Edificio;
                    worksheet.Cell(currentRow, 118).Value = item.T_Hotel;
                    worksheet.Cell(currentRow, 119).Value = item.T_Condominio;
                    worksheet.Cell(currentRow, 120).Value = item.T_Apartamento;
                    worksheet.Cell(currentRow, 121).Value = item.Ocupado;
                    worksheet.Cell(currentRow, 122).Value = item.Llaves;
                    worksheet.Cell(currentRow, 123).Value = item.MapsLat;
                    worksheet.Cell(currentRow, 124).Value = item.MapsLong;
                    worksheet.Cell(currentRow, 125).Value = item.Estado;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();


                    return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "PropiedadesAgentes.xlsx");
                }
            }
        }
    }
}