﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using unified_checkpoint_v2.Models;

namespace unified_checkpoint_v2.Controllers
{
    public class ImageController : Controller
    {
        DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();
        public ActionResult Index(string codigo)
        {
            byte[] imageData = model.Propiedades.FirstOrDefault(i => i.Codigo == codigo)?.Imagen;
            if (imageData != null)
            {
                return File(imageData, "image/png");
            }
            return null;
        }
    }
}