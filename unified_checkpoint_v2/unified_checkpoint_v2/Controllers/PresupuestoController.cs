﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using unified_checkpoint_v2.Models;
using unified_checkpoint_v2.Negocio;

namespace unified_checkpoint_v2.Controllers
{
    public class PresupuestoController : Controller
    {
        DBUnifiedCheckpointEntities2 model = new DBUnifiedCheckpointEntities2();
        List<Presupuesto> _presupuestos = new List<Presupuesto>();


        [Authorize(Roles = "Admin,Vendedor")]
        // GET: Presupuesto
        public ActionResult Index()
        {
            using (model)
            {
                var presupuestos = (from x in model.Presupuestoes select x).ToList();

                return View(presupuestos);
            }
        }

        [HttpGet]
        public ActionResult InsertarPresupuesto()
        {
            using (model)
            {
                var propiedades = (from x in model.Propiedades select x).ToList();

                return View(propiedades);

            }
        }

        [HttpPost]
        public ActionResult InsertarPresupuesto(Presupuesto presupuestos)
        {
            try
            {
                PresupuestoHelpers presupuestoHelpers = new PresupuestoHelpers();
                presupuestoHelpers.sp_InsertarPresupuesto(presupuestos);

                string mensaje = "Presupuesto agregado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al ingresar el presupuesto, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult ActualizarPresupuesto(int? IdPresupuesto)
        {
            try
            {
                var getPresupuesto = new Presupuesto();
                getPresupuesto = (from id in model.Presupuestoes
                                  where id.IdPresupuesto == IdPresupuesto
                                  select id).FirstOrDefault();
                return View(getPresupuesto);
            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult ActualizarPresupuesto(Presupuesto presupuestos)
        {
            try
            {
                PresupuestoHelpers presupuestoHelpers = new PresupuestoHelpers();
                presupuestoHelpers.sp_ActualizarPresupuesto(presupuestos);

                string mensaje = "Presupuesto actualizado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al actualizar este presupuesto, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public ActionResult EliminarPresupuesto(int? IdPresupuesto)
        {
            try
            {
                var getPresupuesto = new Presupuesto();
                getPresupuesto = (from id in model.Presupuestoes
                                  where id.IdPresupuesto == IdPresupuesto
                                  select id).FirstOrDefault();
                return View(getPresupuesto);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult EliminarPresupuesto(Presupuesto presupuestos)
        {
            try
            {
                PresupuestoHelpers presupuestoHelpers = new PresupuestoHelpers();
                presupuestoHelpers.sp_EliminarPresupuesto(presupuestos);

                string mensaje = "Presupuesto eliminado corectamente!!";
                return Json(mensaje, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                string mensaje = "Error al eliminar este Presupuesto, por favor verifique que los campos sean correctos!!";
                return Json(mensaje, JsonRequestBehavior.DenyGet);
            }
        }
        [HttpGet]
        public ActionResult DetallesPresupuesto(int? IdPresupuesto)
        {
            try
            {
                var getPresupuesto = new Presupuesto();
                getPresupuesto = (from id in model.Presupuestoes
                                  where id.IdPresupuesto == IdPresupuesto
                                  select id).FirstOrDefault();
                return View(getPresupuesto);

            }
            catch (Exception ex)
            {
                ex.ToString();
                //return RedirectToAction("index", "Home");
                return Json(null, JsonRequestBehavior.DenyGet);
            }
        }

    }
}